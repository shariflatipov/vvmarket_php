﻿$(document).ready(function(){
    
function liFormat (row, i, num) {
	var result = row[0] + '<p class=qnt>' + row[1] + ' </p>';
	return result;
}

function selectProduct(li) {
    count = li.extra[1];

    document.forms[0].elements["unit_" + count].value = li.extra[5];
    document.forms[0].elements["quantity_" + count].value = li.extra[0];
    document.forms[0].elements["barcode_" + count].value = li.extra[4];
    document.forms[0].elements["price_" + count].value = li.extra[3];
    //document.forms[0].elements["rate_" + count].value = li.extra[5];
    //document.forms[0].elements["curabrev_" + count].value = li.extra[6];
    document.forms[0].elements["total_" + count].value = (li.extra[0] * li.extra[3]);
}
function selectBarcode(li) {
    count = li.extra[1];

    document.forms[0].elements["unit_" + count].value = li.extra[5];
    document.forms[0].elements["quantity_" + count].value = li.extra[0];
    document.forms[0].elements["product_" + count].value = li.extra[4];
    document.forms[0].elements["price_" + count].value = li.extra[3];
    //document.forms[0].elements["barcode_" + count].value = li.extra[0];
    //document.forms[0].elements["curabrev_" + count].value = li.extra[6];
    document.forms[0].elements["total_" + count].value = (li.extra[0] * li.extra[3]);
}

function selectre(li) {
    count = li.extra[1];
    document.forms[0].elements["artikle_" + count].value =li.extra[0];
}

function selectshtr(li) {
    count = li.extra[1];
    document.forms[0].elements["artikle_" + count].value =li.extra[0];
    document.forms[0].elements["product_" + count].value =li.extra[2];

}


function selectAcc_person(li) {
    
	if( !!li.extra ) var sValue = li.extra[1];
	else var sValue = li.selectValue;
	$('#acc_id').val(sValue);
	$('#acc_id').focus();
}
function getAcc_id() {
    return ($('#acc_id').val());
}
$('#acc_id').val(0);
// --- Автозаполнение2 ---
$("#acc_person_f").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:true,
	formatItem:liFormat,
	maxItemsToShow:10,
	onItemSelect:selectAcc_person
}); 

$("#acc_person_s").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:true,
	formatItem:liFormat,
	maxItemsToShow:10
});

var count2 = 0;
while( count2++ < 17 ) {
    $("#barcode_field_" + count2 ).autocomplete("forms/GetBarcode.php", {
    	delay:600,
	    minChars:3,
	    width: 400,
	    matchSubset:1,
	    autoFill:false,
	    matchContains:1,
	    cacheLength:10,
	    selectFirst:true,
	    formatItem:liFormat,
	    maxItemsToShow:10,
	    onItemSelect:selectBarcode,
	    extraParams: {id: count2}
    });
};


var count = 0;	
while( count++ < 17 ) {
    $("#accp_s_products_" + count ).autocomplete("forms/get_accp_s_products.php", {
    	delay:600,
	    minChars:3,
	    width: 400,
	    matchSubset:1,
	    autoFill:false,
	    matchContains:1,
	    cacheLength:10,
	    selectFirst:true,
	    formatItem:liFormat,
	    maxItemsToShow:10,
	    onItemSelect:selectProduct,
	    extraParams: {id: count}
    });
};

$('#storekeeper').autocomplete("forms/get_storekeeper.php", {
	delay:600,
    minChars:3,
    matchSubset:1,
    autoFill:false,
    matchContains:1,
    cacheLength:10,
    selectFirst:false,
    formatItem:liFormat,
    maxItemsToShow:10
});

$("#accepted_from").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:false,
	formatItem:liFormat,
	maxItemsToShow:10
});

var count1 = 0;
var rowCount = (document.getElementById("dataStoreTable").getElementsByTagName("TR").length - 2);

while( count1++ <= rowCount ) {
    $("#product_" + count1).autocomplete("forms/get_products.php", {
	    delay:300,
	    minChars:3,
	    matchSubset:1,
	    autoFill:false,
	    matchContains:1,
	    cacheLength:10,
	    selectFirst:true,
	    formatItem:liFormat,
	    maxItemsToShow:10,
        onItemSelect:selectre,
        extraParams: {id: count1}
    });
}

while( count1++ <= rowCount ) {
    alert(count1);
    $("#artikle_" + count1).autocomplete("forms/get_artikle.php", {
    	delay:600,
	    minChars:3,
	    matchSubset:1,
	    autoFill:false,
	    matchContains:1,
	    cacheLength:10,
	    selectFirst:true,
	    formatItem:liFormat,
	    maxItemsToShow:10,
        onItemSelect:selectshtr,
        extraParams: {id: count1}
    });
}

$("#product_selection").autocomplete("forms/get_products.php", {
	delay:600,
    minChars:3,
    matchSubset:1,
    autoFill:false,
    matchContains:1,
    cacheLength:10,
    selectFirst:true,
    formatItem:liFormat,
    maxItemsToShow:10
});

$("#matres").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:false,
	formatItem:liFormat,
	maxItemsToShow:10
});

$("#acc_person").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:false,
	formatItem:liFormat,
	maxItemsToShow:10
});

$("#acc_person_1").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:false,
	formatItem:liFormat,
	maxItemsToShow:10
});

$("#acc_person_2").autocomplete("forms/get_acc_persons.php", {
	delay:600,
	minChars:3,
	matchSubset:1,
	autoFill:false,
	matchContains:1,
	cacheLength:10,
	selectFirst:false,
	formatItem:liFormat,
	maxItemsToShow:10
});
 
});