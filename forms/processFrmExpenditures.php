﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
include_once 'languages.php';
include_once 'classes/supplier.php';
include_once('inc/functions.php');
parseLangFile(langSelected());
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$langs['expOrder'];?> № <?=$_POST['doc_number'];?></title>


<!--[if lt IE 7]>
<script type="text/javascript" src="js/jquery/jquery.js"></script>
<script type="text/javascript" src="js/jquery/jquery.dropdown.js"></script>
<![endif]-->
<script type="text/javascript" src="js/miscFunctions.js"></script>
</head>
<body>



<?php
$doc_id = $_GET['docid'];
	
	$query_prko = "SELECT prko.*, concat(ap.Surname, ' ', ap.FirstName, ' ', ap.LastName) fio
					FROM
					  prko, acc_persons ap
					WHERE
					  ap.Acc_PersonID = prko.supplierID
					  AND prko.NomDoc = $doc_id
					  AND prko.type = 0";
	//echo $query_prko;
	$result_prko = mysql_query($query_prko);
	$row = mysql_fetch_array($result_prko);
						$nomDoc = $row['NomDoc'];
						$korchet= 0;
						$data   = $row['DayOf'];
                        $sum    = $row['summa'];
						$credit = $sum;
						$debit  = $sum;
						$fio= $row['fio'];
						$doc_type = $_POST['doc_type'];
                        $formid = $_POST['formid'];
						$attach = mysql_real_escape_string($row['attachments']);
                          //SQL запрос для извлечения operationid и Contents_taj для основания
                          $operationSql="SELECT * FROM `operations` WHERE `Credit` = 1";
                          $operationResult = mysql_query($operationSql) or die('Error '.mysql_error());
                          $operationRow = mysql_fetch_assoc($operationResult);
                              $osnovanie = $operationRow['Contents_rus'];
                              $operationid =$operationRow['OperationID'];
							  $fio= trim($_POST['given_to']);
                              //$content = iconv('WINDOWS-1251','UTF-8',$content);
						$supplier_id = Supplier::insSupplier ( $fio );
						$personal_id = $_SESSION['user_id'];
                        //IN DocumentNumber int, IN Account int(korrespondentskiy schet), IN FIO varchar(200)(podotchetnoe lico) CHARACTER SET utf8, IN Contents varchar(200)(Osnovanie) CHARACTER SET utf8, IN Debit double, IN Credit double, IN Summa double, IN CURRENTDATA DATE, IN DocType Bool, IN FormID INT, IN OperationID INT
						//$rj_addQuery = ("call addtojurnal_test($nomDoc,$korchet,$supplier_id,$personal_id,'$osnovanie',$sum,'$data',$formid, $operationid,1,1);");
                        //debug mode
//						$insDocQuery = "INSERT INTO `documents` VALUES ($nomDoc, $formid, 0);";
//						$exeInsDocQuery = mysql_query($insDocQuery) or die(mysql_error);
// 
//  						$rj_addQuery = "INSERT INTO `prko` values (null,$nomDoc, $personal_id, $supplier_id, $sum, '$data', $operationid, 0, '$attach')";
//						
//						
//						writeTestData($rj_addQuery);
						$debug=0;
						if ($debug)
						{
							echo '№        : ' . $_POST['doc_number']  . '<br />';
							echo 'Кор.счет : ' . $_POST['coraccount']   . '<br />';
							echo 'Сана     : ' . $_POST['cur_date']     . '<br />';
							echo 'Аз ки    : ' . $_POST['given_to']      . '<br />';
							echo 'Сумма    : ' . $_POST['somoni']. '.'. $_POST['diram']. '<br />';
							echo 'Асос     : ' . $_POST['basis']. '<br />';
							echo 'Типи док : ' . $_POST['doc_type']  . '<br />';
							echo 'Форм ID  : ' . $_POST['formid']    . '<br />';
							echo 'Опер. ID : ' . $operationid        . "<br />";
							echo 'Основание: ' . $osnovanie          . "<br />";
							echo $operationSql;
							echo $operIDSql;
							echo "<br />Запрос :" . $rj_addQuery;
						}



//                    $res = @mysql_query($rj_addQuery) or die('My error:' . mysql_error());
//					if($res){
?>

<table border="1" cellpadding="1" cellspacing="0" width="500" style="border-collapse:collapse">
    <tr>
      <td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
        <tr>
          <td valign="top"><table border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="311" align="center" id="caption1"><u></u></td>
              <td width="181" align="center">Форма №&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            </tr>
            <tr>
              <td align="center"><em>(<?=$langs['enterprise'];?>, <?=$langs['org'];?> ) </em></td>
              <td>&nbsp;</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><table  border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
            <tr>
              <th   align="center" id="upcase"><?=$langs['expOrder'];?> № <?=$nomDoc;?></th>
            </tr>
            <tr>
              <td align="center"><table align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="152" align="center"><span class="main"><u>
                    <?=$data;?></u>
                  </span></td>
                  </tr>
                <tr>
                  <td class="unline" align="center">(<?=$langs['year'];?>, <?=$langs['month'];?>, <?=$langs['day'];?> ) </td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="49" align="center" valign="top"><table border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
                <tr>
                <th width="76" height="45" scope="col"> № таб.</th>
                <th width="114" scope="col"><?=$langs['korchet'];?></th>
                <th width="123" scope="col"><?=$langs['ramz'];?></th>
                <th width="82" scope="col"><?=$langs['sum'];?></th>
                <th width="94" scope="col"><?=$langs['ramz2'];?></th>
                </tr>
                <tr>
                  <td height="25" align="center"></td>
                  <td align="center"><?=$korchet;?></td>
                  <td align="center"></td>
                  <td align="center"><?=$sum;?></td>
                  <td align="center"></td>
                </tr>
              </table></td>
            </tr>




            <tr>
              <td height="17" valign="top"><table width="582" border="1" cellpadding="1" cellspacing="0" style="border:none" >
                <tr>
                  <td colspan="2" scope="col"><table width="582" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left">&nbsp;&nbsp;&nbsp;<strong><?=$langs['givento'];?></strong></td>
                    </tr>
                    <tr>
                      <td align="center"><u><?=$fio;?></u></td>
                    </tr>
                    <tr>
                      <td align="center"><em>( фамилия, ном, номи падар)</em></td>
                    </tr>
                  </table></td>
                </tr>
                <tr>
                  <td colspan="2" scope="col" align="left">&nbsp;&nbsp;&nbsp;<strong><?=$langs['basis'];?>: </strong><?=$osnovanie;?></td>
                </tr>
                <tr>
                  <td colspan="2" align="center"><strong><?=$sum;?></div></strong></td>
                </tr>
                
                <tr>
                  <td colspan="2" align="right"><?=$_POST['somoni'];?>
                    <strong><?=$sum;?> сом </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="2" align="left"><strong>&nbsp;&nbsp;&nbsp;<?=$langs['application'];?></strong>
                    <?=$_POST['attachments'];?></td>
                </tr>
                <tr>
                  <td colspan="2"><table width="513" border="0" cellspacing="0" cellpadding="4" align="center">
                    <tr>
                      <td align="left">&nbsp;&nbsp;&nbsp;<strong>Рохбар</strong> </td>
                      <td >&nbsp;&nbsp;&nbsp;<strong><?=$langs['glbuh'];?></strong></td>
                    </tr>
                    <tr>
                      <td align="left">&nbsp;&nbsp;&nbsp;<strong><?=$langs['accepted'];?></strong></td>
                      <td>&nbsp;&nbsp;&nbsp;<?=$sum;?>
                        <strong>сом</strong></td>
                    </tr>
                    <tr>
                      <td colspan="2" align="center"><strong><?=$_POST['sum_in_words'];?></strong></td>
                      </tr>
                    
                  </table></td>
                </tr>
                <tr>
                  <td >&nbsp;&nbsp;&nbsp;<strong><?=$langs['date'];?></strong>
                    <?=$data;?></td>
                  <td >&nbsp;&nbsp;&nbsp; <strong><?=$langs['signature'];?></strong> ________________________ </td>
                </tr>
                <tr>
                  <td colspan="2"><p>&nbsp;</p>
                    <table width="518" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="left">&nbsp;&nbsp;&nbsp;<strong><?=$langs['accordingto'];?></strong>
                          <em><?=$_POST['accordingto'];?></em></td>
                      </tr>
                      <tr>
                        <td><em>( ном, ракам, сана ва чои додани хуччате, ки шахсияти гирандаро тасдик мекунад )</em></td>
                      </tr>
                    </table>
                    <p>&nbsp;&nbsp;&nbsp;<strong><?=$langs['cashier'];?></strong> _________________________________<strong>додааст. </strong></p></td>
                </tr>
              </table></td>
            </tr>

          </table></td>
        </tr>
      </table></td>
      </tr>
  </table>
<?php
//----------------------------------------START OF FORM-------------------------------------------------------------------------*/
//}else{
//    echo "<div class='processDataBad'>Хатоги!!!</div>";
//}
//mysql_close();
?>
<script type="text/javascript">
// Do print the page
window.onload = function()
{
    if (typeof(window.print) != 'undefined')
        window.print();
}

</script>
</body>
</html>