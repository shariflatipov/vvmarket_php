<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../forms/css/template.css" rel="stylesheet" type="text/css" />
</head>
<?php
error_reporting(0);
session_start();
//if ($_SESSION['user_id'] != NULL || $_SESSION['user_id'] != "") {
    $id = $_GET['id'];

    include_once '../connectDB.php';
    connect();

    if ($id > 0) {

        $query = "SELECT concat(ac.surname, ' ', ac.first_name) AS fio
                         , su.company
                         , rd.id
                         , rd.reciept_date
                    FROM
                      receiptdoc rd, acc_persons ac, supplier su
                    WHERE
                      rd.supplier_id = su.id
                      AND rd.acc_person_id = ac.id
                      AND rd.id = ?";
        $stmt = mysqli_prepare($hconnect, $query);
        mysqli_stmt_bind_param($stmt, 's', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $fio, $company, $docId, $date);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);
?>
        <body >
            <table valign="top" align=center border="0" width="50%"   cellpadding="0" cellspacing="0">

                <tr>
                    <td>
                        <table border="0"  cellspacing="0" cellpadding="0" >
                            <tr>

                                <td width=80% align=right>Дата
                                    <INPUT size=20 maxLength=20 readonly="readonly" value="<?php echo $date; ?>" class="line" >
                                </td>

                            </tr>
                        </table>
                        <table border="0"  cellspacing="0" cellpadding="0" align=center >
                            <tr>
                                <th  class="main" align="center">Приходная Накладная №
                                    <input type="text" size="5" readonly="readonly" class="line" value=" <?php echo $docId ?>">
                                </th>
                            </tr>
                        </table>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>Продовец</td>
                                <td>
                                    <input type="text" readonly="readonly" size="60"   class="line"
                                           value=' <?php echo $fio ?>'>
                                </td>
                            </tr>
                            <tr>
                                <td>Покупатель</td>
                                <td>
                                    <input type="text" readonly="readonly" size="60"  class="line"
                                           value=" <?php echo $company ?>"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br/>
                        <table  width="100%" ALIGN=center   border="1" cellpadding="0" cellspacing="0" class="collapse">

                            <tr>
                                <th width="5%" >№ б/т</th>
                                <th width="50%">Наименование</th>
                                <th width="15%">Количество</th>
                                <th width="15%">Ед. изм</th>
                                <th width="15%" >Цена прихода</th>
                                <th width="15%" >Цена продажи</th>
                                <th width="15%" >Сумма</th>
                            </tr>

                    <?php
                    $sel_prod_from_doc = "SELECT pr.`name`
                                             , rd.price
                                             , rd.price_sell
                                             , ut.`name` uName
                                             , rd.quantity
                                        FROM
                                          product pr, units ut, receiptdocbyprod rd
                                        WHERE
                                          pr.barcode = rd.barcode
                                          AND pr.unit = ut.id
                                          AND rd.reciept_doc_id = ?";
                    if (!$stmt = mysqli_prepare($hconnect, $sel_prod_from_doc)) {
                        echo mysqli_stmt_error($stmt) . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    }
                    if (!mysqli_stmt_bind_param($stmt, "s", $id)) {
                        echo mysqli_stmt_error($stmt) . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    }
                    if (!mysqli_stmt_execute($stmt)) {
                        echo mysqli_stmt_error($stmt) . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    }
                    if (!mysqli_stmt_bind_result($stmt, $name, $price, $priceSell, $unit, $quantity)) {
                        echo mysqli_stmt_error($stmt) . "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    }

                    #debug($sel_prod_from_doc);
                    while (mysqli_stmt_fetch($stmt)) {
                        $subTotal = $quantity * $price;
                        $total += $subTotal;

                        echo '<tr><td align=center>' . ++$count . '</td>
			  <td align=center>' . $name . '</td>
			  <td align=center>' . $quantity . '</td>
			  <td align=center>' . $unit . '</td>
			  <td align=center>' . $price . '</td>
			  <td align=center>' . $priceSell . '</td>
			  <td align=center>' . $subTotal . '</td></tr>';
                    }

                    echo '<tr><td align=center>&nbsp;</td>
			  <td>&nbsp;Итого</td>

			  <td align=center></td>
			  <td align=center></td>
			  <td align=center></td>
			  <td align=center></td>
			  <td align=center>' . $total . '</td></tr>';
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <br/><br/>
                <table width="80%" border="0" cellspacing="0" cellpadding="1">
                    <tr>
                        <td> Поставщик  ______________________</td>
                        <td> Сдал       ______________________</td>
                    </tr>
                    <tr>
                        <td> Складовщик _____________________</td>
                        <td> Принял     ___________________</td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <script type="text/javascript">
        // Do print the page
        window.onload = function()
        {
            if (typeof(window.print) != 'undefined')
                window.print();
        }

    </script>
</body>
<?php //}
 } ?>