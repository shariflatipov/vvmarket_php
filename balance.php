﻿
<?php
	/*   */
	#include_once "template.inc";
	include_once "connectDB.php";
	include_once "functions.php";
	include_once "languages.php";

	$_txtDate = $_REQUEST['txtDate'];
	$_date = $_REQUEST['date'];
	$_account = $_REQUEST['account'];
	
	if(!isset($_txtDate)) $_txtDate = date('Y-m-d');
	
	/* Filters */
	if(isset($_date) && $_date == 'day' || !isset($_date) || $_date == '' ) {
		$_date = 'day';
		$filter_date = "`Date` = '$_txtDate'";
	}
	elseif(isset($_date) && $_date == 'month')
		$filter_date = "`Month` = MONTH('$_txtDate') AND `Year` = YEAR('$_txtDate')";
	elseif(isset($_date) && $_date == 'year')
		$filter_date = "`Date` = YEAR('$_txtDate')";

	if( !isset($_account) || $_account == 'all' )
		$filter_account = '';
	else
		$filter_account = " LEFT( t.`Account`, 3 ) = LEFT( $_account, 3) AND ";
	
?>


<table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
<form name="frmBalance" method="post">

<tr>
<?php
	echo '<td><table border="0" width=100%  class="collapse"><tr><td>' . $lang[$language.'_Period'] . '</td><td>
		  <INPUT size=10 maxLength=10 readonly="readonly" name="txtDate" 
		  value="' . $_txtDate . '"  
		  onclick="displayCalendar(document.frmBalance.txtDate,\'yyyy-mm-dd\', this)" 
		  onchange="frmBalance.submit()"></td><td>';


	createSelectDate( 'frmBalance' );

	echo "</td><td>" . $lang[$language.'_Account'] . "</td><td>";

	$account_query = 'SELECT * FROM `Accounts` WHERE `Dependent` = 0';
	createSelect( 'account', 'width: 250px', 
				  'onchange="frmBalance.submit()"',
				  '' ,$account_query, 0, 'AccountID' );	

	echo "<td width=100% align=right >	
			  <span onmouseout='nd();' onmouseover=\"return overlib('<b>" . 
			  $lang[$language.'_Print'] . "</b>', WIDTH, 150)\">
		<a style='cursor: hand' onclick=\"window.open('print_view/balance.php?date=". 
	$_date ."&txtDate=". $_txtDate ."&account=". $_account ."',
		'', '')\">
			  <img src='images/print.bmp'></a></span></td>";			
/*
	echo "</td><td><INPUT type='button' value='" . $lang[$language.'_Print'] . "'
 		  onclick=\"window.open('print_view/balance.php?date=". 
		  $_date ."&txtDate=". $_txtDate ."&account=". $_account ."',
		  '', '')\"></td><td>";	*/			
?>

</td></table></td>
</tr>

</form>
<?php


	$balance_query = "SELECT * 
				 FROM 
					 `Total_". $_date ."` t, 
					 `Accounts` a
				WHERE t.`Account` = a.`AccountID` 
				  AND " . $filter_account . $filter_date .
		   " ORDER BY t.`Account`";

	mysql_query('call `full_Total_tables`();');	
	$hbalance_query = mysql_query($balance_query) or debug(mysql_error());
	echo '<tr><td><table ALIGN=center width=90%  border="1" cellpadding="5" cellspacing="0" class="silver">';
	echo "<tr class=rh><td rowSpan=2 width=20%>" . $lang[$language.'_Name_Accounts'] . "</td>
			<td colSpan=2 width=22%>" . $lang[$language.'_SBalance'] . "</td>
			<td colSpan=2 width=22%>" . $lang[$language.'_Oborot'] . "</td>
			<td colSpan=2 width=22%>" . $lang[$language.'_EBalance'] . "</td></tr>";

	echo "<tr  class=rh>
			<td width=11%>" . $lang[$language.'_Debit'] . "</td>
			<td width=11%>" . $lang[$language.'_Credit'] . "</td>
			<td width=11%>" . $lang[$language.'_Debit'] . "</td>
			<td width=11%>" . $lang[$language.'_Credit'] . "</td>
			<td width=11%>" . $lang[$language.'_Debit'] . "</td>
			<td width=11%>" . $lang[$language.'_Credit'] . "</td></tr>";
	$total_sb_cred = $total_sb_deb = $total_eb_cred = $total_eb_deb = 0;  
	while( $row = mysql_fetch_array($hbalance_query) ) {
		$i++;
		$i %= 2;
		$bgcolor = ($i ? 'lightyellow' : 'white');
		
		
		echo "<tr bgcolor=".$bgcolor.">
			  <td align=center title='". $row['Name_rus']. "'>
			  <span onmouseout='nd();' onmouseover=\"return overlib('<b>". $row['Name_' . $language]. "</b>', WIDTH, 250)\">"
			  . getAccountF( $row['Account'] ) . "</Td>";		
			
		$sb_cred = (double)( $row['SBalance_cred'] <= 0 ? 0 : $row['SBalance_cred'] );
		$sb_deb = (double)( $row['SBalance_deb'] <= 0 ? 0 : $row['SBalance_deb'] );

		$sb_cred_abs = (double)( $row['SBalance_cred'] <= 0 ? -$row['SBalance_cred'] : 0 );
		$sb_deb_abs = (double)( $row['SBalance_deb'] <= 0 ? -$row['SBalance_deb'] : 0 );

		
		$eb_cred = (double)( $row['EBalance_cred'] <= 0 ? 0 : $row['EBalance_cred'] );
		$eb_deb = (double)( $row['EBalance_deb'] <= 0 ? 0 : $row['EBalance_deb'] );

		$eb_cred_abs = (double)( $row['EBalance_cred'] <= 0 ? -$row['EBalance_cred'] : 0 );
		$eb_deb_abs = (double)( $row['EBalance_deb'] <= 0 ? -$row['EBalance_deb'] : 0 );
				
		if( ( $sb_deb + $sb_cred_abs ) == 0) echo "<td>&nbsp;</td>";
		else echo "<td>&nbsp;". ( $sb_deb + $sb_cred_abs ) ."</td>";
		if( ( $sb_cred + $sb_deb_abs ) == 0) echo "<td>&nbsp;</td>";
		else echo "<td>&nbsp;". ( $sb_cred + $sb_deb_abs ) ."</td>";
	
		if( $row['Oborot_deb'] == 0) echo "<td>&nbsp;</td>";
		else echo "<td>&nbsp;". $row['Oborot_deb'] ."</td>";
		if( $row['Oborot_cred'] == 0) echo "<td>&nbsp;</td>";
		else echo "<td>&nbsp;". $row['Oborot_cred'] ."</td>";
		
		if( ( $eb_deb + $eb_cred_abs ) == 0) echo "<td>&nbsp;</td>";
		else echo "<td>&nbsp;". ( $eb_deb + $eb_cred_abs ) ."</td>";
		if( ( $eb_cred + $eb_deb_abs ) == 0) echo "<td>&nbsp;</td>";
		else echo "<td>&nbsp;". ( $eb_cred + $eb_deb_abs ) ."</td></tr>";
		$total_eb_cred += ( $eb_cred + $eb_deb_abs );		
		$total_eb_deb += ( $eb_deb + $eb_cred_abs );
		$total_sb_cred += ( $sb_cred + $sb_deb_abs );		
		$total_sb_deb += ( $sb_deb + $sb_cred_abs );
		
	}

	echo "<tr><th>" . $lang[$language.'_Total'] . "</th>";

	$sum_query = "SELECT 
						SUM(`SBalance_deb`) SB_deb, 
						SUM(`SBalance_cred`) SB_cred, 
						SUM(`Oborot_deb`) O_deb,
						SUM(`Oborot_cred`) O_cred,
						SUM(`EBalance_deb`) EB_deb, 
						SUM(`EBalance_cred`) EB_cred 
					FROM `Total_". $_date ."` t
				   WHERE " . $filter_account . $filter_date;

	$hsum_query = mysql_query($sum_query);
	$sum = mysql_fetch_array( $hsum_query );
/*
	if( $sum['SB_deb'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['SB_deb'] ."</th>";
	if( $sum['SB_cred'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['SB_cred'] ."</th>";
	if( $sum['O_deb'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['O_deb'] ."</th>";
	if( $sum['O_cred'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['O_cred'] ."</th>";
	if( $sum['EB_deb'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['EB_deb'] ."</th>";
	if( $sum['EB_cred'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['EB_cred'] ."</th></tr>";
*/
	if( $total_sb_deb == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $total_sb_deb ."</th>";
	if( $total_sb_cred == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $total_sb_cred ."</th>";
	if( $sum['O_deb'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['O_deb'] ."</th>";
	if( $sum['O_cred'] == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $sum['O_cred'] ."</th>";
	if( $total_eb_deb == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $total_eb_deb ."</th>";
	if( $total_eb_cred == 0) echo "<th>&nbsp;</th>";
	else echo "<th>". $total_eb_cred ."</th></tr>";

echo "</TABLE>"; 


?>
</tr></table>

</body>