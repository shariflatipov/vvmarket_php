<?php

function mysql_xml_error($trn, $error, $status) {
    $xml = new DOMDocument("1.0", "utf-8");
    $magazin = $xml->createElement("magazin");
    $trn = $xml->createElement("trn", $trn);
    $status = $xml->createElement("status", $status);
    $description = $xml->createElement("description", $error);
    $magazin->appendChild($trn);
    $magazin->appendChild($status);
    $magazin->appendChild($description);
    $xml->appendChild($magazin);
    echo $xml->saveXML();
}

function libxml_display_error($error) {
    $return = "<br/>\n";

    switch ($error->level) {
        case LIBXML_ERR_WARNING :
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR :
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL :
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }

    $return .= trim($error->message);

    if ($error->file) {
        $return .= " in <b>$error->file</b>";
    }

    $return .= " on line <b>$error->line</b>\n";

    return $return;
}

function libxml_display_errors() {
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        print libxml_display_error($error);
    }
    libxml_clear_errors();
}

function validation($xml_load, $xsd_path) {
    // Enable user error handling 
    libxml_use_internal_errors(true);

    $xml = new DOMDocument ();
    $xml->loadXML($xml_load);

    $xml_trn = simplexml_load_string($xml_load);
    $sold_attribute = $xml_trn->sold->attributes();
    $id_sold = (int) $sold_attribute->id;

    if (!$xml->schemaValidate($xsd_path)) {
        //print '<b>DOMDocument::schemaValidate() Generated Errors!</b>' ;
        mysql_xml_error($id_sold, 'Validation error ', 8);
        exit();
    } else {
        return "validated";
        //echo "validated" ; 
    }
}

?>