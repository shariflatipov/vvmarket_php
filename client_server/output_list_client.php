<?php

include_once '../connectDB.php';
connect();

$xml = simplexml_load_string($_REQUEST['myxml']);

$seller_attributes = $xml->seller->attributes();
$login = (string) $seller_attributes->login;
$checksum = (string) $xml->seller->checksum;
$date = (string) $xml->seller->date;
$update = (string) $xml->seller->update;

$query_users = "SELECT cs.cash_id, ai.`password` , ap.`department` 
			FROM auth_info ai, acc_persons ap, cash cs
			WHERE ap.authinfo_id = ai.id
			AND ai.login = ?
			AND ap.id = cs.acc_person_id";
//echo $query_users;

$stmt = mysqli_prepare($hconnect, $query_users);
mysqli_stmt_bind_param($stmt, 's', $login);

if (!mysqli_stmt_execute($stmt)) {
    $errorLine = 42;
    $error = 1;
    $errorDescription = mysqli_stmt_error($stmt);
    echo '$errorDescription 44 ' . $errorDescription;
}

mysqli_stmt_bind_result($stmt, $id_user, $pass, $department);
mysqli_stmt_fetch($stmt);
mysqli_stmt_close($stmt);


if ($id_user > 0) {
    $string = sha1($date . "#" . $login . "#" . $pass);
    //$string = sha1(1);
}else {
    mysql_xml_error($id_sold, "Login error", 7);
    exit();
}

if ($string == $checksum && $update == 'get') {
    $query = "SELECT * FROM update_cash WHERE cash_id = $id_user";
    //echo $query;
    $result = mysqli_query($hconnect, $query);

    if ($result) {
        $xml = new DOMDocument("1.0", "utf-8");
        $magazin = $xml->createElement("magazin");


        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $product = $xml->createElement("product");
            $product->setAttribute("id", $row['barcode']);

            $name = $xml->createElement("name", $row['product_name']);
            $price = $xml->createElement("price", $row['price']);
            $price_sell = $xml->createElement("price_sell", $row['price']);
            $unit = $xml->createElement("unit", $row['unit']);
            $image_source = $xml->createElement("image_source", $row['img_source']);
            $flag = $xml->createElement("flag", $row['flag']);
            $is_list = $xml->createElement("is_list", $row['is_list']);
            $depend = $xml->createElement("depend", $row['dependent']);

            $product->appendChild($name);
            $product->appendChild($price);
            $product->appendChild($price_sell);
            $product->appendChild($unit);
            $product->appendChild($image_source);
            $product->appendChild($flag);
            $product->appendChild($is_list);
            $product->appendChild($depend);
            $magazin->appendChild($product);
        }
        $xml->appendChild($magazin);
        $xml->formatOutput = true;
        $result_XML = $xml->saveXML();
        if ($result_XML) {
            $upd_query = "UPDATE update_cash SET status = 1 WHERE cash_id = $id_user";
            $result = mysqli_query($hconnect, $upd_query);
            echo $result_XML;
        }
    } else {
        $xml = new DOMDocument("1.0", "utf-8");
        $magazin = $xml->createElement("magazin");
        $status = $xml->createElement("status", 5);
        $description = $xml->createElement("description", "No update");
        $magazin->appendChild($status);
        $magazin->appendChild($description);
        $xml->appendChild($magazin);
        echo $xml->saveXML();
    }
} elseif ($string == $checksum && $update == 'ok') {
    $query = "DELETE FROM update_cash WHERE cash_id = $id_user AND status = 1";
    $result = mysqli_query($hconnect, $query);
    if ($result) {
        $xml = new DOMDocument("1.0", "utf-8");
        $magazin = $xml->createElement("magazin");
        $status = $xml->createElement("status", 6);
        $description = $xml->createElement("description", "OK");
        $magazin->appendChild($status);
        $magazin->appendChild($description);
        $xml->appendChild($magazin);
        echo $xml->saveXML();
    } else {
        $xml = new DOMDocument("1.0", "utf-8");
        $magazin = $xml->createElement("magazin");
        $status = $xml->createElement("status", 7);
        $description = $xml->createElement("description", "Delete error");
        $magazin->appendChild($status);
        $magazin->appendChild($description);
        $xml->appendChild($magazin);
        echo $xml->saveXML();
    }
}
?>