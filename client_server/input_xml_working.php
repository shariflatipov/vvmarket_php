<?php

include_once '../connectDB.php';
include 'functions.php';
connect();
$_date = date('Y-m-d');
$xml = simplexml_load_string($_REQUEST['myxml']);
//validation($_REQUEST['myxml'], 'magazin.xsd');

//seller
$seller_attributes = $xml->seller->attributes();
$login = (string) $seller_attributes->login;
$checksum = (string) $xml->seller->checksum;

//sold
$sold_attribute = $xml->sold->attributes();
$id_sold = (int) $sold_attribute->id;
$sum = (double) $sold_attribute->sum;
$date = (string) $sold_attribute->date;

//products
$i = 0;
foreach ($xml->sold->product as $key => $child) {
    $product['id'][$i] = (string) $child->attributes()->id;
    $product['count'][$i] = (double) str_replace(",", ".", $child->count);
    $product['price'][$i] = (double) str_replace(",", ".", $child->price);
    $i++;
}

$query_users = "SELECT ap.id, ai.`password`, ap.`department`
                    FROM
                      auth_info ai, acc_persons ap
                    WHERE
                      ap.authinfo_id = ai.id
                      AND ai.login = ?";


$stmt = mysqli_prepare($hconnect, $query_users);
mysqli_stmt_bind_param($stmt, 's', $login);

if (!mysqli_stmt_execute($stmt)) {
    $errorLine = 42;
    $error = 1;
    $errorDescription = mysqli_stmt_error($stmt);
    echo '$errorDescription 44 ' . $errorDescription;
}

mysqli_stmt_bind_result($stmt, $id_user, $pass, $department);
mysqli_stmt_fetch($stmt);
mysqli_stmt_close($stmt);
//echo $query_users;
if ($id_user > 0) {
    $string = sha1($date . "#" . $login . "#" . $pass);
} else {
    mysql_xml_error($id_sold, "Login error", 7);
    exit();
}

if ($string == $checksum) {
    $sel_trn_id = "SELECT count(*) FROM cash_transactions WHERE trn_id = ? AND user_id = ?";
    $stmt = mysqli_prepare($hconnect, $sel_trn_id);
    //echo "SELECT count(*) FROM cash_transactions WHERE trn_id = {$id_sold} AND user_id = {$id_user}";
    mysqli_stmt_bind_param($stmt, 'ss', $id_sold, $id_user);
    if (!mysqli_stmt_execute($stmt)) {
        $errorLine = 65;
        $error = 1;
        $errorDescription = mysqli_stmt_error($stmt);
        echo '$errorDescription 64 = ' . $errorDescription;
    }

    mysqli_stmt_bind_result($stmt, $countTrans);
    mysqli_stmt_fetch($stmt);

    //echo $countTrans;
    if ($countTrans > 0) {
        mysql_xml_error($id_sold, "OK", 2);
        exit();
    }

    mysqli_stmt_close($stmt);

    mysqli_autocommit($hconnect, FALSE);


    //$query = "INSERT INTO expensedoc VALUES (id, acc_person_id, expence_date, reciepter_id, total_sum);";
    $query = "INSERT INTO expensedoc VALUES (null, ?, ?, 0, 0);";

    $stmt = mysqli_prepare($hconnect, $query);
    mysqli_stmt_bind_param($stmt, 'ss', $id_user, $date);
    if (!mysqli_stmt_execute($stmt)) {
        $errorLine = 91;
        $error = 1;
        $errorDescription = mysqli_stmt_error();
        //echo '$errorDescription 87 ' . $errorDescription;
    }
    $maxId = mysqli_stmt_insert_id($stmt);
    mysqli_stmt_close($stmt);

    $query = "INSERT INTO cash_transactions VALUES (null, ?, ?, 0);";

    $stmt = mysqli_prepare($hconnect, $query);
    mysqli_stmt_bind_param($stmt, 'ss', $id_user, $id_sold);
    if (!mysqli_stmt_execute($stmt)) {
        $errorLine = 104;
        $error = 1;
        $errorDescription = mysqli_stmt_error();
        //echo '$errorDescription 98 ' . $errorDescription;
    }
    mysqli_stmt_close($stmt);

    /*
     * Block start insert expense by one
     */
    for ($i = 0; $i < count($product['id']); $i++) {
        $total += $product['count'][$i] * $product['price'][$i];
        $productId = 0;
        $productReceiptPrice = 0;

        $query_products = "select pr.id, wh.receipt_price from product pr, warehouse wh where wh.product_id = pr.id and pr.barcode = ? limit 1";

        $stmt = mysqli_prepare($hconnect, $query_products);
        mysqli_stmt_bind_param($stmt, 's', $product['id'][$i]);
        if (!mysqli_stmt_execute($stmt)) {
            $errorLine = 124;
            $error = 1;
            $errorDescription = mysqli_stmt_error();
            //echo '$errorDescription 116 ' . $errorDescription;
        }
        mysqli_stmt_bind_result($stmt, $productId, $productReceiptPrice);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);


        // Product doesn't exists in database
        if ($productId <= 0) {
            //Insert new product to table product

            $productQuantity = 0 - $product['count'][$i];

            $qInsertProduct = "insert into product values (null, ?, null, null, null, null, ?, 'Unknown', ?, 1, 0)";

            //echo $qInsertProduct;
            $stmt = mysqli_prepare($hconnect, $qInsertProduct);
            mysqli_stmt_bind_param($stmt, 'sss', $product['id'][$i], $productQuantity, $product['price'][$i]);
            if (!mysqli_stmt_execute($stmt)) {
                $errorLine = 145;
                $error = 1;
                $errorDescription = mysqli_stmt_error($stmt);
                //echo '$errorDescription 132 ' . $errorDescription;
            }
            mysqli_stmt_store_result($stmt);
            $productId = mysqli_stmt_insert_id($stmt);
            echo $productId . '$productId';
            mysqli_stmt_close($stmt);



            //Insert new product into warehouse
            $qInsertWarehouse = "insert into warehouse values(null, ?, ?, 0, ?)";

            $stmt = mysqli_prepare($hconnect, $qInsertWarehouse);

            mysqli_stmt_bind_param($stmt, 'sss', $department, $productQuantity, $productId);
            if (!mysqli_stmt_execute($stmt)) {
                $errorLine = 164;
                $error = 1;
                $errorDescription = mysqli_stmt_error($stmt);
                //echo '$errorDescription 145 ' . $errorDescription;
            }
            mysqli_stmt_close($stmt);
        } else { //Product exists in database so i need just update quantity
            //Update product
            $qUpdateProduct = "update product set quantity = (quantity - ?) where id = ?";

            $stmt = mysqli_prepare($hconnect, $qUpdateProduct);
            mysqli_stmt_bind_param($stmt, 'ss', $product['count'][$i], $productId);
            if (!mysqli_stmt_execute($stmt)) {
                $errorLine = 173;
                $error = 1;
                $errorDescription = mysqli_stmt_error($stmt);
                //echo '$errorDescription 156 ' . $errorDescription;
            }
            mysqli_stmt_close($stmt);

            //Update warehouse
            $qUpdateWarehouse = "update warehouse set  quantity = (quantity - ?) where product_id = ?";

            $stmt = mysqli_prepare($hconnect, $qUpdateWarehouse);
            mysqli_stmt_bind_param($stmt, 'ss', $product['count'][$i], $productId);
            if (!mysqli_stmt_execute($stmt)) {
                $errorLine = 186;
                $error = 1;
                $errorDescription = mysqli_stmt_error($stmt);
                //echo '$errorDescription 167 ' . $errorDescription;
            }
            mysqli_stmt_close($stmt);
        }

        $qSelectReceiptPrice = "select receipt_price from warehouse where product_id = {$productId}";
        if (!$result = mysqli_query($hconnect, $qSelectReceiptPrice)) {
            echo mysqli_error();
        }
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $receiptPrice = $row['receipt_price'];

        if ($receiptPrice == NULL || $receiptPrice == "") {
            $receiptPrice = 0;
        }

        //insert product into expense document by product
        $qInsertExpByProd = "insert into expensedocbyprod values(null, ?, ?, ?, ?, ?)";

        $stmt = mysqli_prepare($hconnect, $qInsertExpByProd);
        mysqli_stmt_bind_param($stmt, 'sssss', $product['price'][$i], $productId, $product['count'][$i], $maxId, $receiptPrice);
        if (!mysqli_stmt_execute($stmt)) {
            $errorLine = 211;
            $error = 1;
            $errorDescription = mysqli_stmt_error($stmt);
            //echo '$errorDescription 178 ' . $errorDescription;
        }
        mysqli_stmt_close($stmt);

        $qUpdateExpenseDoc = "update expensedoc set  total_sum = ? where id = ?";

        $stmt = mysqli_prepare($hconnect, $qUpdateExpenseDoc);
        mysqli_stmt_bind_param($stmt, 'ss', $total, $maxId);
        if (!mysqli_stmt_execute($stmt)) {
            $errorLine = 223;
            $error = 1;
            $errorDescription = mysqli_stmt_error($stmt);
            //echo '$errorDescription 167 ' . $errorDescription;
        }
        mysqli_stmt_close($stmt);
    }
    if ($error != 1) {
        mysqli_commit($hconnect);
        mysql_xml_error($id_sold, "OK", 2);
    } else {
        mysqli_rollback($hconnect);
        mysql_xml_error($id_sold, "Line : " . $errorLine . $errorDescription, 6);
    }
}
?>