﻿<?php
/* ------------------------------------------------------------------------------------------------------------- */
/* Add operation */
#include_once "template.inc";;
include_once "connectDB.php";
include_once "functions.php";
include_once "languages.php";

?>
<table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">

<tr>
<td>
<?php


	$form_name = 'frmProduct';

/* Variables for groups */
	$_task = $_REQUEST['task'];
/* Variables for product */
	$_product_name = $_REQUEST['txtProductName'];


	/* Variables  for filters */
	$_fmark = trim( $_REQUEST['fmark']);
	$_fquantity = $_REQUEST['fquantity'];
	$_funit = $_REQUEST['funit'];
	$_fprice = $_REQUEST['fprice'];
	$_fsupplier = $_REQUEST['fsupplier'];
	$_facc_person = $_REQUEST['facc_person'];
	$_fdatestart = $_REQUEST['datestart'];
	$_fdateend = $_REQUEST['dateend'];



	if( !isset($_fdatestart) || $_fdatestart == '' ) $_fdatestart = date('Y-m-d');
	if( !isset($_fdateend) || $_fdateend == '' ) $_fdateend = date('Y-m-d');

	$sel_date = " AND prod.`Date` BETWEEN '{$_fdatestart}' AND '{$_fdateend}'";

	if( !isset($_fmark) || $_fmark == '' ) $sel_mark = '';
	else $sel_mark = " AND prod.`Name_product` LIKE '%{$_fmark}%'";

	if( !isset($_fquantity) || $_fquantity == 'all' ) $sel_quantity = '';
	else $sel_quantity = " AND prod.`Quantity` = {$_fquantity}";

	if( !isset($_funit) || $_funit == 'all' ) $sel_unit = '';
	else $sel_unit = " AND prod.`UnitID` = {$_funit}";

	if( !isset($_fprice) || $_fprice == 'all' ) $sel_price = '';
	else $sel_price = " AND prod.`Price` = {$_fprice}";

	if( !isset($_fsupplier) || $_fsupplier == 'all' ) $sel_supplier = '';
	else $sel_supplier = " AND prod.`SupplierID` = {$_fsupplier}";

	if( !isset($_facc_person) || $_facc_person == 'all' ) $sel_acc_person = '';
	else $sel_acc_person = " AND prod.`Acc_PersonID` = {$_facc_person}";

	$filters = $sel_mark . " " . $sel_quantity . " " . $sel_date . " " . $sel_unit . " " . $sel_price . " " . $sel_supplier . " " . $sel_acc_person;

	$count_query = "SELECT COUNT(`ProductID`) FROM `Products`;";

	$limit = createLimitRecord( $_epos, $_spos, $up_disabled, $down_disabled, $count_query );


	$product_disabled  = 'disabled';
	$group_disabled = 'disabled';


?>
<form name="frmProduct"  method="post">

от
<INPUT size=10 maxLength=10 readonly='readonly' name='datestart' class='line' value=<?= $_fdatestart?> onclick='displayCalendar(document.frmProduct.datestart, "yyyy-mm-dd", this)'/>
до
<INPUT size=10 maxLength=10 readonly='readonly' name='dateend' class='line' value=<?= $_fdateend?> onclick='displayCalendar(document.frmProduct.dateend, "yyyy-mm-dd", this)' onchange='this.form.submit()'/>
</td>

<td>
<?php
	createNavigateRecord( $_epos, $_spos, $up_disabled, $down_disabled, $form_name );
?>
</td>

</tr>
</table>



</td>
</tr>
<?php
/* ------------------------------------------------------------------------------------------------------------- */
/* Show products */
?>
<tr>

<td>	

<?php


echo '<table ALIGN=center width=100%  border="1" cellpadding="2" cellspacing="0" class="silver">';


/*$sel_prod_query = "SELECT rd.ReceiptsDocID
	 , rd.Acc_PersonID
	 , rd.`SupplierID`
     , rd.`Date`
     , concat(ap.Surname, ' ', ap.LastName, ' ', ap.FirstName) fioAp
     , concat(sp.`Name`) `Name_s`
     , rdp.MarkID
     , rdp.Price
     , rdp.Quantity
     , ut.`Name_{$language}` `Name_u`
     , ut.UnitID
     , mk.`Name` `Name_product`
FROM
  receiptsdoc rd, acc_persons ap, suppliers sp, receiptsdocprod rdp, marks mk, units ut
WHERE
  rd.Acc_PersonID = ap.Acc_PersonID
  AND
  rd.SupplierID = sp.SupplierID
  AND
  rdp.ReceiptsDocID = rd.ReceiptsDocID
  AND
  rdp.MarkID = mk.MarkID
  AND
  rdp.Unit = ut.UnitID
";*/

$sel_prod_query = "SELECT rd.ReceiptsDocID
     , rd.Acc_PersonID
     , rd.`SupplierID`
     , rd.`Date`
     , concat(ap.Surname, ' ', ap.LastName, ' ', ap.FirstName) fioAp
     , concat(sp.Surname, ' ', sp.LastName, ' ', sp.FirstName) `Name_s`
     , rdp.MarkID
     , rdp.Price
     , rdp.Quantity
     , ut.`Name_{$language}` `Name_u`
     , ut.UnitID
     , mk.`Name` `Name_product`
FROM
  receiptsdoc rd, acc_persons ap, acc_persons sp, receiptsdocprod rdp, marks mk, units ut
WHERE
  rd.Acc_PersonID = ap.Acc_PersonID
  AND
  rd.SupplierID = sp.Acc_PersonID
  AND
  rdp.ReceiptsDocID = rd.ReceiptsDocID
  AND
  rdp.MarkID = mk.MarkID
  AND
  rdp.Unit = ut.UnitID";


$sel_prod_query = 
	"SELECT * 
		FROM ({$sel_prod_query}) `prod`
		WHERE  1 " . $filters . $limit;
				
debug($sel_prod_query);
$_hquery = mysql_query($sel_prod_query) or die("ERROR " . mysql_error());

/* --------------------------------------------------------------------------------------------- */
/* Create Filter for Quantity */
$event = 'onchange="' . $form_name . '.submit()"';

echo "<tr class=rh><td>";
echo "<input name=fmark id='product_selection' value='{$_fmark}' style ='width: 200px' {$event}></td><td>";

$create_filter_quantity_query = 
	"SELECT * 
	   FROM ({$sel_prod_query}) `prod`
	  WHERE  1 " . $filters . "
   GROUP BY prod.`Quantity`;";
debug( $create_filter_quantity_query );

createSelect( 'fquantity', '', $event, '', $create_filter_quantity_query, 2, 'Quantity' );

/* Create Filter for Unit */
echo "</td><td>";
$create_filter_unit_query = 
	"SELECT * 
	   FROM ({$sel_prod_query}) `prod`
	  WHERE  1 " . $filters . "
   GROUP BY prod.`UnitID`;";

debug( $create_filter_unit_query );

createSelect( 'funit', '', $event, '', $create_filter_unit_query, 2, 'Name_u' );

/* Create Filter for Price */
echo "</td><td>";
$create_filter_price_query = 
	"SELECT * 
	   FROM ({$sel_prod_query}) `prod`
	  WHERE  1 " . $filters . "
   GROUP BY prod.`Price`;";

debug( $create_filter_price_query );

createSelect( 'fprice', '', $event, '', $create_filter_price_query, 2, 'Price' );

/* Create Filter for Supplier */
echo "</td><td></td><td>";
$create_filter_supplier_query = 
	"SELECT * 
	   FROM ({$sel_prod_query}) `prod`
	  WHERE  1 " . $filters . "
   GROUP BY prod.`SupplierID`;";

debug( $create_filter_supplier_query );

createSelect( 'fsupplier', '', $event, '', $create_filter_supplier_query, 2, 'Name_s' );

/* Create Filter for Acc_person */
echo "</td><td>";
$create_filter_acc_person_query = 
	"SELECT * 
	   FROM ({$sel_prod_query}) `prod`
	  WHERE  1 " . $filters . "
   GROUP BY prod.`Acc_PersonID`;";

debug( $create_filter_acc_person_query );

createSelect( 'facc_person', '', $event, '', $create_filter_acc_person_query, 2, 'Acc_PersonID' );
echo "</td><td></td><td></td>";
/* --------------------------------------------------------------------------------------------- */
echo '<tr class=rh>
		<td>' . $lang[$language.'_Name'] . '</td>
		<td>' . $lang[$language.'_Quantity'] . '</td>
		<td>' . $lang[$language.'_Unit'] . '</td>
		<td>' . $lang[$language.'_Price'] . '</td>
		<td>' . $lang[$language.'_Sum'] . '</td>
		<td>' . $lang[$language.'_Supplier'] . '</td>
		<td>' . $lang[$language.'_Acc_Person'] . '</td>
		<td width=21%>' . $lang[$language.'_Date'] . '</td>
		<td>' . $lang[$language.'_Document'] . '</td>
		</tr>';
		$tprice = $ttprice=0;
while( $row = mysql_fetch_array($_hquery) ) {
	$i++;
	$i %= 2;
	$bgcolor = ($i ? 'lightyellow' : 'white');
	
	$tprice += $row['Price'];
	$ttprice += $row['Quantity'] * $row['Price'];
	echo "<tr bgcolor=".$bgcolor.">"; 
	echo "<td valign=top>&nbsp;&nbsp;". $row['Name_product'] ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;".  $row['Quantity'] *1 ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;".  $row['Name_u'] ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;".  $row['Price'] ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;". ( $row['Quantity'] * $row['Price'] ) ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;". $row['Name_s'] ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;". $row['fioAp'] ."</td>";
	echo "<td valign=top>&nbsp;&nbsp;". $row['Date'] ."</td>";
	echo "<td align=center><a style='cursor: hand' onclick=\"window.open('load_doc.php?formid=1&docid=". $row['ReceiptsDocID'] ."',
		'', 'dependent,width=700,height=700,left=0 ,top=0')\">";
	echo "<img src='images/DOC.BMP'></td>";			
}
		echo "<Tr><Th>" . $lang[$language.'_Total'] . "</Th>
					<Th></Th><Th></Th>
					<Th>" . $tprice . "</Th>
					<Th>" . $ttprice . " </Th><Th></Th><Th></Th><Th></Th><Th></Th></tr>";
					#<Th>" . ( $tosprice - $twprice ) . "</Th></tr>";
echo "</TABLE>"; 
?>



</td>

</td></tr>
</TABLE>
</form>
