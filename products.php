<?php
$_product = $_REQUEST['product'];
$_barcode = $_REQUEST['barcode'];
$_task = $_REQUEST['task'];
if (isset($_REQUEST['id'])) {
    $id = $_REQUEST['id'];
}

if (isset($_task) && $_task == 'delete_product') {

    $stmt = mysqli_prepare($hconnect, "delete from product where id = ?");
    mysqli_stmt_bind_param($stmt, 's', $id);
    if (!mysqli_stmt_execute($stmt)) {
        echo "<h1>" . mysqli_stmt_error($stmt) . "<h1>";
    }
    if (mysqli_stmt_affected_rows($stmt) > 0) {
        echo '<h2>Продукт успешно удален</h2>';
    }
}
?>

<form action="" method="post">
    <table  width="100%" ALIGN=center   border="1" cellpadding="0" cellspacing="0" class="collapse">

        <tr>
            <td><input type="text" id="single_product" name="product" value="<?php echo $_product; ?>" size="65"/></td>
            <td><input type="text" id="barcode" name="barcode" value="<?php echo $_barcode; ?>"/></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><input type="submit" value="Фильтр"/></td>
        </tr>
        <tr>
            <td>Наименование</td>
            <td>Штрихкод</td>
            <td>Внутренний код</td>
            <td>Мин. кол-во</td>
            <td>Ед. измер.</td>
            <td>Количество</td>
            <td>Цена</td>
            <td>Сумма</td>
            <td>Редактирование</td>
            <td>Удаление</td>
        </tr>

        <?php
        if (isset($_product) && $_product != "") {
            $product = "%" . $_product . "%";
            $filterProduct = " and pr.name like ? ";
        }
        if (isset($_barcode) && $_barcode != "") {
            $barcode_ = "%" . $_barcode . "%";
            $filterBarcode = " and pr.barcode like ? ";
        }

        $filter = $filterBarcode . $filterProduct;

        $qProducts = "SELECT pr.id,
                                 pr.name,
                                 pr.barcode,
                                 pr.internal_code,
                                 pr.min_count,
                                 ut.`name` unit,
                                 pr.quantity,
                                 pr.sell_price
                            FROM
                              product pr, units ut
                            WHERE
                              pr.unit = ut.id {$filter} limit 50;";

        $stmt = mysqli_prepare($hconnect, $qProducts);

        if (isset($filterBarcode) && isset($filterProduct)) {
            mysqli_stmt_bind_param($stmt, 'ss', $product, $barcode_);
        } else if (isset($filterProduct)) {
            mysqli_stmt_bind_param($stmt, 's', $product);
        } else if (isset($filterBarcode)) {
            mysqli_stmt_bind_param($stmt, 's', $barcode_);
        }

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id, $productName, $barcode, $internalCode, $minCount, $unit, $quantity, $sellPrice);

        while (mysqli_stmt_fetch($stmt)) {

            $i++;
            $i %= 2;
            $bgcolor = ($i ? 'lightyellow' : 'white');

            echo "<tr bgcolor=" . $bgcolor . ">";


            $subTotal = $sellPrice * $quantity;
            $total += $subTotal;
            echo "<td align=center>" . $productName . "</td>
                    <td align=center>" . $barcode . "</td>
                    <td align=center>" . $internalCode . "</td>
                    <td align=center>" . $minCount . "</td>
                    <td align=center>" . $unit . "</td>
                    <td align=center>" . $quantity . "</td>
                    <td align=center>" . $sellPrice . "</td>
                    <td align=center>" . $subTotal . "</td>
                    <td align=center><a href='index.php?action=show&task=change_product&id=" . $id . "'>Редактировать</a></td>
                    <td align=center><a href='index.php?action=show&task=delete_product&id=" . $id . "' onclick='return confirm(\"Восстоновление невозможно! Вы уверены?\") ? true : false;' >Удаление</a></td>
                </tr>";
        }
        ?>
        <tr>
            <td></td>
            <td>Итого:</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><?php echo $total ?></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>