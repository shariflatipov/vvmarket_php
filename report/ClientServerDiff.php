<?php

$_start_date = $_REQUEST['txtDateFrom'];
$_end_date = $_REQUEST['txtDateTo'];

if (!isset($_start_date) && $_start_date == "") {
    $_start_date = date("Y-m-d");
}
if (!isset($_end_date) && $_end_date == "") {
    $_end_date = date("Y-m-d");
}

connect();
$sDate = $_start_date . " 00:00:00%";
$eDate = $_end_date . " 23:59:59";
$query = "SELECT pr.id
                    , pr.barcode
                    , pr.name
                    , pr.sell_price
                    , exd.price
                    , ex.expence_date
                    , concat(acc.surname , ' ', acc.first_name)

               FROM
                 expensedocbyprod exd, product pr, warehouse wh, expensedoc ex, acc_persons acc
               WHERE
                 exd.product_id = pr.id
                 AND wh.product_id = pr.id
                 AND pr.sell_price <> exd.price
                 AND exd.quantity = 1
                 AND exd.product_id <> 11887
                 AND ex.id = exd.expense_doc_id
                 AND ex.expence_date BETWEEN ? AND ?
                 AND acc.id = ex.acc_person_id
               ORDER BY
                 ex.expence_date;";

$stmt = mysqli_prepare($hconnect, $query);

if (!mysqli_stmt_bind_param($stmt, 'ss', $sDate, $eDate)) {
    echo mysqli_stmt_error($stmt);
}
if (!mysqli_stmt_execute($stmt)) {
    echo mysqli_stmt_error($stmt);
}
if (!mysqli_stmt_bind_result($stmt, $id, $barcode, $name, $basePrice, $soldPrice, $date, $person)) {
    echo mysqli_stmt_error($stmt);
}


echo "<form name='frmPriceDiff' method='post'><table ALIGN=center width='100%'  border='1' cellpadding='5' cellspacing='0' class='collapse'><tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td colspan=2>от <INPUT type='text' size=10 maxLength=10 readonly='readonly' 
                    name='txtDateFrom' value='" . $_start_date . "'
                    onclick=\"displayCalendar(document.frmPriceDiff.txtDateFrom,'yyyy-mm-dd', this)\">
                     - до <INPUT type='text'size=10 maxLength=10 readonly='readonly'
                    name='txtDateTo' value='" . $_end_date . "'
                    onclick=\"displayCalendar(document.frmPriceDiff.txtDateTo,'yyyy-mm-dd', this)\"></td>
        <td><input type='submit' value='Фильтр'></td>
        </tr>
        <tr>
        <td>Штрихкод</td>
        <td>Наименование</td>
        <td>Цена на сервере</td>
        <td>Проданная цена</td>
        <td>Разница</td>
        <td>Дата</td>
        <td>Кассир</td>
        </tr>";

while ($row = mysqli_stmt_fetch($stmt)) {
    $diff = $basePrice - $soldPrice;
    $i++;
    $i %= 2;
    $bgcolor = ($i ? 'lightyellow' : 'white');
    echo "<tr bgcolor=\"$bgcolor\">
        <td>$barcode</td>
        <td>$name</td>
        <td>$basePrice</td>
        <td>$soldPrice</td>
        <td>$diff</td>
        <td>$date</td>
        <td>$person</td>
        </tr>";
    $sum += $diff;
}

echo "<tr>
        <td></td>
        <td></td>
        <td></td>
        <td>Итого</td>
        <td>$sum</td>
        <td></td>
        <td></td>
        </tr></table></form>";
?>
