<?php
    $subtask = $_REQUEST['subtask'];
    $cashier = $_REQUEST['cashier'];
    $date = $_REQUEST['txtDate'];
    $s1 = intval($_REQUEST['s1']);
    $s3 = intval($_REQUEST['s3']);
    $s5 = intval($_REQUEST['s5']);
    $s10 = intval($_REQUEST['s10']);
    $s20 = intval($_REQUEST['s20']);
    $s50 = intval($_REQUEST['s50']);
    $s100 = intval($_REQUEST['s100']);
    $s200 = intval($_REQUEST['s200']);
    $s500 = intval($_REQUEST['s500']);
    $coins = floatval($_REQUEST['coins']);
    $expense = doubleval($_REQUEST['expense']);
    $expense_supplier = doubleval($_REQUEST['expense_supplier']);
    $return = doubleval($_REQUEST['return']);
    $cash_back = doubleval($_REQUEST['cash_back']);

    if($subtask == 'save'){
        $query = "INSERT INTO cash_cash (   cash_id, `date`, 
                                            note1, note3, 
                                            note5, note10, 
                                            note20, note50, 
                                            note100, note200,
                                            note500, coins,
                                            expense, expense_supplier, 
                                            `return`, cash_back) 
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        
        if(!$stmt = mysqli_prepare($hconnect, $query)){
            echo mysqli_stmt_error($stmt);
        }
        
        if(!mysqli_stmt_bind_param($stmt, 'ssssssssssssssss' , $cashier, $date, $s1, 
                                $s3, $s5, $s10, $s20, $s50, $s100, $s200, $s500, $coins,
                                $expense, $expense_supplier, $return, $cash_back)){
            
            echo mysqli_stmt_error($stmt);

        }
        if(!mysqli_stmt_execute($stmt)){
            echo mysqli_stmt_error($stmt);
        }
    }
    ?>
<form method="post" name="cash">
    <table border="1">
        <tr>
            <td>Кассир</td>
            <td>Дата</td>
            <td>1 сомони</td>
            <td>3 сомони</td>
            <td>5 сомони</td>
            <td>10 сомони</td>
            <td>20 сомони</td>
            <td>50 сомони</td>
            <td>100 сомони</td>
            <td>200 сомони</td>
            <td>500 сомони</td>
            <td>Монеты</td>
            <td>Расход</td>
            <td>Расход поставщикам</td>
            <td>Возврат</td>
            <td>Сдачи</td>
            <td>Сохранить</td>
        </tr>
        <tr>
            
            <?php 
                $queryCashier = "SELECT ap.id, ap.first_name
                                    FROM
                                      cash ch, acc_persons ap
                                      where ch.acc_person_id = ap.id
                                      and first_name <> 'print';";
                echo '<td><select name="cashier">';
                 $hquery = mysqli_query($hconnect, $queryCashier);
                                            while ($row = mysqli_fetch_array($hquery, MYSQLI_ASSOC))
                                                echo "<OPTION VALUE=" . $row['id'] . ">" . $row['first_name'] . "</OPTION>";
            echo '</select></td>';
            ?>
            
            <td><INPUT size=10 maxLength=10
                       readonly="readonly" name="txtDate" class="line"
                       value="<?= $_date ?>"
                       onclick="displayCalendar(document.cash.txtDate, 'yyyy-mm-dd', this);" /></td>
            <td><input type="text" name="s1"></td>
            <td><input type="text" name="s3"></td>
            <td><input type="text" name="s5"></td>
            <td><input type="text" name="s10"></td>
            <td><input type="text" name="s20"></td>
            <td><input type="text" name="s50"></td>
            <td><input type="text" name="s100"></td>
            <td><input type="text" name="s200"></td>
            <td><input type="text" name="s500"></td>
            <td><input type="text" name="coins"></td>
            <td><input type="text" name="expense"></td>
            <td><input type="text" name="expense_supplier"></td>
            <td><input type="text" name="return"></td>
            <td><input type="text" name="cash_back"></td>
            <td><input type='submit' value='Сохранить' 
                       onclick="if(window.confirm('Вы действительно собираетесь сохранить документ?')) cash.subtask.value='save'; else return false;" ></td>
        <input type="hidden" name="subtask" value="none"/>
        </tr>
    </table>    
</form>
