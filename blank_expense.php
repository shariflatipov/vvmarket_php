<?php
$form_name = 'frmExpense';
$_task = $_REQUEST['task'];


$cmdSave = "<input type='submit' value='Сохранить'
				onclick=\"if(window.confirm('Вы действительно хотите сохранить документ'))" . $form_name . ".task.value='save'; else return false;\" >";
$cmdCancel = "<input type='reset' value='Отменить'>";

$error = 0;
//echo '$_task   --------' . $_task;
if (isset($_task) && $_task == 'save') {
    $_receipter = $_REQUEST['receipter'];
    $receipterId = getSupplier($_receipter);

    if ($receipterId != 0) {

        mysqli_autocommit($hconnect, FALSE);

        $qInsertExpenseDoc = "insert into expensedoc values(null, ?, now(), ?, 0)";
        if (!$stmt = mysqli_prepare($hconnect, $qInsertExpenseDoc)) {
            echo mysqli_stmt_error($stmt);
        }
        mysqli_stmt_bind_param($stmt, 'ss', $_SESSION['user_id'], $receipterId);
        if (mysqli_stmt_execute($stmt)) {
            $expenseDocID = mysqli_stmt_insert_id($stmt);

            $elementsCount = count($_POST);

            $rowsCount = ($elementsCount - 6) / 7;
            $count = 0;
            $sum = 0;

            //echo $rowsCount . '$rowsCount';
            while ($count++ <= $rowsCount) {
                //echo 'in while block';
                $_quantity = $_REQUEST['quantity_' . $count];
                $_productName = $_REQUEST['product_' . $count];
                $_price = $_REQUEST['price_' . $count];
                $_barcode = $_REQUEST['barcode_' . $count];

                if ($_quantity == '' || $_productName == '' || $_price == '') {
                    break;
                }
                if ($_quantity <= 0 || $_price <= 0) {
                    echo '<script>alert("Количество или цена не может быть пустым или отрицательным");</script>';
                    $error = 1;
                }

                $total = $_quantity * $_price;
                $sum += $total;

                $productID = getProductIDByBarcode($_barcode);
                if ($productID == 0) {
                    break;
                }

                $qSelectReceiptPrice = "select receipt_price from warehouse where product_id = {$productID}";
                if (!$result = mysqli_query($hconnect, $qSelectReceiptPrice)) {
                    echo mysqli_error();
                }
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $receiptPrice = $row['receipt_price'];

                $qInsertExpensedocbyprod = "insert into expensedocbyprod values(null, ?, ?, ?, ?, ?)";

                if (!$stmt = mysqli_prepare($hconnect, $qInsertExpensedocbyprod)) {
                    echo mysqli_stmt_error();
                }
                mysqli_stmt_bind_param($stmt, 'sssss', $_price, $productID, $_quantity, $expenseDocID, $receiptPrice);
                if (!mysqli_stmt_execute($stmt)) {
                    echo mysqli_stmt_error($stmt);
                }
                mysqli_stmt_close($stmt);

                $qUpdateProduct = "update product set quantity = (quantity - ?) where id = ?";
                if (!$stmt = mysqli_prepare($hconnect, $qUpdateProduct)) {
                    echo mysqli_stmt_error($stmt);
                }
                mysqli_stmt_bind_param($stmt, 'ss', $_quantity, $productID);
                if (!mysqli_stmt_execute($stmt)) {
                    echo mysqli_stmt_error($stmt);
                }
                mysqli_stmt_close($stmt);

                $qUpdateWarehouse = "update warehouse set quantity = (quantity - ?) where product_id = ?";
                if (!$stmt = mysqli_prepare($hconnect, $qUpdateWarehouse)) {
                    echo mysqli_stmt_error($stmt);
                }
                mysqli_stmt_bind_param($stmt, 'ss', $_quantity, $productID);
                if (!mysqli_stmt_execute($stmt)) {
                    echo mysqli_stmt_error($stmt);
                }
                mysqli_stmt_close($stmt);
            }

            $qUpdateExpenseDoc = "update expensedoc set total_sum = ? where id = ?";
            $stmt = mysqli_prepare($hconnect, $qUpdateExpenseDoc);
            mysqli_stmt_bind_param($stmt, 'ss', $sum, $expenseDocID);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);

            if ($error == 0) {
                mysqli_commit($hconnect);
            } else {
                mysqli_rollback($hconnect);
            }
        }
    }
}
?>
<html>
    <body>

        <table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
            <form name="<?= $form_name ?>" method="post">
                <tr>
                    <td>
                        <fieldset>
                            <legend>Расходная накладная</legend>
                            <table valign=top align=center border="0" width="50%"   cellpadding="0" cellspacing="0">
                                <tr><td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>Продовец</td>
                                                <td><input name="acc_person_1" type="text" id="acc_person_f" size="60" value="<? echo $_SESSION['fio'] ?>"  class="line"/></td></tr>
                                            <tr>
                                                <td>Кому</td>
                                                <td><input name="receipter" type="text" id="acc_person_s" size="60"   class="line"/></td>
                                            </tr>
                                        </table>
                                        <br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table  width="100%" ALIGN=center   border="1" cellpadding="0" cellspacing="0" class="collapse">
                                            <tr>
                                                <th width="5%" >№ б/т</th>
                                                <th width="10%" >Штрихкод</th>
                                                <th width="50%">Наименование</th>
                                                <th width="15%">ед. изм</th>
                                                <th width="15%">Количество</th>
                                                <th width="15%" >Цена</th>
                                                <th width="15%" >Сумма</th>
                                            </tr>
                                            <?php
                                            $count = 0;

                                            while ($count++ < 17) {
                                                echo '<tr>
                                                    <td align=center><input type="text" name="number_' . $count . '" size="1" value="' . $count . '" readonly="readonly" class="bordernone"/></td>
                                                    <td align=center><input type="text" id="barcode_field_' . $count . '" name="barcode_' . $count . '" size="15" class="bordernone"/></td>
                                                    <td align=center><input type="text" id="accp_s_products_' . $count . '" name="product_' . $count . '" size="50" class="bordernone"/></td>
                                                    <td align=center><input type="text" name="unit_' . $count . '" size="4" readonly="readonly" class="bordernone"/></td>

                                                    <td align=center><input type="text" name="quantity_' . $count . '" size="4" class="bordernone"
                                                            onkeyup="return calcTotalExpense( ' . $count . ' )"/>
                                                            <input type="hidden" name="hidden_' . $count . '"> </td>

                                                    <td align=center><input type="text" name="price_' . $count . '" size="8"  class="bordernone"
                                                            onkeyup="return calcTotalExpense( ' . $count . ' )"/></td>

                                                                                                <td align=center><input type="text" name="total_' . $count . '" size="8" readonly="readonly" class="bordernone"/></td>';
                                                echo '</tr>';
                                            }
                                            echo '<tr><td align=center>&nbsp;</td>
                                                  <td>&nbsp;Итого&nbsp;&nbsp;</td>
                                                  <td></td><td></td><td align=center><input type="text" name="totalQuantity" size="8" readonly="readonly" class="bordernone"/></td><td></td>
                                                  <td><input type="text" name="total" size="8" readonly="readonly" class="bordernone"/></td></tr>';
                                            ?>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="80%" border="0" cellspacing="0" cellpadding="1">
                                            <tr>
                                                <td><?= $cmdSave . $cmdCancel; ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <input type="hidden" name=task value="none">
            </form>
        </table>
    </body>
</html>