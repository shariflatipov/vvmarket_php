<?php
	set_time_limit(0);
	session_start();
	require_once("connectDB.php");
	include_once "functions.php";
	include_once "languages.php";
	
	//Выбранная дата
	$_fdatestart = $_REQUEST['datestart'];
	$_fdateend = $_REQUEST['dateend'];
	if(!isset($_fdatestart) || $_fdatestart == '')$_fdatestart = date('Y-m-d');
	if(!isset($_fdateend) || $_fdateend == '')$_fdateend = date('Y-m-d');
	
	$query_date = "SELECT max(date) as date
					FROM
					  ostatok o
					WHERE
					  o.date <= '$_fdatestart'";
	//echo $query_date;
	$result_date = mysql_query($query_date);
	$row = mysql_fetch_array($result_date);
	
	$start_date = substr($row['date'], 0, 10);
	
	$query_ostatok_start = "SELECT o.mark_id
							     , o.count
							     , m.Name
							FROM
							  ostatok o, marks m
							WHERE
							  o.date like '$start_date%'
							  AND m.MarkID = o.mark_id
							ORDER BY m.Name";
	//echo $query_ostatok_start;
	
	$result_start = mysql_query($query_ostatok_start);
	while ($row = mysql_fetch_array($result_start)){
		$count_start[$row['mark_id']] = $row['count'];
		$count_end[$row['mark_id']] = $row['count'];
		$name[$row['mark_id']] = $row['Name'];
		//echo $name[$row['mark_id']];
	}
//	$date = $_fdatestart;
//	$result_start = mysql_query($query_ostatok_start);
//	while($row = mysql_fetch_array($result_start)){
//		$count = 0;
//		$price = 0;
//		$r = 0;
//		
//		while ($count_start[$row['mark_id']] > $count || $count_start[$row['mark_id']] == 0){
//			$r++;
//			$query_start_summ = "SELECT max(re.`Date`) as date, rp.Quantity, rp.Price
//								FROM
//								  receiptsdoc re, receiptsdocprod rp
//								WHERE
//								  re.`Date` <= '$date'
//								  AND re.ReceiptsDocID = rp.ReceiptsDocID and rp.MarkID = {$row['mark_id']}";
//			$result_start_summ = mysql_query($query_start_summ);
//			$ruw = mysql_fetch_array($result_start_summ);
//			$count += $ruw['Quantity'];
//			$date = $ruw['date'];
//			$price += $ruw['Price'];
//			//echo $ruw['Price']."<br>";
//		}
//		$price_start[$row['mark_id']] = $price / $r;
//		$sum_start[$row['mark_id']] = $count_start[$row['mark_id']] * ($price / $r); 
//		//echo $count_start[$row['MarkID']];
//	}
	//Остаток начало
	$query_rec_start = "SELECT rp.*, p.Price pPrice
						FROM
						  receiptsdoc re, receiptsdocprod rp, products p
						WHERE
						  re.`Date` BETWEEN '$start_date 00:00:00' AND '$_fdatestart 00:00:00' AND re.ReceiptsDocID = rp.ReceiptsDocID AND
						  p.ProductID = rp.MarkID";
	
	$result_rec_start = mysql_query($query_rec_start);
	while ($row = mysql_fetch_array($result_rec_start)){
		
		$count_start[$row['MarkID']] += $row['Quantity'];
		$sum_start[$row['MarkID']] += $row['Quantity'] * $row['Price'];
		$count_end[$row['MarkID']] += $row['Quantity'];
		$sum_end[$row['MarkID']] += $row['Quantity'] * $row['Price'];
		
		$summ_count_start += $row['Quantity'];
		$summ_count_end += $row['Quantity'];
		$summ_sum_start += $row['Quantity'] * $row['Price'];
		$summ_sum_end += $row['Quantity'] * $row['Price'];
	}
	
	$query_exp_start = "SELECT pr.MarkID, p.Quantity, pr.Price, p.receipt_price
						FROM
						  prodaja p, products pr
						WHERE
						  p.SDate BETWEEN '$start_date 00:00:00' AND '$_fdatestart 00:00:00' AND pr.ProductID = p.ProductID";
	//echo $query_exp_start;
	
	$result_exp_start = mysql_query($query_exp_start);
	while ($row = mysql_fetch_array($result_exp_start)){
		
		$count_start[$row['MarkID']] -= $row['Quantity'];
		$count_end[$row['MarkID']] -= $row['Quantity'];
		$sum_start[$row['MarkID']] -= $row['Quantity'] * $row['receipt_price'];
		$sum_end[$row['MarkID']] -= $row['Quantity'] * $row['receipt_price'];
		
		$summ_count_start -= $row['Quantity'];
		$summ_sum_start -= $row['Quantity'] * $row['receipt_price'];
		$summ_count_end -= $row['Quantity'];
		$summ_sum_end -= $row['Quantity'] * $row['receipt_price'];
	}
//	echo "<pre>";
//	print_r($count_start);
//	echo "</pre>";
//	echo $count_start['1587'];
	//Приход
	$query_rec = "SELECT rp.*
						FROM
						  receiptsdoc re, receiptsdocprod rp
						WHERE
						  re.`Date` BETWEEN '$_fdatestart 00:00:00' AND '$_fdateend 23:59:59' AND re.ReceiptsDocID = rp.ReceiptsDocID";
	
	$result_rec = mysql_query($query_rec);
	while ($row = mysql_fetch_array($result_rec)){
		$count_rec[$row['MarkID']] += $row['Quantity'];
		$count_end[$row['MarkID']] += $row['Quantity'];
		$sum_rec[$row['MarkID']] += $row['Quantity'] * $row['Price'];
		$sum_end[$row['MarkID']] += $row['Quantity'] * $row['Price'];
		
		$summ_count_rec += $row['Quantity'];
		$summ_count_end += $row['Quantity'];
		$summ_sum_rec += $row['Quantity'] * $row['Price'];
		$summ_sum_end += $row['Quantity'] * $row['Price'];
	}
	
	//Расход
	$query_exp = "SELECT pr.MarkID, p.Quantity, s.price as sell_price, p.Price
					FROM
					  prodaja p, products pr, sell_price s
					WHERE
					  p.SDate BETWEEN '$_fdatestart 00:00:00' AND '$_fdateend 23:59:59' AND pr.ProductID = p.ProductID AND pr.sell_priceID = s.id";
	
	$result_exp = mysql_query($query_exp);
	while ($row = mysql_fetch_array($result_exp)){
		$count_exp[$row['MarkID']] += $row['Quantity'];
		$count_end[$row['MarkID']] -= $row['Quantity'];
		$sum_exp[$row['MarkID']] += $row['Quantity'] * $row['Price'];
		$sum_discount[$row['MarkID']] += $row['Quantity'] * ($row['sell_price'] - $row['Price']);
		$sum_end[$row['MarkID']] -= $row['Quantity'] * $row['Price'];
		
		$summ_count_exp += $row['Quantity'];
		$summ_count_end -= $row['Quantity'];
		$summ_sum_exp += $row['Quantity'] * $row['Price'];
		$summ_sum_end -= $row['Quantity'] * $row['Price'];
	}
?>

<table border='' class='silver'>
<form name="frmProduct"  method="post">
<?=$lang[$language.'_Ot']?>
<INPUT size=10 maxLength=10 readonly='readonly' name='datestart' class='line' 
value="<?= $_fdatestart?>" onclick='displayCalendar(document.frmProduct.datestart, "yyyy-mm-dd", this)'/>
<?=$lang[$language.'_do']?>
<INPUT size=10 maxLength=10 readonly='readonly' name='dateend' class='line' 
value="<?= $_fdateend?>" onclick='displayCalendar(document.frmProduct.dateend, "yyyy-mm-dd", this)' onchange='this.form.submit()'/>
<input type="hidden" name=task value="none">
<?php 

//	$date = $_fdatestart;
//	$result_start = mysql_query($query_ostatok_start);
//	while($row = mysql_fetch_array($result_start)){
//		$count = 0;
//		$price = 0;
//		$r = 0;
//		
//		while ($count_start[$row['mark_id']] > $count){
//			$r++;
//			$query_start_summ = "SELECT max(re.`Date`) as date, rp.Quantity, rp.Price
//								FROM
//								  receiptsdoc re, receiptsdocprod rp
//								WHERE
//								  re.`Date` <= '$date'
//								  AND re.ReceiptsDocID = rp.ReceiptsDocID and rp.MarkID = {$row['mark_id']}";
//			$result_start_summ = mysql_query($query_start_summ);
//			$ruw = mysql_fetch_array($result_start_summ);
//			$count += $ruw['Quantity'];
//			$date = $ruw['date'];
//			$price += $ruw['Price'];
//			//echo $query_start_summ."<br>";
//		}
//		$price_start[$row['mark_id']] = $price / $r;
//		$sum_start[$row['mark_id']] = $count_start[$row['mark_id']] * ($price / $r); 
//		//echo $count_start[$row['MarkID']];
//	}

	echo "<table border = '1'><tr><td>№</td><td>Название</td><td>Ост. на нач.</td><td>Сумма на нач.</td><td>Приход</td>
	<td>Приход на сумму</td><td>Расход</td><td>Расход на сумму</td><td>Скидка</td><td>Ост на конец</td><td>Сумма на конец</td></tr>";
	$query_ostatok_start = "SELECT o.mark_id
							     , o.count
							     , m.artikle
							     , m.Name
							FROM
							  ostatok o, marks m
							WHERE
							  o.date like '$start_date%'
							  AND m.MarkID = o.mark_id
							ORDER BY m.Name";
	//echo $query_ostatok_start;
	$result_start = mysql_query($query_ostatok_start);
	$i = 0;
//	$date = $start_date;
	while ($row = mysql_fetch_array($result_start)){
		
//		$count = 0;
//		$price = 0;
//		$r = 0;
//		while ($count_start[$row['MarkID']] > $count){
//			$r++;
//			$query_start_summ = "SELECT max(re.`Date`) as date, rp.Quantity, rp.Price
//								FROM
//								  receiptsdoc re, receiptsdocprod rp
//								WHERE
//								  re.`Date` <= '$date'
//								  AND re.ReceiptsDocID = rp.ReceiptsDocID and rp.MarkID = {$row['MarkID']}";
//			$result_start_summ = mysql_query($query_start_summ);
//			$ruw = mysql_fetch_array($result_start_summ);
//			$count += $ruw['Quantity'];
//			$date = $ruw['date'];
//			$price += $ruw['Price'];
//		}
//		
//		$sum_start[$row['MarkID']] = $count_start[$row['MarkID']] * ($price / $r); 
		
		$i++;
		echo "<tr><td>{$i}</td><td>{$name[$row['mark_id']]}</td><td>" . round($count_start[$row['mark_id']],2) . "</td><td>" . round($sum_start[$row['mark_id']],2). "</td>
		<td>" . round($count_rec[$row['mark_id']],2). "</td><td>" . round($sum_rec[$row['mark_id']],2) . "</td><td>" . round($count_exp[$row['mark_id']],2) . "</td>
		<td>" . round($sum_exp[$row['mark_id']], 2) . "</td><td>{$sum_discount[$row['mark_id']]}</td><td>" . round($count_end[$row['mark_id']],2) . "</td><td>" . round($sum_end[$row['mark_id']],2) . "</td></tr>";
		$s = $s + $sum_discount[$row['mark_id']];
	}
	echo "<tr><td colspan=2>Итого:</td><td>{$summ_count_start}</td><td>{$summ_sum_start}</td>
		<td>{$summ_count_rec}</td><td>{$summ_sum_rec}</td><td>{$summ_count_exp}</td>
		<td>{$summ_sum_exp}</td><td>$s</td><td>{$summ_count_end}</td><td>" . $summ_sum_end . "</td></tr>";
	echo "</table>";
//	echo "<pre>";
//	print_r($sum_discount);
//	echo "</pre>";
?>