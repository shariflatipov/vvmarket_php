﻿<?php

class Acc_Person {

    function isAcc_Person($_full_name) {
        $sel_acc_p_query = "SELECT * 
				FROM `Acc_Persons`
				WHERE CONCAT( `Surname`, ' ', `FirstName`, ' ', `LastName` ) = '" . trim($_full_name) . "';";

        $hquery = mysql_query($sel_acc_p_query) or die("ERROR: " . mysql_error());

        if (mysql_num_rows($hquery) == 0)
            return false;
        else
            return true;
    }

    function getAcc_PersonID($_full_name) {
        global $hconnect;
        $_full_name = trim($_full_name);
        $usersql = "SELECT id 
				FROM `supplier`
				WHERE company = ?";

        
        $stmt = mysqli_prepare($hconnect, $usersql);
        mysqli_stmt_bind_param($stmt, 's', $_full_name);

        if(!mysqli_stmt_execute($stmt)){
            echo mysqli_error($hconnect);
        }
        
        

        mysqli_stmt_bind_result($stmt, $id);
        mysqli_stmt_fetch($stmt);
        
        return $id;
    }

    function createAcc_Person_Prod_depent($_acc_person, $_field) {
        global $lang, $language;

        $sel_prod_query = "SELECT 
					pr.`UnitID`,
					pr.`Price` `FPrice`,
					pr.`AmortizationID`,
					pr.`Quantity` `Quantity_p`,
					am.`Rate`,
					mk.`Name` `Name_product`, 
					pg.`Type` `Name_type`, 
					ap.*,
					ut.*,
					wr.`OsPrice`,
					wr.`WearPrice`,
					( SELECT `DocumentID` FROM `AccP_S_Products_Start` ap_s
					  WHERE ap_s.`Date` = ap.`FDate` AND ap_s.`ProductID` =  ap.`ProductID`
					  AND ap_s.`SupplierID` =  ap.`SupplierID` ) `DocID`
					FROM 
					`Products` pr, 
					`ProductGroups` pg, 
					`Marks` mk,
					`Units` ut,
				    `Amortizations` am, 
					`AccP_S_Products` ap,
					`Wears` wr
					WHERE ap.`ProductID` = pr.`ProductID`
					AND ap.`ProductID` = wr.`ProductID` 	
					AND pr.`AmortizationID` = am.`AmortizationID`
					AND pr.`MarkID` = mk.`MarkID`
					AND pr.`UnitID` = ut.`UnitID`
					AND mk.`ProductGroupID` = pg.`ProductGroupID` 
					AND ap.`{$_field}` = " . $_acc_person;
        #. " AND " . $_filter_date;

        debug($sel_prod_query);

        mysql_query("CALL `calc_Amortization`( 0 );");
        $hquery = mysql_query($sel_prod_query);

        $count_prod = mysql_num_rows($hquery);
        if (!$count_prod)
            return;

        $text = '<table width=100%  border="1" cellpadding="5" cellspacing="0" class="silver">';

        /* Create Filter for Date */
        $event = 'onchange="frmAccounts.submit()"';
        #$create_filter_account_query = "SELECT * FROM `Accounts` WHERE `Dependent` = 0 " . $filters;
        $create_filter_account_query = "SELECT * FROM `Accounts` WHERE `Dependent` = 0 " . $sel_type;
        debug($create_filter_account_query);

//			$text .=    "<tr class=rh><td></td><td>";			
//			$text .= '<INPUT size=10 maxLength=10 readonly="readonly" name="fdate"  value="' . $_fdate . '"';
//			$text .= 'onclick="displayCalendar(document.forms[0].fdate,\'yyyy-mm-dd\', this)"'; 
//			$text .= 'onchange="frmAcc_per_Prod.submit()"></td>';
//
//			$text .= '<td>';			
//			$text .= '<INPUT size=10 maxLength=10 readonly="readonly" name="sdate"  value="' . $_sdate . '"';
//			$text .= 'onclick="displayCalendar(document.forms[0].sdate,\'yyyy-mm-dd\', this)"'; 
//			$text .= 'onchange="frmAcc_per_Prod.submit()"></td>';
//			$text .= '<td>' . Acc_Person::createSelectQPrice( $_acc_person, 'Quantity' ) . '</td>';
//			$text .= '<td>' . Acc_Person::createSelectQPrice( $_acc_person, 'Price' ) . '</td><td></td>';
//			$text .= '<td>' . Acc_Person::createSelectTypeProduct( $_acc_person ) . '</td>';
//			$text .= '<td></td><td>' . Acc_Person::createSelectAmortization( $_acc_person ) . '</td><td></td><td></td></tr>';
        $text .= "<Tr><Th width=25%>" . $lang[$language . '_Name'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Date'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Date_oprixod'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Quantity'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Price'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Sum'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Type'] . "</Th>";
        $text .= "<Th width=15%>" . $lang[$language . '_First_cost'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Group'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Residual_value'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Sum_Wear'] . "</Th></tr>";
        #$text .=    "<Th width=10%>" . $lang[$language.'_OsBalance_cost'] . "</Th></tr>";


        while ($row = mysql_fetch_array($hquery)) {
            $i++;
            $i %= 2;
            $bgcolor = ($i ? 'lightyellow' : 'white');
            $fprice = $row['FPrice'];
            $osprice = $row['OsPrice'] * $row['Quantity'];
            $wprice = $row['WearPrice'] * ( $row['Quantity']); # / $row['Quantity_p'] );
            #$fprice = ceil( $fprice * 100 ) / 100;
            #$osprice = ceil( $osprice * 100 ) / 100;
            #$wprice = ceil( $wprice * 100 ) / 100;

            $tfprice += $fprice;
            $tosprice += $osprice;
            $twprice += $wprice;

            $product_name = $row['Name_product'];
            $product_name = htmlspecialchars($product_name, ENT_COMPAT, 'UTF-8');
            #$product_name = Acc_Person::stripquote($product_name);
            $text .= "<tr bgcolor=" . $bgcolor . ">";
            #$text .=    "<td>&nbsp;&nbsp;". $product_name ."</td>";
            $text .= "<td><span onmouseout='nd();' onmouseover=\"return overlib('<b>" . $product_name . "</b>', WIDTH, 200)\">";
            $text .= '&nbsp;&nbsp;<INPUT class=bordernone_t style="width: 200px;background-color:' . $bgcolor . ';"  value="' . $product_name . '" readonly="readonly"></span></td>';
            $text .= "<td align=center><a style='cursor: hand' onclick=\"window.open('load_doc.php?docid=" . $row['DocID'] . "',
							'', 'dependent,width=700,height=700,left=0 ,top=0')\">
							" . $row['FDate'] . "</a></td>";
            $text .= "<td align=center><a style='cursor: hand' onclick=\"window.open('load_doc.php?docid=" . $row['DocumentID'] . "',
							'', 'dependent,width=700,height=700,left=0 ,top=0')\">
							" . $row['SDate'] . "</a></td>";

            #$text .=    "<td align=center>". $row['FDate'] ."</td>";
            #$text .=    "<td align=center>". $row['SDate'] ."</td>";
            $text .= "<td>&nbsp;&nbsp;" . $row['Quantity'] * 1 . " " . $row['Name_' . $language] . "</td>";
            $text .= "<td>&nbsp;&nbsp;" . $row['Price'] . "</td>";
            $text .= "<td>&nbsp;&nbsp;" . ( $row['Quantity'] * $row['Price'] ) . "</td>";
            #$text .=    "<td>&nbsp;&nbsp;".  $row['Name_type'] ."</td>";
            $text .= "<td><span onmouseout='nd();' onmouseover=\"return overlib('<b>" . $row['Name_type'] . "</b>', WIDTH, 200)\">";
            $text .= '&nbsp;&nbsp;<INPUT class=bordernone_t style="width: 200px;background-color:' . $bgcolor . ';"  
					value="' . $row['Name_type'] . '" readonly="readonly"></span></td>';
            $text .= "<td>&nbsp;&nbsp;" . "({$fprice}) " . ( $row['Quantity'] * $fprice ) . "</td>";
            $text .= "<td><span onmouseout='nd();' onclick=\"return overlib('" . Account::createAccountTable($row['AmortizationID']) . "', WIDTH, 500)\">";
            $text .= "<a style='cursor: hand'>
							&nbsp;&nbsp;" . $row['AmortizationID'] . " - " . $row['Rate'] . "%</a></span></td>";

            #$text .=    "<td>&nbsp;&nbsp;".  $row['AmortizationID'] ." - ".  $row['Rate'] ."%</td>";
            $text .= "<td>&nbsp;&nbsp;" . $osprice . "</td>";
            $text .= "<td>&nbsp;&nbsp;" . $wprice . "</td></tr>";
            #$text .=    "<td>&nbsp;&nbsp;".  ($osprice - $wprice) ."</td></tr>";
        }

        $text .= "<Tr><Th>" . $lang[$language . '_Total'] . "</Th>
					<Th></Th><Th></Th><Th></Th><Th></Th><Th></Th><Th></Th>
					<Th>" . $tfprice . "</Th><Th></Th>
					<Th>" . $tosprice . "</Th>
					<Th>" . $twprice . "</Th></tr>";
        #<Th>" . ( $tosprice - $twprice ) . "</Th></tr>";

        $text .= "</table>";
        return $text;
    }

    function createAcc_Person_Prod_($_acc_person, $_acc_person_name, $_filter_date) {
        global $lang, $language;

        $sel_prod_query = "SELECT 
					pr.`UnitID`,
					pr.`Price` `FPrice`,
					pr.`AmortizationID`,
					pr.`Quantity` `Quantity_p`,
					am.`Rate`,
					mk.`Name` `Name_product`, 
					pg.`Type` `Name_type`, 
					ap.*,
					ut.*,
					wr.`OsPrice`,
					wr.`WearPrice`,
					( SELECT `DocumentID` FROM `AccP_S_Products_Start` ap_s
					  WHERE ap_s.`Date` = ap.`FDate` AND ap_s.`ProductID` =  ap.`ProductID`
					  AND ap_s.`SupplierID` =  ap.`SupplierID` ) `DocID`
					FROM 
					`Products` pr, 
					`ProductGroups` pg, 
					`Marks` mk,
					`Units` ut,
				    `Amortizations` am, 
					`AccP_S_Products` ap,
					`Wears` wr
					WHERE ap.`ProductID` = pr.`ProductID`
					AND ap.`ProductID` = wr.`ProductID` 	
					AND pr.`AmortizationID` = am.`AmortizationID`
					AND pr.`MarkID` = mk.`MarkID`
					AND pr.`UnitID` = ut.`UnitID`
					AND mk.`ProductGroupID` = pg.`ProductGroupID` 
					AND ap.`Acc_PersonID` = " . $_acc_person;
        #. " AND " . $_filter_date;

        debug($sel_prod_query);

        mysql_query("CALL `calc_Amortization`( 0 );");
        $hquery = mysql_query($sel_prod_query);

        $count_prod = mysql_num_rows($hquery);
        if (!$count_prod)
            return;

        $text = '<table width=100%  border="1" cellpadding="5" cellspacing="0" class="silver">';
        $text .= "<Tr><Td COLSPAN=11	 class=rb>" . $lang[$language . '_Acc_Person'] . " \"" . $_acc_person_name . "\"</tr>";
        /* Create Filter for Date */
        /*
          $event = 'onchange="frmAccounts.submit()"';
          #$create_filter_account_query = "SELECT * FROM `Accounts` WHERE `Dependent` = 0 " . $filters;
          $create_filter_account_query = "SELECT * FROM `Accounts` WHERE `Dependent` = 0 " . $sel_type;
          debug( $create_filter_account_query );

          $text .=    "<tr class=rh><td></td><td>";
          $text .= '<INPUT size=10 maxLength=10 readonly="readonly" name="fdate"  value="' . $_fdate . '"';
          $text .= 'onclick="displayCalendar(document.forms[0].fdate,\'yyyy-mm-dd\', this)"';
          $text .= 'onchange="frmAcc_per_Prod.submit()"></td>';

          $text .= '<td>';
          $text .= '<INPUT size=10 maxLength=10 readonly="readonly" name="sdate"  value="' . $_sdate . '"';
          $text .= 'onclick="displayCalendar(document.forms[0].sdate,\'yyyy-mm-dd\', this)"';
          $text .= 'onchange="frmAcc_per_Prod.submit()"></td>';
          $text .= '<td>' . Acc_Person::createSelectQPrice( $_acc_person, 'Quantity' ) . '</td>';
          $text .= '<td>' . Acc_Person::createSelectQPrice( $_acc_person, 'Price' ) . '</td>';
          $text .= '<td>' . Acc_Person::createSelectTypeProduct( $_acc_person ) . '</td>';
          $text .= '<td></td><td></td><td>' . Acc_Person::createSelectAmortization( $_acc_person ) . '</td><td></td><td></td><td></td></tr>'; */
        $text .= "<Tr><Th width=25%>" . $lang[$language . '_Name'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Date'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Date_oprixod'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Quantity'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Price'] . "</Th>";
        $text .= "<Th width=5%>" . $lang[$language . '_Sum'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Type'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_First_cost'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Group'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Residual_value'] . "</Th>";
        $text .= "<Th width=10%>" . $lang[$language . '_Sum_Wear'] . "</Th></tr>";
        #$text .=    "<Th width=10%>" . $lang[$language.'_OsBalance_cost'] . "</Th></tr>";


        $tfprice_t = 0;
        while ($row = mysql_fetch_array($hquery)) {
            $i++;
            $i %= 2;
            $bgcolor = ($i ? 'lightyellow' : 'white');
            $fprice = $row['FPrice'];
            $osprice = $row['OsPrice'] * $row['Quantity'];
            $wprice = $row['WearPrice'] * ( $row['Quantity']); # / $row['Quantity_p'] );
            #$fprice = ceil( $fprice * 100 ) / 100;
            #$osprice = ceil( $osprice * 100 ) / 100;
            #$wprice = ceil( $wprice * 100 ) / 100;

            $tfprice += $fprice;
            $tfprice_t += $fprice * $row['Quantity'];
            $tosprice += $osprice;
            $twprice += $wprice;

            $product_name = $row['Name_product'];
            #$product_name = Acc_Person::stripquote($product_name);
            $product_name = htmlspecialchars($product_name, ENT_COMPAT, 'UTF-8');
            $text .= "<tr bgcolor=" . $bgcolor . ">";
            #$text .=    "<td>&nbsp;&nbsp;". $row['Name_product'] ."</td>";
            $text .= "<td><span onmouseout='nd();' onmouseover=\"return overlib('<b>" . $product_name . "</b>', WIDTH, 200)\">";
            $text .= '&nbsp;&nbsp;<INPUT class=bordernone_t style="width: 200px;background-color:' . $bgcolor . ';"  value="' . $product_name . '" readonly="readonly"></span></td>';
            $text .= "<td align=center><a style='cursor: hand' onclick=\"window.open('load_doc.php?docid=" . $row['DocID'] . "',
							'', 'dependent,width=700,height=700,left=0 ,top=0')\">
							" . $row['FDate'] . "</a></td>";
            $text .= "<td align=center><a style='cursor: hand' onclick=\"window.open('load_doc.php?docid=" . $row['DocumentID'] . "',
							'', 'dependent,width=700,height=700,left=0 ,top=0')\">
							" . $row['SDate'] . "</a></td>";

            #$text .=    "<td align=center>". $row['FDate'] ."</td>";
            #$text .=    "<td align=center>". $row['SDate'] ."</td>";
            $text .= "<td>&nbsp;&nbsp;" . $row['Quantity'] * 1 . " " . $row['Name_' . $language] . "</td>";
            $text .= "<td>&nbsp;&nbsp;" . $row['Price'] . "</td>";
            $text .= "<td>&nbsp;&nbsp;" . ( $row['Quantity'] * $row['Price'] ) . "</td>";
            #$text .=    "<td>&nbsp;&nbsp;".  $row['Name_type'] ."</td>";
            $text .= "<td><span onmouseout='nd();' onmouseover=\"return overlib('<b>" . $row['Name_type'] . "</b>', WIDTH, 200)\">";
            $text .= '&nbsp;&nbsp;<INPUT class=bordernone_t style="width: 200px;background-color:' . $bgcolor . ';"  
					value="' . $row['Name_type'] . '" readonly="readonly"></span></td>';
            $text .= "<td>&nbsp;&nbsp;" . "({$fprice}) " . ( $row['Quantity'] * $fprice ) . "</td>";
            $text .= "<td><span onmouseout='nd();' onclick=\"return overlib('" . Account::createAccountTable($row['AmortizationID']) . "', WIDTH, 500)\">";
            $text .= "<a style='cursor: hand'>
							&nbsp;&nbsp;" . $row['AmortizationID'] . " - " . $row['Rate'] . "%</a></span></td>";

            #$text .=    "<td>&nbsp;&nbsp;".  $row['AmortizationID'] ." - ".  $row['Rate'] ."%</td>";
            $text .= "<td>&nbsp;&nbsp;" . $osprice . "</td>";
            $text .= "<td>&nbsp;&nbsp;" . $wprice . "</td></tr>";
            #$text .=    "<td>&nbsp;&nbsp;".  ($osprice - $wprice) ."</td></tr>";
        }

        $text .= "<Tr><Th>" . $lang[$language . '_Total'] . "</Th>
					<Th></Th><Th></Th><Th></Th><Th></Th><Th></Th><Th></Th>
					<Th>({$tfprice}) {$tfprice_t}</Th><Th></Th>
					<Th>" . $tosprice . "</Th>
					<Th>" . $twprice . "</Th></tr>";
        #<Th>" . ( $tosprice - $twprice ) . "</Th></tr>";

        $text .= "</table></br>";
        return $text;
    }

    function createSelectQPrice($_acc_personid, $_field_qp) {
        global $lang, $language;

        $supplierid = $_REQUEST['supplier'];
        debug($supplierid);

        $qprice_query = "SELECT ap.* FROM `AccP_S_Products` ap
							 WHERE ap.`Acc_PersonID` = {$_acc_personid}
							 GROUP BY ap.`{$_field_qp}`;";
        $hquery = mysql_query($qprice_query);

        $text = "<SELECT NAME={$_field_qp} SIZE=1 " . $_event . " >";
        $text .= "<OPTION VALUE=all>" . $lang[$language . '_All_'] . "</OPTION>";
        $text .= "<OPTION VALUE=condition>" . $lang[$language . '_Condition'] . "</OPTION>";

        while ($row = mysql_fetch_array($hquery)) {
            $selected = '';
            if ($row[$_field_qp] == $supplierid)
                $selected = 'selected';
            $text .= "<OPTION VALUE=" . $row[$_field_qp] . " " . $selected . ">" . $row[$_field_qp] . "</OPTION>";
        }

        return $text;
    }

    function createSelectTypeProduct($_acc_personid) {
        global $lang, $language;

        $supplierid = $_REQUEST['supplier'];
        debug($supplierid);

        $qprice_query = "SELECT *
							  FROM
								  `AccP_S_Products` ap
								  INNER JOIN `Products` pr ON ap.`ProductID` = pr.`ProductID`
								  INNER JOIN `Marks` mk ON pr.`MarkID` = mk.`MarkID`
								  INNER JOIN `ProductGroups` prg ON mk.`ProductGroupID` = prg.`ProductGroupID`
							 WHERE ap.`Acc_PersonID` = {$_acc_personid}
						  GROUP BY prg.`Type`;";

        $hquery = mysql_query($qprice_query);

        $text = "<SELECT NAME=type_product SIZE=1 " . $_event . " >";
        $text .= "<OPTION VALUE=all>" . $lang[$language . '_All_'] . "</OPTION>";

        while ($row = mysql_fetch_array($hquery)) {
            $selected = '';
            if ($row['Type'] == $supplierid)
                $selected = 'selected';
            $text .= "<OPTION VALUE=" . $row['Type'] . " " . $selected . ">" . $row['Type'] . "</OPTION>";
        }

        return $text;
    }

    function createSelectAmortization($_acc_personid) {
        global $lang, $language;

        $supplierid = $_REQUEST['supplier'];
        debug($supplierid);

        $qprice_query = "SELECT *
							  FROM
								  `AccP_S_Products` ap
								  INNER JOIN `Products` pr ON ap.`ProductID` = pr.`ProductID`
							 WHERE ap.`Acc_PersonID` = {$_acc_personid}
						  GROUP BY pr.`AmortizationID`;";

        $hquery = mysql_query($qprice_query);

        $text = "<SELECT NAME=amortization SIZE=1 " . $_event . " >";
        $text .= "<OPTION VALUE=all>" . $lang[$language . '_All_'] . "</OPTION>";

        while ($row = mysql_fetch_array($hquery)) {
            $selected = '';
            if ($row['AmortizationID'] == $supplierid)
                $selected = 'selected';
            $text .= "<OPTION VALUE=" . $row['AmortizationID'] . " " . $selected . ">" . $row['AmortizationID'] . "</OPTION>";
        }

        return $text;
    }

    function stripquote($_str) {
        $len = mb_strlen($_str);
        $new_str = '';
        for ($_ = 0; $_ < $len; $_++) {
            $symbol = mb_substr($_str, $_, 1);
            if (ord($symbol) != ord('"') && ord($symbol) != ord('\''))
                $new_str .= $symbol;
        }
        return $new_str;
    }

}

;
?> 