<?php

class Account {
	function createAccountTable( $_amortization )
	{
		global $lang, $language;
		$account_query = "SELECT * FROM `Accounts` acc INNER JOIN `Amort_Accounts` amacc 
							  ON acc.`AccountID` = amacc.`AccountID`
						   WHERE amacc.`AmortizationID` = {$_amortization}"; # . $filters . " " . $limit;
						
		debug($account_query);
		$_hquery = mysql_query($account_query);

		$text = "<table ALIGN=center width=90%  border=1 cellpadding=0 cellspacing=0 class=silver>";
		$text .= "<tr class=rh><td width=5%>" . $lang[$language.'_Account'] . "</td>";
		$text .= "<td width=50%>" . $lang[$language.'_Name'] . "</td>";
		$text .= "<td width=5%>" . $lang[$language.'_Type'] . "</td></tr>";
		
		while( $row = mysql_fetch_array($_hquery) ) {
			$i++;
			$i %= 2;
			$bgcolor = ($i ? 'faf0e6' : 'fffff');
			$bgcolor = ($i ? 'lightyellow' : 'white');
			
			$text .= "<tr bgcolor=".$bgcolor." class=rt>";
			
			if($row['Dependent'] == '0' ){ 
				$text .= "<td align=center><b>". getAccountF( $row['AccountID'] )."</b></td>";
				$text .= "<td>&nbsp;&nbsp;<b>". $row['Name_' . $language] ."</b></td>";
				$text .= "<td align=center><b>". getAccountType( $row['Type']) ."</b></td></tr>";
				
			}
			else { 
				$text .= "<td align=center>". getAccountF( $row['AccountID'] ) ."</td>";
				$text .= "<td>&nbsp;&nbsp;". $row['Name_' . $language] ."</td>";
				$text .= "<td align=center>". getAccountType( $row['Type']) ."</td></tr>";
			}
			
		}
		$text .= "</TABLE>"; 
		
		return $text;
		
	}
};

?>