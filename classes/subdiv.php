<?php
	
	class Subdiv {
		function isSubdiv( $_name_rus, $_name_taj ) {
			$sel_subdiv_query = "SELECT * 
								   FROM `Struc_Subdivisions`
								  WHERE `Name_rus` = '" . trim( $_name_rus ) . "'
								    AND `Name_taj` = '" . trim( $_name_taj ) . "';";	
			$hquery = mysql_query( $sel_subdiv_query ) or die( "ERROR: " . mysql_error() );
			
			if( mysql_num_rows( $hquery ) == 0 ) return false;
			else return true;
		}

		function getSubdivID( $_name_rus, $_name_taj ) {
			$sel_subdiv_query = "SELECT * 
								   FROM `Struc_Subdivisions`
								  WHERE `Name_rus` = '" . trim( $_name_rus ) . "'
								    AND `Name_taj` = '" . trim( $_name_taj ) . "';";	
			$hquery = mysql_query( $sel_subdiv_query ) or die( "ERROR: " . mysql_error() );
		
			if( mysql_num_rows( $hquery ) == 0 ) return false;
			
			$res = mysql_fetch_array( $hquery );
			return $res['SSID'];
		}
		
		function insSubdivD( $_name_rus, $_name_taj ) {
			if( Subdiv::isSubdiv( $_name_rus, $_name_taj ) == true )
				return Subdiv::getSubdivID( $_name_rus, $_name_taj );

			$subdiv_query = "SELECT COUNT(`SSID`) 
					 FROM `Struc_Subdivisions` 
					WHERE `Dependent` = 0 
					  AND `Level` = 1;";
					
			$hsubdiv_query = mysql_query($subdiv_query);
			$count = mysql_result($hsubdiv_query, 0, 0);

			$ssid = ++$count + 101;			
						
			$ins_subdiv_query = "INSERT INTO `Struc_Subdivisions` VALUES								
							( $ssid, '" . trim($_name_rus) . "', '" . trim( $_name_taj) . "', 0, 1, 'D' );";
				
			$hquery = mysql_query( $ins_subdiv_query ) or die( "ERROR: " . mysql_error() );
			
			return Subdiv::getSubdivID( $_name_rus, $_name_taj );
		}

		function insSubdivF( $_name_dr, $_name_dt, $_name_fr, $_name_ft ) {

			$depen = Subdiv::insSubdivD( $_name_dr, $_name_dt );
						
			$subdiv_query = "SELECT COUNT(`SSID`) 
					 FROM `Struc_Subdivisions` 
					WHERE `Dependent` = " . $depen . " 
					  AND `Level` = 2;";
					
			$hsubdiv_query = mysql_query($subdiv_query);
			$count = mysql_result($hsubdiv_query, 0, 0);

			$ssid = $depen . ( ++$count );
						
			$ins_subdiv_query = "INSERT INTO `Struc_Subdivisions` VALUES								
							( $ssid, '" . trim($_name_fr) . "', '" . trim( $_name_ft) . "', $depen, 2, 'F' );";
				
			$hquery = mysql_query( $ins_subdiv_query ) or die( "ERROR: " . mysql_error() );
			
			return Subdiv::getSubdivID( $_name_dr, $_name_ft );
		}

		function updSubdivD( $_name_rus, $_name_taj ) {
			
			$ssid = Subdiv::getSubdivID( $_name_rus, $_name_taj );

			$subdiv_query = "SELECT COUNT(`SSID`) 
					 FROM `Struc_Subdivisions` 
					WHERE `Dependent` = 0 
					  AND `Level` = 1;";
					
			$upd_subdiv_query = "UPDATE `Struc_Subdivisions`
									SET `Name_rus` = " . $_group . " 
								  WHERE `SSID` = " . $ssid;
		
			$ins_subdiv_query = "INSERT INTO `Struc_Subdivisions` VALUES								
							( $ssid, '" . trim($_name_rus) . "', '" . trim( $_name_taj) . "', 0, 1, 'D' );";
				
			$hquery = mysql_query( $ins_subdiv_query ) or die( "ERROR: " . mysql_error() );
			
			return Subdiv::getSubdivID( $_name_rus, $_name_taj );
		}

		function updSubdivF( $_name_dr, $_name_dt, $_name_fr, $_name_ft ) {

			$depen = Subdiv::insSubdivD( $_name_dr, $_name_dt );
						
			$subdiv_query = "SELECT COUNT(`SSID`) 
					 FROM `Struc_Subdivisions` 
					WHERE `Dependent` = " . $depen . " 
					  AND `Level` = 2;";
					
			$hsubdiv_query = mysql_query($subdiv_query);
			$count = mysql_result($hsubdiv_query, 0, 0);

			$ssid = $_subdiv . ( ++$count );
						
			$ins_subdiv_query = "INSERT INTO `Struc_Subdivisions` VALUES								
							( $ssid, '" . trim($_name_rus) . "', '" . trim( $_name_taj) . "', 0, $depen, 'F' );";
				
			$hquery = mysql_query( $ins_subdiv_query ) or die( "ERROR: " . mysql_error() );
			
			return Subdiv::getSubdivID( $_name_dr, $_name_ft );
		}


		function updMark( $_mark_id, $_group ) {
		
			#if( Mark::isMark( $_name ) != true ) return false;
									
			$upd_mark_query = "UPDATE `Marks`
								   SET `ProductGroupID` = " . $_group . " 
								 WHERE `MarkID` = " . Mark::getMarkID( $_name );

			$upd_mark_query = "UPDATE `Marks`
				SET `ProductGroupID` = " . $_group . " 
				WHERE `MarkID` = " . $_mark_id;
		
			$hquery = mysql_query( $upd_mark_query ) or die( "ERROR: " . mysql_error() );
			
			return true;
		}

		function getGroupID( $_mark_id ) {
			
			$sel_mark_query = "SELECT * 
								 FROM `Marks`
								WHERE `MarkID` = " . $_mark_id;
			
			$hquery = mysql_query( $sel_mark_query ) or die( "ERROR: " . mysql_error() );
			
			if( mysql_num_rows( $hquery ) == 0 ) return false;
			
			$res = mysql_fetch_array( $hquery );
			return $res['ProductGroupID'];
		}
		
	};
?> 