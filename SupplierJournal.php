<?php
// Show list suppliers
/*
  $getSupplierQuery = "select * from supplier";
  $result = mysqli_query($hconnect, $getSupplierQuery);
  echo "<table border=1>";
  while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
  echo '<tr>';
  echo "<td>" . $row['address'] . "</td>";
  echo "<td>" . $row['company'] . "</td>";
  echo "<td>" . $row['phone'] . "</td>";
  echo "<td>" . $row['responsible_person'] . "</td>";
  echo "<td>" . $row['supplier_type'] . "</td>";
  echo "<td><a href=''>" . $row['supplier_type'] . "</a></td>";
  echo '</tr>';
  }
  echo "</table>";
 */

$_address = $_REQUEST['address'];
$_company = $_REQUEST['company'];
$_phone = $_REQUEST['phone'];
$_supperson = $_REQUEST['supperson'];
$_suptype = $_REQUEST['suptype'];

if (isset($_address) && $_address != "" && isset($_company) && $_company != "" && isset($_phone) && $_phone != "" && isset($_supperson) && $_supperson != "") {
    if (intval($_suptype) > 0 && intval($_suptype) < 3) {
        
        $insertSupplier = "insert into supplier(address, company, phone, responsible_person, supplier_type) values(?,?,?,?,?)";
        if (!$stmt = mysqli_prepare($hconnect, $insertSupplier)) {
            echo mysqli_stmt_error($stmt);
        }
        if (!mysqli_stmt_bind_param($stmt, 'sssss', $_address, $_company, $_phone, $_supperson, $_suptype)) {
            echo mysqli_stmt_error($stmt);
        }
        if (!mysqli_stmt_execute($stmt)) {
            echo '<h1>Данный поставщик существует</h1>';
        }  else {
            echo '<h1>Успешно</h1>';
        }
        
        mysqli_stmt_close($stmt);
    }
}
?>
<!-- add new supplier -->

<form method="post">
    <table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
        <tr>
            <td>Адресс</td>
            <td>Название Компании</td>
            <td>Телефон</td>
            <td>Ответственное лицо</td>
            <td>Тип лица</td>
        </tr>
        <tr>
            <td><input type="text" name="address"/></td>
            <td><input type="text" name="company"/></td>
            <td><input type="text" name="phone"/></td>
            <td><input type="text" name="supperson"/></td>
            <td>
                <select name="suptype">
                    <option value="1">Юридическое лицо</option>
                    <option value="2">Физическое лицо</option>
                </select>
            </td>
        </tr>
        <tr>
            <td colspan="5"><input type="submit" value="Сохранить"></td>
        </tr>
    </table>
</form>