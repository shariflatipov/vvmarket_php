<?php

if ($_SESSION['user_id'] != NULL || $_SESSION['user_id'] != "") {

    $id = intval($_GET['id']);

    if (isset($_POST['save'])) {
        $destination = "";
        if ($_FILES['image']['error'] > 0) {

            $qImage = "select image_path from product where id = ?";

            $stmt = mysqli_prepare($hconnect, $qImage);
            mysqli_stmt_bind_param($stmt, 's', $id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $destination);
            mysqli_stmt_fetch($stmt);
            mysqli_stmt_close($stmt);
        } else {
            $destination = "images/mark/" . basename($_FILES['image']['name']);
            if (move_uploaded_file($_FILES['image']['tmp_name'], $destination)) {
                echo "Картинка успешно загружена";
            }
        }

        if ($id > 0) {
            $_name = $_REQUEST['name'];
            $_barcode = $_REQUEST['barcode'];
            $_image = $destination;
            $_intCode = $_REQUEST['intcode'];
            $_minCount = $_REQUEST['mincount'];
            $_sellPrice = $_REQUEST['sellprice'];
            $_unit = $_REQUEST['unit'];
            $_isList = $_REQUEST['islist'];
            $_dependent = $_REQUEST['dependent'];

            if (!empty($_isList) && $_isList == 'on') {
                $isList = 1;
            } else {
                $isList = 0;
            }
            if ($_dependent != "none") {
                $dependent = $_dependent;
            } else {
                $dependent = 0;
            }

            //echo '$_name ' . $_name . ' $_barcode ' . $_barcode . ' $_iamage ' . $_iamage . ' $_intCode ' . $_intCode . '
            //     $_minCount ' . $_minCount . ' $_quantity ' . $_quantity . ' $sellPrice ' . $sellPrice . ' $_unit ' . $_unit;

            $qEditProduct = "update product
                                        set `name` = ?,
                                            barcode = ?,
                                            image_path = ?,
                                            internal_code = ?,
                                            min_count = ?,
                                            sell_price = ?,
                                            unit = ?
                                        where id = ?";
            if (!$stmt = mysqli_prepare($hconnect, $qEditProduct)) {
                echo mysqli_stmt_error($stmt) . "ssssssssssssssss";
            }
            if (!mysqli_stmt_bind_param($stmt, "ssssssss", $_name, $_barcode, $_image, $_intCode, $_minCount, $_sellPrice, $_unit, $id)) {
                echo mysqli_stmt_error($stmt) . "aaaaaaaaaaaaaaaaaa";
            }
            if (!mysqli_stmt_execute($stmt)) {
                echo mysqli_stmt_error($stmt) . "fffffffffffffffffff";
            }
            echo "before save to cash_update";
            cashUpdate($_barcode, $_name, $_sellPrice, $_unit, 1, $isList, $dependent, $_image);
        }
    } else if ($id > 0) {
        $qEditProduct = "SELECT pr.id,
                                pr.name,
                                pr.barcode,
                                pr.image_path,
                                pr.internal_code,
                                pr.min_count,
                                pr.sell_price,
                                pr.unit
                           FROM product pr
                           WHERE id=?";
        $stmt = mysqli_prepare($hconnect, $qEditProduct);
        mysqli_stmt_bind_param($stmt, 's', $id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $prId, $prName, $prBarcode, $prImage, $prIntCode, $prMinCount, $prSellPrice, $unit);
        mysqli_stmt_fetch($stmt);
        mysqli_stmt_close($stmt);

        echo '<form enctype="multipart/form-data" method="post">
                <table  width="100%" ALIGN=center   border="1" cellpadding="0" cellspacing="0" class="collapse">
                    <tr>
                        <td>Наименование</td>
                        <td>Штрихкод</td>
                        <td>Изображение</td>
                        <td>Внутренний код</td>
                        <td>Минимальное количество</td>
                        <td>Цена продажи</td>
                        <td>Еденица измерения</td>
                        <td>Быстрый доступ</td>
                        <td>В каком меню</td>
                    </tr>
                    <tr>
                        <td><input type="text" name="name" value="' . $prName . '"></td>
                        <td><input type="text" name="barcode" value="' . $prBarcode . '"></td>
                        <td><input type="file" name="image" value="' . $prImage . '"></td>
                        <td><input type="text" name="intcode" value="' . $prIntCode . '"></td>
                        <td><input type="text" name="mincount" value="' . $prMinCount . '"></td>
                        <td><input type="text" name="sellprice" value="' . $prSellPrice . '"></td>
                        <td>';


        $qSelectUnit = "select id, `name` from units";
        $result = mysqli_query($hconnect, $qSelectUnit);

        echo '<select name="unit">';

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $selected = "";
            if ($unit == $row['id']) {
                $selected = 'selected="selected"';
            }
            echo "<option value='" . $row['id'] . "' $selected>" . $row['name'] . "</option>";
        }
        echo '</select>';

        echo '</td>
            <td><input type="checkbox" name="islist"></td>
            <td><select name="dependent">
                        <option value="none">Нет</option>
                        <option value="10000">1</option>
                        <option value="20000">2</option>
                        <option value="30000">3</option>
                        <option value="40000">4</option>
                        <option value="50000">5</option>
                        <option value="60000">6</option>
                </select>
            </td>
                    </tr>
                    <tr>
                        <td colspan=7><td>
                        <td><input type="submit" name="save" value="Сохранить"/></td>
                    </tr>
                </table>
            </form>';

        //echo 'prId ' . $prId . '$prName ' . $prName . '$prBarcode ' . $prBarcode . '$prImage ' . $prImage . '$prImage ' . $prIntCode
        //        . '$prImage ' .  $prMinCount . '$prImage ' .  $prQuantity . '$prSellPrice ' . $prSellPrice;
    }
}
?>

