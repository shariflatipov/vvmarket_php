﻿<?php

include_once 'connectDB.php';

connect();
session_start();

if ($_GET['do'] == "logout" || $_SESSION['user_role'] <= 0) {
    session_destroy();
    header('Location: authorize.php');
    exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="css/template.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="css/menu.css" type="text/css"/>        
        <link href="css/cc_calendar.css" rel="stylesheet" type="text/css" />
        <link href="css/templ_kv.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="forms/css/jquery.autocomplete.css" type="text/css" />

        <!-- <script type="text/javascript" src="js/miscFunctions.js"></script>
        <script type="text/javascript" src="js/ast.js"></script>
        <script type="text/javascript" src="js/ast2.js"></script> -->
        <script type="text/javascript" src="js/cc_calendar.js"></script>
        <script type="text/javascript" src="js/cc_validate.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
        <script type="text/javascript" src="js/overlib_mini.js"></script>
        <script type="text/javascript" src="forms/js/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="forms/js/init_autocomplete.js"></script>
        <script type="text/javascript" src="forms/js/jquery.autocomplete.js"></script>
    </head>

    <body>
        <?php
        if (isset($_GET['do']) && trim($_GET['do'] == 'logout')) {
            session_destroy();
            header("Location: authorize.php");
        }
        ?>
        <div id = "header-banner">
            <img src = "images/header.png"/>
        </div>

        <ul id="nav">
            <li><a href="" class="dir">Документы</a>
                <ul>
                    <a href="index.php?action=show&task=receipts"><li>Приходный кассовый ордер</li></a>
                    <a href="index.php?action=show&task=expenditures"><li>Расходный кассовый ордер</li></a>
                    <a href="index.php?action=show&task=paymentorderin"><li>Прих плат поручение</li></a>
                    <a href="index.php?action=show&task=paymentorderout"><li>Расх плат поручение</li></a>
                    <a href="index.php?action=show&task=blankreceipts"><li>Приходные документы</li></a>
                    <a href="index.php?action=show&task=blankexpense"><li>Расходные документы</li></a>
                    <a href="index.php?action=show&task=return"><li>Возврат</li></a>
                    <a href="index.php?action=show&task=other_operation"><li>Прочие операции</li></a>
                </ul>
            </li>
            <li><a href="" class="dir">Товары</a>
                <ul>
                    <a href="index.php?action=show&task=productjournal"><li>Товары</li></a>
                    <a href="index.php?action=show&task=productjournalsell"><li>Проданные товары</li></a>
                    <a href="index.php?action=show&task=change_product"><li>Изменить товар</li></a>
                    <a href="index.php?action=show&task=search_product"><li>Поиск товара</li></a>
                </ul>
            </li>

            <li><a href="" class="dir">Справочники</a>
                <ul>
                    <a href="index.php?action=show&task=supplierjournal"><li>Поставщики</li></a>
                    <a href="index.php?action=show&task=recieptdocs"><li>Приходные документы</li></a>
                    <a href="index.php?action=show&task=expensedoc"><li>Расходные документы</li></a>
                    <a href="index.php?action=show&task=jobtitlejournal"><li>Должности</li></a>
                    <a href="index.php?action=show&task=accpersonjournal"><li>Подотчетные лица</li></a>
                    <a href="index.php?action=show&task=price_differences"><li>Разница</li></a>
                </ul>
            </li>

            <li><a href="" class="dir">Валюта</a>
                <ul>
                    <a href="index.php?action=show&task=add_currency"><li>Добавить</li></a>
                </ul>
            </li>

            <li><a href="" class="dir">Заявка</a>
                <ul>
                    <a href="index.php?action=show&task=inf_messame"><li>Отчёт</li></a>
                </ul>
            </li>


            <li><a href="" class="dir">Касса</a>
                <ul>
                    <a href="index.php?action=show&task=debit_kredit"><li>Кредит Дебит</li></a>
                    <a href="index.php?action=show&task=otchet_kassa"><li>Отчет кассы</li></a>
                    <a href="index.php?action=show&task=cash"><li>Ввод средств</li></a>

                </ul>
            </li>

            <li><a href="index.php?do=logout" class="dir">Выход</a></li>
        </ul>

        <div id="maincnts">
            <table align="center" width="900"  border="0" cellpadding="3" cellspacing="0" class="collapse" >
                <tr>
                    <td>
                        <?php
                        /* Parse urls for action and task */
                        if ((isset($_GET['action'])) && (isset($_GET['task']))) {
                            if ($url = allowUrl($_SESSION['user_role'], $_GET['task'])) {
                                include_once $url;                           
                            } else {
                                echo "<p align='center' style='font-size:12px;border:1px solid #f03e08;color:#ea1625;width:200px;margin-left:350px;padding:20px'><span style='padding:10px;'><img src='images/noaccess.jpg' title='Why u gotcha enter?'/></span><br />" . $langs['accessdenied'] . "! <br/ >" . $langs['noaccess'] . "</p>";
                            }
                        }
                        ?>
                    </td>
                </tr>
            </table>
            <br/>
            <div class="footer" align="center" style="background:F5F5F5 left top; border:1px solid #E8E8E8;">&copy; vvmarket 2012 All rights reserved</div><br />
        </div>
    </body>
</html>