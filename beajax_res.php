<?php

session_start();
if ($_SESSION['user_id'] != NULL || $_SESSION['user_id'] != "") {
    if (isset($_GET['args'])) {
        include("connectDB.php");
        connect();
        $sql = "SELECT pr.`name`
		     , wh.receipt_price
		     , pr.`sell_price`
		     , pr.quantity
		     , pr.unit
		FROM
		  `warehouse` wh, product pr
		WHERE
		  wh.`product_id` = pr.`id`
		  AND pr.barcode = ?
		LIMIT
		  1";

        $stmt = mysqli_prepare($hconnect, $sql);
        mysqli_stmt_bind_param($stmt, 's', $_GET['args']);

        mysqli_stmt_execute($stmt);

        mysqli_stmt_bind_result($stmt, $name, $rec_price, $sell_price, $quantity, $unit);
        mysqli_stmt_fetch($stmt);

        echo $name . "|-|-|" . $rec_price . "|-|-|" . $sell_price . "|-|-|" . $unit . "|-|-|" . $quantity;
    }
} else {
    echo ":)";
}
?>