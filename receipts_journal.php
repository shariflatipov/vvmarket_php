<form name="frmSoldProduct"  method="post">
    <table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
        <?php
        $recordsLimit = $_REQUEST['limit'];
        $txtDateFrom = $_POST['txtDateFrom'];
        $txtDateTo = $_POST['txtDateTo'];
        $supplier = $_POST['supplier'];
        $storekeeper = $_POST['storekeeper'];

        if ($_POST['txtDateFrom'] == "" || $_POST['txtDateFrom'] == null) {
            $txtDateFrom = date("Y-m-d");
        }
        if ($_POST['txtDateTo'] == "" || $_POST['txtDateTo'] == null) {
            $txtDateTo = date("Y-m-d");
        }

        if (isset($supplier) && $supplier != "" && $supplier != NULL) {
            $filterSupplier = " and su.company like ?";
            $supp = "%" . $supplier . "%";
        }

        if (isset($storekeeper) && $storekeeper != "" && $storekeeper != NULL) {
            $filterStorekeeper = " and concat(ac.surname, ' ', ac.first_name) like ?";
            $store = "%" . $storekeeper . "%";
        }

        $filterAccPerson = $filterSupplier . $filterStorekeeper;

        $qGetRecords = "SELECT concat(ac.surname, ' ', ac.first_name) AS fio
                             , su.company
                             , rd.id
                             , rd.reciept_date
                             , rd.total_sum
                        FROM
                          receiptdoc rd, acc_persons ac, supplier su
                        WHERE
                          rd.supplier_id = su.id
                          AND rd.acc_person_id = ac.id
                          AND rd.reciept_date BETWEEN ? AND ?" . $filterAccPerson;
        //echo $qGetRecords;
        if (!$stmt = mysqli_prepare($hconnect, $qGetRecords)) {
            //mysqli_stmt_error($stmt) . "sssssssssskkkkkkkkkkk";
        }

        $dateTo = $txtDateTo . " 23:59:59";
        $dateFrom = $txtDateFrom . " 00:00:00";

        if ($filterSupplier != "") {
            if (!mysqli_stmt_bind_param($stmt, 'sss', $dateFrom, $dateTo, $supp)) {
                //echo mysqli_stmt_error($stmt) . "Filter supplier => supplier";
            }
            if ($filterStorekeeper != "") {
                if (!mysqli_stmt_bind_param($stmt, 'ssss', $dateFrom, $dateTo, $supp, $store)) {
                    //echo mysqli_stmt_error($stmt) . "Filter supplier => storekeeper";
                }
            }
        } else if ($filterStorekeeper != "") {
            if (!mysqli_stmt_bind_param($stmt, 'sss', $dateFrom, $dateTo, $store)) {
                //echo mysqli_stmt_error($stmt) . "Filter storekeeper => storekeeper";
            }
            if ($filterSupplier != "") {
                if (!mysqli_stmt_bind_param($stmt, 'ssss', $dateFrom, $dateTo, $supp, $store)) {
                    //echo mysqli_stmt_error($stmt) . "Filter strorekeeper => supplier";
                }
            }
        } else {
            if (!mysqli_stmt_bind_param($stmt, 'ss', $dateFrom, $dateTo)) {
                //mysqli_stmt_error($stmt) . "sssssssssssssssssssssssssssssssssssssssssss";
            }
        }
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $fio, $receipter, $docId, $docDate, $subTotal);
        $rowsCount = mysqli_stmt_num_rows($stmt);

        echo "<tr>
                <td>Складовщик</td>
                <td>Число</td>
                <td>Поставщик</td>
                <td>Сумма</td>
                <td>Документ</td>
              </tr>";
        echo "<tr>
                <td><input name='storekeeper' type='text' id='storekeeper'
                    value='{$storekeeper}'/></td>
                <td>от <INPUT type='text' size=10 maxLength=10 readonly='readonly' 
                    name='txtDateFrom' value='" . $txtDateFrom . "'
                    onclick=\"displayCalendar(document.frmSoldProduct.txtDateFrom,'yyyy-mm-dd', this)\">
                     - до <INPUT type='text'size=10 maxLength=10 readonly='readonly'
                    name='txtDateTo' value='" . $txtDateTo . "'
                    onclick=\"displayCalendar(document.frmSoldProduct.txtDateTo,'yyyy-mm-dd', this)\"></td>";

        echo "<td><input type='text' name='supplier' id='accepted_from' value='{$supplier}'/></td><td></td>
                <td><input type='submit' value='Фильтр'></td>
              </tr>";

        while (mysqli_stmt_fetch($stmt)) {
            $i++;
            $i %= 2;
            $bgcolor = ($i ? 'lightyellow' : 'white');
            echo "<tr bgcolor=" . $bgcolor . ">";
            echo "<td valign=top>&nbsp;&nbsp;" . $fio . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $docDate . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $receipter . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $subTotal . "</td>";
            echo "<td valign=center><a style='cursor: hand'
                    onclick=\"window.open('forms/frmReceipts.php?id=" . $docId . "', '', '')\">
		    <img src='images/doc.bmp'></a></td></tr>";
            $total += $subTotal;
        }
        mysqli_stmt_close($stmt);
        ?>
        <tr>
            <td></td>
            <td>Итого</td>
            <td></td>
            <td><?php echo $total ?></td>
            <td></td>
        </tr>
    </table>
</form>