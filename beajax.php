<script language="javascript" type="text/javascript">
function getHTTPObject()
{
  if (window.ActiveXObject) return new ActiveXObject("Microsoft.XMLHTTP");
  else if (window.XMLHttpRequest) return new XMLHttpRequest();
  else
  {
     alert("Your browser does not support AJAX.");
  return null;
  }
}
// Change the value of the outputText field
function setOutput(id)
{
  if(httpObject.readyState == 4)
  {
     document.getElementById('product_' + id).value = httpObject.responseText;
  }
}

// Implement business logic
function doWork(id)
{
  alert(id);
  httpObject = getHTTPObject();
  if (httpObject != null)
  {
    httpObject.open("GET", "beajax_res.php?args="
    +document.getElementById('artikle_' + id).value, true);
    httpObject.send(null);
    httpObject.onreadystatechange = setOutput(id);
  }
}

var httpObject = null;
</script>
