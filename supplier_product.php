﻿<?php

	#include_once "template.inc";
	include_once "connectDB.php";
	include_once "functions.php";
	include_once "languages.php";

	$_supplier = $_REQUEST['supplier'];
	$_txtDate = $_REQUEST['txtDate'];
	$_date = $_REQUEST['date'];


	if(!isset($_txtDate)) $_txtDate = date('Y-m-d');
	
	/* Filters date*/
	if(isset($_date) && $_date == 'day' )
		$filter_date = "`FDate` = '$_txtDate'";
	elseif(isset($_date) && $_date == 'month')
		$filter_date = "MONTH(`FDate`) = MONTH('$_txtDate') AND YEAR(`FDate`) = YEAR('$_txtDate')";
	elseif(isset($_date) && $_date == 'year')
		$filter_date = "YEAR(`FDate`) = YEAR('$_txtDate')";


?>


<table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
<form name="frmSupplier_Prod" method="post">

<tr>

<?php
	echo '<td><table border="0"  class="collapse"><tr><td>' . $lang[$language.'_Period'] . '</td><td>
		  <INPUT size=10 maxLength=10 readonly="readonly" name="txtDate" 
		  value="' . $_txtDate . '"  
		  onclick="displayCalendar(document.frmSupplier_Prod.txtDate,\'yyyy-mm-dd\', this)" 
		  onchange="frmSupplier_Prod.submit()"></td><td>';


	createSelectDate( 'frmSupplier_Prod' );

	# field  Sender
	echo '<td>' . $lang[$language.'_Supplier'] . '</td><td>';
	
	createSelectSupplier( '', 'onchange="frmSupplier_Prod.submit()"', 0 );
	# field  Search Supplier
	echo '<input type=button value=".." 
		onclick="window.open(\'select_supplier.php\', \'popWindow\', \'dependent,width=320,height=360,left=150 ,top=100\');">';
		

	
	
?>

</td></tr></table></td>
</tr>

</form>
<tr><td>
<?php

/* TODO: Add code here */

	$supplier_once = '';
	if(!isset($_supplier) || $_supplier == '' ) exit();
	elseif($_supplier != 'all') $supplier_once =  "WHERE `SupplierID` = " . $_supplier;

	
	$supplier_query = "SELECT * FROM `Suppliers` " . $supplier_once . " ORDER BY `SupplierID`;";
	debug($supplier_query);
	$hquery = mysql_query($supplier_query);
	
	while($row = mysql_fetch_array($hquery))
		createSupp_Prod( $row['SupplierID'], $row['Name'], $filter_date ); 

	
function createSupp_Prod( $_supplier, $_supplier_name, $_filter_date ) 
{
	global $lang, $language;
		$sel_prod_query = "SELECT 
								  pr.*,
								  mk.`Name` `Name_product`, 
								  pg.`Type` `Name_type`,	
								  sp.`Name`,
								  ap.`FDate` `Date`
                             FROM 
							     `Products` pr, 
								 `ProductGroups` pg, 
								 `Marks` mk, 
								 `AccP_S_Products` ap,
								 `Suppliers` sp
			                WHERE ap.`ProductID` = pr.`ProductID` 
                              AND pr.`MarkID` = mk.`MarkID`
			                  AND mk.`ProductGroupID` = pg.`ProductGroupID` 
							  AND ap.`SupplierID` = sp.`SupplierID`
			                  AND ap.`SupplierID` = " . $_supplier . "
							  AND " . $_filter_date . "
						 GROUP BY ap.`SupplierID`, ap.`ProductID`;";	
	debug($sel_prod_query);
	$hquery = mysql_query($sel_prod_query);
	$count_prod = mysql_num_rows($hquery);
	if( !$count_prod ) return;
	
	echo '<table ALIGN=center width=90%  border="1" cellpadding="5" cellspacing="0" class="silver">';
	
	echo "<Tr><Td COLSPAN=5 class=rb>" . $lang[$language.'_Supplier'] . " \"" . $_supplier_name . " \"</tr>";

		
	echo "<Tr><Th width=25%>" . $lang[$language.'_Name'] . "</Th>";
	echo "<Th width=5%>" . $lang[$language.'_Date'] . "</Th>";
	echo "<Th width=5%>" . $lang[$language.'_Quantity'] . "</Th>";
	echo "<Th width=5%>" . $lang[$language.'_Unit'] . "</Th>";
	echo "<Th width=5%>" . $lang[$language.'_Price'] . "</Th>";
	echo "<Th width=10%>" . $lang[$language.'_Type'] . "</Th></Tr>";
	
	
		
	
	
	
	while( $row = mysql_fetch_array($hquery) ) {
		$i++;
		$i %= 2;
		$bgcolor = ($i ? 'lightyellow' : 'white');
		
		
		echo "<tr bgcolor=".$bgcolor.">"; 
		echo "<td>&nbsp;&nbsp;". $row['Name_product'] ."</td>";
		echo "<td align=center>&nbsp;&nbsp;". $row['Date'] ."</td>";
		echo "<td>&nbsp;&nbsp;".  $row['Quantity'] ."</td>";
		echo "<td>&nbsp;&nbsp;".  $row['Unit'] ."</td>";
		echo "<td>&nbsp;&nbsp;".  $row['Price'] ."</td>";		
		echo "<td>&nbsp;&nbsp;".  $row['Name_type'] ."</td>";

	}
	
	
	echo "</table><br/>";


}
?>
</tr></table>

</body></html>
