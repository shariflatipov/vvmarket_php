﻿<?php

function connect() {
    $host = 'localhost';
    $user = 'root';
    $passwd = '123';
    $database = "vvmarket";
    $port = 3306;
    global $hconnect;
    $hconnect = new mysqli($host, $user, $passwd, $database);

    if (mysqli_connect_errno ()) {
        echo "Could not connect to MySQL Server";
        exit();
    }

    $hconnect->query("SET CHARSET utf8");
    $hconnect->query("SET NAME utf8");
}

function allowUrl($role, $qs) {
    global $hconnect;
    if ($role != null && $qs != "") {

        $query = "SELECT url from sections where role_id = ? and nik_name = ?";

        if (!$stmt = mysqli_prepare($hconnect, $query)) {
            echo mysqli_error($hconnect) . 'sssssssssss';
        }
        mysqli_stmt_bind_param($stmt, 'ss', $role, $qs);

        if (!mysqli_stmt_execute($stmt)) {
            echo mysqli_error($hconnect) . 'sssssssssss';
        }

        mysqli_stmt_bind_result($stmt, $url);
        mysqli_stmt_fetch($stmt);
    }
    if ($url != null && $url != '') {
        return $url;
    }
    return false;
}

function insertGoods($docId, $barcode, $quantity, $price, $unit, $priceSell, $productName) {
    global $hconnect;
    $departmentId = $_SESSION['department'];

    $qCheckProductExistanse = "SELECT pr.id FROM product pr WHERE pr.barcode = ?";
    $stmt = mysqli_prepare($hconnect, $qCheckProductExistanse);
    mysqli_stmt_bind_param($stmt, 's', $barcode);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $productId);
    mysqli_stmt_fetch($stmt);
    mysqli_stmt_close($stmt);

    if ($productId > 0) { //product exists
        if (isChanged($productName, $priceSell, $barcode)) {
            cashUpdate($barcode, $productName, $priceSell, $unit, 1);
        }
        $qUpdateProduct = "update product set quantity = (quantity + ?), sell_price = ?, `name` = ? , deleted = 0 where id = ?";
        $stmt = mysqli_prepare($hconnect, $qUpdateProduct);
        mysqli_stmt_bind_param($stmt, 'ssss', $quantity, $priceSell, $productName, $productId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        $qUpdateWarehouse = "update warehouse set quantity = (quantity + ?), receipt_price = ? where product_id = ?";
        $stmt = mysqli_prepare($hconnect, $qUpdateWarehouse);
        mysqli_stmt_bind_param($stmt, 'sss', $quantity, $price, $productId);
        if (!mysqli_stmt_execute($stmt)) {
            echo mysqli_stmt_error($stmt) . 'ssssssss';
        }
        mysqli_stmt_close($stmt);
    } else {// New product 
        if (isChanged($productName, $priceSell, $barcode)) {
            cashUpdate($barcode, $productName, $priceSell, $unit, 0);
        }

        $qInsertNewProduct = "insert into product values(null, ?, null, null, null, null, ?, ?, ?, ?, 0)";
        $stmt = mysqli_prepare($hconnect, $qInsertNewProduct);
        mysqli_stmt_bind_param($stmt, 'sssss', $barcode, $quantity, $productName, $priceSell, $unit);
        mysqli_stmt_execute($stmt);
        $maxProductId = mysqli_stmt_insert_id($stmt);
        mysqli_stmt_close($stmt);

        $qInsertProductToWarehouse = "insert into warehouse values(null, ?, ?, ?, ?)";
        $stmt = mysqli_prepare($hconnect, $qInsertProductToWarehouse);
        mysqli_stmt_bind_param($stmt, 'ssss', $departmentId, $quantity, $price, $maxProductId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }

    $qInsertReceiptDocByProd = "insert into receiptdocbyprod values(null, ?, null, ?, ?, ?, ?, ?)";
    $stmt = mysqli_prepare($hconnect, $qInsertReceiptDocByProd);
    mysqli_stmt_bind_param($stmt, 'ssssss', $barcode, $price, $priceSell, $quantity, $unit, $docId);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);
}

function getSupplier($company) {
    global $hconnect;
    $id = 0;
    $company = trim($company);
    $usersql = "SELECT id
                    FROM `supplier`
                    WHERE company = ?";

    $stmt = mysqli_prepare($hconnect, $usersql);
    mysqli_stmt_bind_param($stmt, 's', $company);

    if (!mysqli_stmt_execute($stmt)) {
        echo mysqli_error($hconnect);
    }

    mysqli_stmt_bind_result($stmt, $id);
    mysqli_stmt_fetch($stmt);
    //echo '$id  -----  ' . $id ;
    return $id;
}

function getProductID($_name) {
    global $hconnect;
    $id = 0;

    $_product_name = trim($_name);

    $qProductId = "SELECT id
                         FROM `product`
                        WHERE `name` = ?";

    $stmt = mysqli_prepare($hconnect, $qProductId);
    mysqli_stmt_bind_param($stmt, 's', $_product_name);

    if (!mysqli_stmt_execute($stmt)) {
        echo mysqli_error($hconnect);
    }

    mysqli_stmt_bind_result($stmt, $id);
    mysqli_stmt_fetch($stmt);

    return $id;
}

function getProductIDByBarcode($_barcode) {
    global $hconnect;
    $id = 0;

    $_product_barcode = trim($_barcode);

    $qProductId = "SELECT id
                         FROM `product`
                        WHERE `barcode` = ?";

    $stmt = mysqli_prepare($hconnect, $qProductId);
    mysqli_stmt_bind_param($stmt, 's', $_product_barcode);

    if (!mysqli_stmt_execute($stmt)) {
        echo mysqli_error($hconnect);
    }

    mysqli_stmt_bind_result($stmt, $id);
    mysqli_stmt_fetch($stmt);

    return $id;
}

function cashUpdate($barcode, $productName, $price, $unit, $status, $isList = 0, $dependent = 0, $destination = "") {
    global $hconnect;

    $qDeleteCash = "delete from update_cash where barcode =? and flag = ?";
    $stmt = mysqli_prepare($hconnect, $qDeleteCash);
    mysqli_stmt_bind_param($stmt, 'ss', $barcode, $status);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_close($stmt);

    $qCountCash = "select cash_id from cash group by cash_id";
    $result = mysqli_query($hconnect, $qCountCash);
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
if($destination!="")  {      
    $qInsertCashUpdate = "insert into update_cash values(null, ?, ?, ?, ?, (select `name` from units where id = ?), ?, 0, ?, ?, ?)";
        $stmt = mysqli_prepare($hconnect, $qInsertCashUpdate);
        mysqli_stmt_bind_param($stmt, 'sssssssss', $row['cash_id'], $barcode, $productName, $price, $unit, $destination, $status, $isList, $dependent);
        mysqli_stmt_execute($stmt);
        }else{
        $qInsertCashUpdate = "insert into update_cash values(null, ?, ?, ?, ?, (select `name` from units where id = ?), (select image_path from product where barcode = ?), 0, ?, ?, ?)";
        $stmt = mysqli_prepare($hconnect, $qInsertCashUpdate);
        mysqli_stmt_bind_param($stmt, 'sssssssss', $row['cash_id'], $barcode, $productName, $price, $unit, $barcode, $status, $isList, $dependent);
        mysqli_stmt_execute($stmt);
        }
        mysqli_stmt_close($stmt);
    }
}

function isChanged($name, $sellPrice, $barcode) {
    global $hconnect;
    $getProduct = "select `name`, sell_price from product where barcode = ?";
    $stmt = mysqli_prepare($hconnect, $getProduct);
    mysqli_stmt_bind_param($stmt, 's', $barcode);
    mysqli_stmt_bind_result($stmt, $nameRecieved, $sellPriceRecieved);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_fetch($stmt);
    //echo $barcode . 'product has changed ' . $name . ' $name ' . $nameRecieved . ' $nameRecieved ' . $sellPrice . ' $sellPrice ' . $sellPriceRecieved . ' $sellPriceReceived';
    if ($name != $nameRecieved or $sellPrice != $sellPriceRecieved) {
        return TRUE;
    }
    //echo "product doesn't changed";
    return FALSE;
}

function createSelectUnit($name, $selectedId) {
    global $hconnect;

    $qSelectUnit = "select id, `name` from units";
    $result = mysqli_query($hconnect, $qSelectUnit);

    echo '<select name="' . $name . '">';

    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        $selected = "";
        if ($selectedId == $row['id']) {
            $selected = 'selected="selected"';
        }
        echo "<option value='" . $row['id'] . "' $selected>" . $row['name'] . "</option>";
    }
    echo '</select>';
}
?>