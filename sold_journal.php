<form name="frmSoldProduct"  method="post">
    <table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
        <?php
        $recordsLimit = $_REQUEST['limit'];
        $txtDateFrom = $_POST['txtDateFrom'];
        $txtDateTo = $_POST['txtDateTo'];
        $acceptedFrom = $_POST['storekeeper'];

        if ($_POST['txtDateFrom'] == "" || $_POST['txtDateFrom'] == null) {
            $txtDateFrom = date("Y-m-d");
        }
        if ($_POST['txtDateTo'] == "" || $_POST['txtDateTo'] == null) {
            $txtDateTo = date("Y-m-d");
        }

        if ($acceptedFrom != "all" && $acceptedFrom != "" && $acceptedFrom != NULL) {
            $filterAccPerson = " and concat(ac.surname, ' ', ac.first_name) like ?";
        }

        $qGetRecords = "SELECT concat(ac.surname, ' ', ac.first_name) AS fio
                             , su.company
                             , ex.id
                             , ex.expence_date
                             , ex.total_sum
                        FROM
                          expensedoc ex, acc_persons ac, supplier su
                        WHERE
                          ex.reciepter_id = su.id
                          AND ex.acc_person_id = ac.id
                          and ex.expence_date BETWEEN ? AND ?" . $filterAccPerson;
        
        $stmt = mysqli_prepare($hconnect, $qGetRecords);

        $dateTo = $txtDateTo . " 23:59:59";
        $dateFrom = $txtDateFrom . " 00:00:00";

        if (isset($filterAccPerson)) {
            $storekeeper = "%" . $acceptedFrom . "%";
            mysqli_stmt_bind_param($stmt, 'sss', $dateFrom, $dateTo, $storekeeper);
        } else {
            mysqli_stmt_bind_param($stmt, 'ss', $dateFrom, $dateTo);
        }
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $fio, $receipter, $docId, $docDate, $subTotal);
        $rowsCount = mysqli_stmt_num_rows($stmt);
        //echo $rowsCount;

        echo "<tr>
                <td>Продовец</td>
                <td>Число</td>
                <td>Покупатель</td>
                <td>Сумма</td>
                <td>Документ</td>
              </tr>";
        echo "<tr>
                <td><input name='storekeeper' type='text' id='storekeeper'
                                                       size='40%' value='{$acceptedFrom}' /></td>
                <td>от <INPUT type='text' size=10 maxLength=10 readonly='readonly' name='txtDateFrom' value='" . $txtDateFrom . "' onclick=\"displayCalendar(document.frmSoldProduct.txtDateFrom,'yyyy-mm-dd', this)\">
                     - до <INPUT type='text'size=10 maxLength=10 readonly='readonly' name='txtDateTo' value='" . $txtDateTo . "' onclick=\"displayCalendar(document.frmSoldProduct.txtDateTo,'yyyy-mm-dd', this)\"></td>
                <td></td>
                <td></td>
                <td><input type='submit' value='Фильтр'></td>
              </tr>";

        while (mysqli_stmt_fetch($stmt)) {
            $i++;
            $i %= 2;
            $bgcolor = ($i ? 'lightyellow' : 'white');

            echo "<tr bgcolor=" . $bgcolor . ">";
            echo "<td valign=top>&nbsp;&nbsp;" . $fio . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $docDate . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $receipter . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $subTotal . "</td>";
            echo "<td valign=center><a style='cursor: hand' onclick=\"window.open('forms/frmExpense.php?id=" . $docId . "', '', '')\">
		  <img src='images/doc.bmp'></a></td></tr>";
            $total += $subTotal;
        }
        mysqli_stmt_close($stmt);
        ?>
        <tr>
            <td></td>
            <td>Итого</td>
            <td></td>
            <td><?php echo $total ?></td>
            <td></td>
        </tr>
    </table>
</form>