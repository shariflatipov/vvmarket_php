<script type="text/javascript">
	
	function getHTTPObject() {
		if (window.ActiveXObject)
			return new ActiveXObject("Microsoft.XMLHTTP");
		else if (window.XMLHttpRequest)
			return new XMLHttpRequest();
		else {
			alert("Your browser does not support AJAX.");
			return null;
		}
	}

	// Change the value of the outputText field
	function setOutput() {
		if (httpObject.readyState == 4) {
			//product
			var pos = strpos(httpObject.responseText, "|-|-|");
			var product = httpObject.responseText.substr(0, pos);

			//price
			var len_product = product.length;
			var price_text = httpObject.responseText.substr(len_product + 5);
			pos = strpos(price_text, "|-|-|");
			var price = price_text.substr(0, pos);

			//sellprice
			var len_price = price.length;
			var priceSell_text = httpObject.responseText.substr(len_price + len_product + 10);
			pos = strpos(priceSell_text, "|-|-|");
			var priceSell = httpObject.responseText.substr(len_price + len_product + 10, pos);

			//UnitID
			var len_priceSell = priceSell.length;
			var Unit_text = httpObject.responseText.substr(len_price + len_product + len_priceSell + 15);
			pos = strpos(Unit_text, "|-|-|");
			var unit = httpObject.responseText.substr(len_price + len_product + len_priceSell + 15, pos);

			//quantity
			var len_unit = unit.length;
			var quantity = httpObject.responseText.substr(len_price + len_product + len_priceSell + len_unit + 20);

			//alert(httpObject.responseText);
			//var quan_pos = strpos(price_text, "|-|-|");

			document.getElementById('product_' + globID).value = product;
			document.getElementById('price_' + globID).value = price;
			document.getElementById('sell_price_' + globID).value = priceSell;
			document.getElementById('quantity_' + globID).value = quantity;
			document.getElementById('unit_' + globID).value = unit;
		}
	}

	var pos = null;
	var product = null;
	// Find position of first occurrence of a string
	function strpos(haystack, needle, offset) {
		var i = haystack.indexOf(needle, offset);
		// returns -1
		return i >= 0 ? i : false;
	}

	// Implement business logic
	function doWork(id) {
		globID = id;
		httpObject = getHTTPObject();
		if (httpObject != null) {
			if (document.getElementById('artikle_' + id).value.length >= 3) {
				httpObject.open("GET", "beajax_res.php?args=" + document.getElementById('artikle_' + id).value, true);
				httpObject.send(null);
				//alert(setOutput);
				httpObject.onreadystatechange = setOutput;
			}
		}
	}
	
	function xenter(event, count) {
        if (event.keyCode == '13') {
            event.preventDefault();
            doWork(count);
        }
    }

	var globID = null;
	var httpObject = null;

</script>

<?php
//require_once "inc/headers.inc";
//require_once 'classes/acc_person.php';

$form_name = 'frmReceipts';
$_task = $_REQUEST['task'];
$_date = $_REQUEST['txtDate'];
if (!isset($_date))
    $_date = date('Y-m-d');

$cmdSave = "<input type='submit' value='Сохранить'
onclick=\"if(window.confirm('Вы действительно собираетесь сохранить документ?'))" . $form_name . ".task.value='save'; else return false;\" >";
$cmdCancel = "<input type='reset' value='Отменить'>";

/* ---------------------------------------
 *
 * Save block start
 *
 * -------------------------------------------------- */

// COUNT ELEMENTS IN FORM
$elementsCount = count($_POST);
$rowsCount = ($elementsCount - 6) / 8;

if (isset($_task) && $_task == 'save') {
    $_supplier = $_REQUEST['supplier'];

    $supplier_id = getSupplier($_supplier);
    $acc_person_id = $_SESSION['user_id'];

    $ins_query = "INSERT INTO `receiptdoc` VALUES(null, $acc_person_id, '{$_date}', $supplier_id, 0)";

    mysqli_autocommit($hconnect, FALSE);

    if (mysqli_query($hconnect, $ins_query)) {
        $docId = mysqli_insert_id($hconnect);

        $sum = 0;
        $count = 0;

        while ($count++ <= $rowsCount) {
            //echo 'we are in while block';

            $_quantity = (double) str_replace(",", ".", $_REQUEST['quantity_' . $count]);
            $_price = (double) str_replace(",", ".", $_REQUEST['price_' . $count]);
            $_productName = $_REQUEST['product_' . $count];
            $_barcode = trim($_REQUEST['artikle_' . $count]);
            $_unit = $_REQUEST['unit_' . $count];
            $_priceSell = (double) str_replace(",", ".", $_REQUEST['sell_price_' . $count]);

            if ($_price == '' || $_quantity == '' || $_barcode == '' || $_productName == '') {
                continue;
            } else {
                $total = $_quantity * $_price;
            }
            $sum += $total;
            insertGoods($docId, $_barcode, $_quantity, $_price, $_unit, $_priceSell, $_productName);
            //echo "$docId, $_barcode, $_quantity, $_price, $_unit, $_priceSell, $_productName";
        }

        $qUpdateReceiptDoc = "update receiptdoc set total_sum = ? where id = ?";
        $stmt = mysqli_prepare($hconnect, $qUpdateReceiptDoc);
        mysqli_stmt_bind_param($stmt, 'ss', $sum, $docId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);

        //echo 'we are in commit block';
        mysqli_commit($hconnect);
    } else {
        mysqli_rollback($hconnect);
    }
}

/* -------------------------------------------------------------------
 *
 * Save block end
 *
 * ----------------------------------------------------------------- */
?>

<!--------------------------------
View part
-------------------------------->

<body>
<form name="frmReceipts" method="post">
<table ALIGN=center width=100% border="1" cellpadding="5"
cellspacing="0" class="collapse">
<tr>
<td>
<fieldset><legend>Приходная накладная</legend>
<table valign=top align=center border="1" width="50%" cellpadding="0"
cellspacing="0">
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>Поставщик</td>
<td><input name="supplier" type="text" id="acc_person"
size="60%" class="line" /></td>
</tr>
<tr>
<td>Принимающий</td>
<td><input name="acc_person" readonly="readonly" type="text" value = "<?= $_SESSION['fio'] ?>"
size="60%" class="line" /></td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table border="0" cellspacing="0" cellpadding="0" align=center>

<tr>
<td class="main" align="center"><INPUT size=10 maxLength=10
readonly="readonly" name="txtDate" class="line"
value="<?= $_date ?>"
onclick="displayCalendar(document.frmReceipts.txtDate,'yyyy-mm-dd', this)" />
</td>
</tr>
</table>
<br />

<table width="100%" ALIGN=center border="1" cellpadding="0"
cellspacing="0" class="collapse" id="dataStoreTable">

<tr>
<th width="5%">№</th>
<th width="10%">Штрихкод</th>
<th width="30%">Название</th>
<th width="15%">ед. изм</th>
<th></th>
<th width="10%">Количество</th>
<th width="10%">Цена</th>
<th width="10%">Цена для продажи</th>
<th width="15%">Сумма</th>
</tr>

<?php
$count = 0;

while ($count++ <= 25) {

    echo '<tr>
<td align=center><input type="text" id="number_' . $count . '" name="number_' . $count . '" size="4"  class="bordernone" value="' . $count . '"/></td>
<td align=center><input type="text" id="artikle_' . $count . '" name="artikle_' . $count . '" size="8"  class="bordernone "  onkeypress="xenter(event, ' . $count . ')"/></td>
<td align=center width=50% nowrap=nowrap><input type="text" id="product_' . $count . '" name="product_' . $count . '" size="50" class="bordernone"/></td>
<td align=center><select style="width:40" name="unit_' . $count . '" id = "unit_' . $count . '" SIZE=1 class=bordernone >';

    $sel_unit_query = "SELECT * FROM `units`;";
    $hquery = mysqli_query($hconnect, $sel_unit_query);

    while ($row = mysqli_fetch_array($hquery, MYSQLI_ASSOC))
        echo "<OPTION VALUE=" . $row['id'] . ">" . $row['name'] . "</OPTION>";

    # field  Add Unit
    echo '</select><td><input type=button value=".." class=cmdbordernone
onclick="window.open(\'addUnit.php\', \'popWindow\', \'dependent,width=280,height=240,left=150 ,top=100\');" >';

    echo '</td>
<td align=center><input type="text" name="quantity_' . $count . '" id = "quantity_' . $count . '" size="4" class="bordernone"
onkeyup="return calcTotal( ' . $count . ' )"/></td>
<td align=center><input type="text" name="price_' . $count . '" id="price_' . $count . '" size="8" class="bordernone"
onkeyup="return calcTotal( ' . $count . ' )"/></td>';

    # field  Add Unit
    echo '<td align=center><input type="text" name="sell_price_' . $count . '" id="sell_price_' . $count . '" size="8"  class="bordernone"/></td>
<td align=center>
<input type="text" name="total_' . $count . '" size="8" readonly="readonly" class="bordernone"/>
</td></tr>';
}

echo '
<tr>
	<td align=center></td>
	<td colspan="2">&nbsp;Итого:</td>
	<td align=center>
	<input type="text" name="total" size="8" readonly="readonly" class="bordernone"/>
	</td>
</tr>';
?>
</table>
</td>
</tr>
<tr>
	<td>
	<table width="80%" border="0" cellspacing="0" cellpadding="1">
		<tr>
			<td><?= $cmdSave . $cmdCancel; ?> <!--<input value="+" type="button" onclick="addRowToTable('dataStoreTable')"/>
			<input value="-" type="button" onclick="removeRowFromTable('dataStoreTable')"/>--></td>
		</tr>
	</table></td>
</tr>

</table>

<input type="hidden" name=task value="none"/>
</fieldset>
</td>
</tr>
</table>
</form>
</body>
