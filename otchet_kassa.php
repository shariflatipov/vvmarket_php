<form name="frmSoldProduct"  method="post">
    <table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">
        <?php
        $txtDateFrom = $_POST['txtDateFrom'];
        $txtDateTo = $_POST['txtDateTo'];

        if ($_POST['txtDateFrom'] == "" || $_POST['txtDateFrom'] == null) {
            $txtDateFrom = date("Y-m-d");
        }
        if ($_POST['txtDateTo'] == "" || $_POST['txtDateTo'] == null) {
            $txtDateTo = date("Y-m-d");
        }



        $qGetRecords = "SELECT concat(ac.surname, ' ', ac.first_name) cashier_name
                             , (SELECT sum(ex.total_sum)
                                FROM
                                  expensedoc ex
                                WHERE
                                  acc_person_id = cs.acc_person_id
                                  AND ex.expence_date BETWEEN ? AND ?) AS total
                        FROM
                          acc_persons ac, cash cs
                        WHERE
                          cs.acc_person_id = ac.id;";
        $stmt = mysqli_prepare($hconnect, $qGetRecords);

        $dateTo = $txtDateTo . " 23:59:59";
        $dateFrom = $txtDateFrom . " 00:00:00";


        mysqli_stmt_bind_param($stmt, 'ss', $dateFrom, $dateTo);

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $cash, $total);
        $rowsCount = mysqli_stmt_num_rows($stmt);
        //echo $rowsCount;

        echo "<tr>
                <td>от <INPUT type='text' size=10 maxLength=10 readonly='readonly' name='txtDateFrom' value='" . $txtDateFrom . "' onclick=\"displayCalendar(document.frmSoldProduct.txtDateFrom,'yyyy-mm-dd', this)\">
                     - до <INPUT type='text'size=10 maxLength=10 readonly='readonly' name='txtDateTo' value='" . $txtDateTo . "' onclick=\"displayCalendar(document.frmSoldProduct.txtDateTo,'yyyy-mm-dd', this)\"></td>
                         <td><input type='submit' value='Фильтр'/></td>
              </tr>";
        echo "<tr>
                <td>Касса</td>
                <td>Сумма</td>
              </tr>";

        $totalAll = 0;
        $i = 0;
        while (mysqli_stmt_fetch($stmt)) {
            $i++;
            $i %= 2;
            $bgcolor = ($i ? 'lightyellow' : 'white');

            echo "<tr bgcolor=" . $bgcolor . ">";
            echo "<td valign=top>&nbsp;&nbsp;" . $cash . "</td>";
            echo "<td valign=top>&nbsp;&nbsp;" . $total . "</td>";
            $totalAll += $total;
        }
        mysqli_stmt_close($stmt);
        ?>
        <tr>
            <td>Итого</td>
            <td><?php echo $totalAll ?></td>
        </tr>
    </table>
</form>