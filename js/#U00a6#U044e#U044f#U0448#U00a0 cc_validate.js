/*-------------------------------------------------------------------------------------------------------
	CAGE CMS
	
	VALIDATION FUNCTIONS
	
	(c) 2006-2008 CAGE Web Design | Rolf van Gelder - http://www.cagewebdev.com/ - http://www.cage.nl/
	
	Revision: 28-02-2008

	CCV_validateform(list)

	CCV_is_alpha(str)
	CCV_is_alphanumeric(str)
	CCV_is_currency(currency)
	CCV_is_date(date,language)
	CCV_is_email(email)
	CCV_is_initials(str)
	CCV_is_integer(integer)	
	CCV_is_number(num)
	CCV_is_phone(phone)
	CCV_is_postcode(postcode)
	CCV_is_prefix(str)
	CCV_is_url(url)

	CCV_mark_error(id,color,px)
	CCV_clear_error(id)
	
-------------------------------------------------------------------------------------------------------*/
function CCV_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=CCV_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

// DEFAULT VALUES
var CCV_retcode        = false;
var CCV_userfunc       = '';

var CCV_language       = 'nl';
var CCV_bordercolor    = 'red';
var CCV_bordersize     = 2;
var CCV_borderstyle    = 'dotted';
var CCV_default_border = 'solid #669999 1px';
var CCV_nrofargs       = 3;

var special_letters    = '���������������������������������';

// MAIN FUNCTION
function CCV_validateform()
{	args=CCV_validateform.arguments;
	CCV_retcode = false;
	// CLEAR ALL ERRORS
	for (i=0; i<(args.length-2); i+=CCV_nrofargs)
	{	CCV_clear_error(args[i]);
	}
	for (i=0; i<(args.length-2); i+=CCV_nrofargs)
	{	// CONDITION TO TEST
		test=args[i+2];
		// FUNCTION TO PERFORM IN CASE OF ERROR
		CCV_retvalue = args[i+3];
		
		fieldname = args[i];
		field=CCV_findObj(fieldname);
		fieldtitle = args[i+1];
		var specialchars = '';
		if(test.indexOf('[')>0)
		{	specialchars = test.substr(test.indexOf('[')+1)
			specialchars = specialchars.substr(0,specialchars.length-1);
			test = test.substr(0,test.indexOf('['));
		}
		test = test.toUpperCase();
		if(test.substr(0,2)=='RLM')
		{	var max_len = parseInt(test.substr(3));
			test = 'RLM';
		}		
		if(test.substr(0,2)=='LM')
		{	var max_len = parseInt(test.substr(2));
			test = 'LM';
		}
		if(test.substr(0,3)=='RLP')
		{	var precise_len = parseInt(test.substr(3));
			test = 'RLP';
		}
		if(test.substr(0,2)=='LP')
		{	var precise_len = parseInt(test.substr(2));
			test = 'LP';
		}
		
		// REQUIRED
		if(test.substr(0,1)=='R')
		{
			if(field.value=='')
			{	if(CCV_userfunc!='')
				{	// PERFORM USER ERROR FUNCTION
					eval(CCV_userfunc);
				}
				CCV_mark_error(fieldname);
				field.focus();
				if(CCV_language=='nl')
				{	alert('Verplicht veld: <'+fieldtitle.toUpperCase()+'>');
				} else
				{	alert('Required field: <'+fieldtitle.toUpperCase()+'>');
				}
				return false;
			}
		}

		// ALPHABETIC (A..Z) [+SPECIAL CHARS]		
		if(test=='A'||test=='RA')
		{	if(field.value.length>0)
			{	if(!(CCV_is_alpha(field.value,specialchars)))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(specialchars!='')
					{	if(CCV_language=='nl')
						{	alert('Het veld <' + fieldtitle.toUpperCase() + '> mag alleen letters en <'+specialchars+'> bevatten');
						} else
						{	alert('The field <' + fieldtitle.toUpperCase() + '> may only contain letters and <'+specialchars+'>');
						}
					} else
					{	if(CCV_language=='nl')
						{	alert('Het veld <' + fieldtitle.toUpperCase() + '> mag alleen letters bevatten');
						} else
						{	alert('The field <' + fieldtitle.toUpperCase() + '> may only contain letters');
						}
					}
					return false;
				}
			}
		}
		
		// ALPHANUMERIC - LETTERS AND NUMBERS [+SPECIAL CHARS]		
		if(test=='AN'||test=='RAN')
		{	if(field.value.length>0)
			{	if(!(CCV_is_alphanumeric(field.value,specialchars)))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(specialchars!='')
					{	if(CCV_language=='nl')
						{	alert('Het veld <' + fieldtitle.toUpperCase() + '> mag alleen letters, cijfers en <'+specialchars+'> bevatten');
						} else
						{	alert('The field <' + fieldtitle.toUpperCase() + '> may only contain letters, digits and <'+specialchars+'>');
						}
					} else
					{	if(CCV_language=='nl')
						{	alert('Het veld <' + fieldtitle.toUpperCase() + '> mag alleen letters en cijfers bevatten');
						} else
						{	alert('The field <' + fieldtitle.toUpperCase() + '> may only contain letters and digits');
						}
					}
					return false;
				}
			}
		}

		// CURRENCY		
		if(test=='C'||test=='RC')
		{	if(field.value.length>0)
			{	if(!CCV_is_currency(field.value))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldig bedrag: <'+field.value+'>');
					} else
					{	alert('Invalid currency: <'+field.value+'>');
					}					
					return false;
				}
			}
		}
		
		// DATE						
		if(test=='D'||test=='RD')
		{	if(field.value.length>0)
			{	if(!CCV_is_date(field.value,CCV_language))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldige datum: <'+field.value+'>');
					} else
					{	alert('Invalid date: <'+field.value+'> - Format: mm/dd/yyyy');
					}
					return false;
				}
			}
		}

		// EMAIL ADDRESS
		if (test=='E'||test=='RE')
		{	if(field.value.length>0)
			{	if(!CCV_is_email(field.value))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldig EMAILADRES: <'+field.value+'>');
					} else
					{	alert('Invalid EMAIL ADDRESS: <'+field.value+'>');
					}
					return false;
				}
			}
		}

		// INTEGER
		if(test=='I'||test=='RI')
		{	if(field.value.length>0)
			{	if(!CCV_is_integer(field.value))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('<'+fieldtitle.toUpperCase()+'> moet een geheel getal zijn');
					} else
					{	alert('<'+fieldtitle.toUpperCase()+'> has to be an integer');
					}					
					return false;
				}
			}
		}

		// MAX LENGTH		
		if(test=='LM'||test=='RLM')
		{	if(field.value.length>0)
			{	if(field.value.length > max_len)
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Het veld <' + fieldtitle.toUpperCase() + '> moet maximaal '+max_len+' tekens lang zijn');
					} else
					{	alert('The field <' + fieldtitle.toUpperCase() + '> can be maximal '+max_len+' characters');
					}					
					return false;
				}
			}
		}

		// PRECISE LENGTH		
		if(test=='LP'||test=='RLP')
		{	if(field.value.length>0)
			{	if(field.value.length != precise_len)
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Het veld <' + fieldtitle.toUpperCase() + '> moet '+precise_len+' tekens lang zijn');
					} else
					{	alert('The field <' + fieldtitle.toUpperCase() + '> has to be '+precise_len+' characters long');
					}					
					return false;
				}
			}
		}

		// NUMERIC		
		if(test=='N'||test=='RN')
		{	if(field.value.length>0)
			{	if(!CCV_is_number(field.value))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('<'+fieldtitle.toUpperCase()+'> moet een getal zijn');
					} else
					{	alert('<'+fieldtitle.toUpperCase()+'> has to be a number');
					}					
					return false;
				}
			}
		}

		// POSTAL CODE (DUTCH: 9999AA OR 9999 AA)		
		if(test=='PNL'||test=='RPNL')
		{	if(field.value.length>0)
			{	if(!(CCV_is_postcode(field.value)))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldige POSTCODE: <'+field.value+'>');
					} else
					{	alert('Invalid POSTAL CODE: <'+field.value+'>');
					}					
					return false;
				}
			}
		}

		// PHONE NUMBER		
		if(test=='T'||test=='RT')
		{	if(field.value.length>0)
			{	if(!(CCV_is_phone(field.value)))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldig TELEFOONNUMMER: <'+field.value+'>');
					} else
					{	alert('Invalid PHONE NUMBER: <'+field.value+'>');
					}					
					return false;
				}
			}
		}

		// URL		
		if(test=='U'||test=='RU')
		{	if(field.value.length>0)
			{	if(!CCV_is_url(field.value))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldige URL: <'+field.value+'>');
					} else
					{	alert('Invalid URL: <'+field.value+'>');
					}					
					return false;
				}
			}
		}

		// VOORLETTERS (INITIALS)		
		if(test=='VL'||test=='RVL')
		{	if(field.value.length>0)
			{	if(!(CCV_is_initials(field.value)))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldige VOORLETTERS: <'+field.value+'>');
					} else
					{	alert('Invalid INITIALS: <'+field.value+'>');
					}					
					return false;
				}
			}
		}

		// VOORVOEGSELS (PREFIXES)		
		if(test=='VV'||test=='RVV')
		{	if(field.value.length>0)
			{	if(!(CCV_is_prefix(field.value)))
				{	if(CCV_userfunc!='')
					{	// PERFORM USER ERROR FUNCTION
						eval(CCV_userfunc);
					}
					CCV_mark_error(fieldname);
					field.focus();
					if(CCV_language=='nl')
					{	alert('Ongeldige VOORVOEGSELS: <'+field.value+'>');
					} else
					{	alert('Invalid PREFIX: <'+field.value+'>');
					}					
					return false;
				}
			}
		}
	}
	
	CCV_retcode = true;
	
	return true;
	
}

// ALPHA: CONTAINS ONLY LETTERS, SPACES and SPECIAL CHARS
function CCV_is_alpha(str,specialchars)
{
	if(specialchars!='')
	{	var xp = '/^([A-Za-z'+special_letters+'\ '+specialchars+']*)$/';
	} else
	{	var xp = '/^([A-Za-z'+special_letters+'\ ]*)$/';
	}
	var alphaRegxp = new RegExp(eval(xp));
	return alphaRegxp.test(str);
}

// ALPHA-NUMERIC: CONTAINS ONLY LETTERS AND DIGITS AND SPACES
function CCV_is_alphanumeric(str,specialchars)
{	if(specialchars!='')
	{	var xp = '/^([A-Za-z0-9'+special_letters+'\ '+specialchars+']*)$/';
	} else
	{	var xp = '/^([A-Za-z0-9'+special_letters+'\ ]*)$/';
	}
	var alphaRegxp = new RegExp(eval(xp));
	return alphaRegxp.test(str);
}

// CURRENCY
function CCV_is_currency(currency)
{	var xp = /^\d*(\,\d\d)?$/;
	return xp.test(currency);
}

// DATE
var minYear=1900;
var maxYear=2100;
var daysinmonth = new Array ( 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function CCV_is_date(dtStr,sLanguage){
	var mylang = 'nl';
	if ( dtStr == '' )
	{	return true;
	}
	if(typeof sLanguage!="string")
	{	mylang = 'nl';
	} else
	{	mylang = sLanguage;
	}
	if(mylang=="nl")
	{	var dtCh= "-";
	} else
	{	var dtCh="/";
	}
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	if(mylang=='nl')
	{	// dd-mm-jjjj
		var strDay=dtStr.substring(0,pos1);
		var strMonth=dtStr.substring(pos1+1,pos2);
	} else
	{	// mm/dd/yyyy
		var strDay=dtStr.substring(pos1+1,pos2);
		var strMonth=dtStr.substring(0,pos1);
	}
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	if ( !CCV_is_integer(strMonth) )
	{	return false;
	}
	if ( !CCV_is_integer(strDay) )
	{	return false;	
	}
	if ( !CCV_is_integer(strYear) )
	{	return false;	
	}
	
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysinmonth[month]){
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || CCV_is_integer(stripCharsInBag(dtStr, dtCh))==false){
		return false
	}
	return true
}

// EMAIL
function CCV_is_email(email)
{	// VALIDATES AN EMAIL ADDRESS
	var emailRegxp = /(^[a-z0-9]([a-z0-9_\.\-]*)@([a-z0-9_\.\-]*)([.][a-z0-9]{2,3})$)|(^[a-z0-9]([a-z0-9_\.\-]*)@([a-z0-9_\.\-]*)(\.[a-z0-9]{2,3})(\.[a-z0-9]{2,3})*$)/i;	
	return emailRegxp.test(email);
}

// INITIALS
function CCV_is_initials(voorletters)
{	var xp = /^([A-Za-z\.]*)$/;
	return xp.test(voorletters);
}

// INTEGER
function CCV_is_integer(int2test)
{	// VALIDATES AN INTEGER
	var xp = /(^-?\d\d*$)/;
	return xp.test(int2test);
}

// NUMBER
function CCV_is_number(num2test)
{	// VALIDATES A NUMBER
	var xp = /(^[0-9\.\,]*)$/;
	return xp.test(num2test);
}

// PHONE NUMBER
function CCV_is_phone(telefoon)
{	// VALIDATES A PHONE NUMBER
	var xp = /^([0-9\(\)\+\-\ ]*)$/;
	return xp.test(telefoon);
}

// PREFIX
function CCV_is_prefix(voorvoegsels)
{	var xp = /^([A-Za-z\'\ ]*)$/;
	return xp.test(voorvoegsels);
}

// POSTAL CODE (DUTCH: 5615HG or 5615 HG)
function CCV_is_postcode(postcode)
{	// VALIDATES A POSTCODE (DUTCH: 9999AA or 9999 AA)
	if(postcode.length==7)
	{	if(postcode.substr(4,1)!=' ')
		{	return false;
		}
		postcode = postcode.substr(0,4)+postcode.substr(5,2);
	}
	if(postcode.length!=6)
	{	return false;
	}
	var pcodeRegxp = /^([0-9]{4})([A-Za-z]{2})$/;
	return pcodeRegxp.test(postcode);
}

// URL
function CCV_is_url(url)
{	// VALIDATES AN URL
	var Regxp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	return Regxp.test(url);
}


// ------------------------------------------------------------------------------
// BORDER FUNCTIONS
// ------------------------------------------------------------------------------
function CCV_mark_error(id,color,px,bstyle)
{	var bordercolor;
	if(typeof color != 'string')
	{	bordercolor = CCV_bordercolor;
	} else
	{	if(color!='')
		{	bordercolor = color;
		} else
		{ bordercolor = CCV_bordercolor;
		}
	}	
	var bordersize;
	if (typeof px != 'number')
	{	bordersize = CCV_bordersize;
	} else
	{	bordersize = px;
	}
	var borderstyle;
	if(typeof bstyle != 'string')
	{	borderstyle = CCV_borderstyle;
	} else
	{	borderstyle = bstyle;
	}
	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById(id).style.border = borderstyle + " " + bordercolor + " " + bordersize + "px";
	} else { 
		if (document.layers) {
			document.id.border = borderstyle + " " + bordercolor + " " + bordersize + "px";
		} else {
			document.all.id.style.border = borderstyle + " " + bordercolor + " " + bordersize + "px";
		}
	}
}
function CCV_clear_error(id)
{	if (document.getElementById) { // DOM3 = IE5, NS6
		document.getElementById(id).style.border = CCV_default_border;

	} else { 
		if (document.layers) {
			document.id.border = CCV_default_border;
		} else {
			document.all.id.style.border = CCV_default_border;
		}
	}
}
