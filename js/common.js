window.onerror = function(){;};

function popup(url, width, height, wname){
	
	if (typeof wname=='undefined') var wname="SKMPopup";

	var features;
	features = 'location=0,menubar=0,scrollbars,resizable,dependent,status=0,toolbar=0,left=0,screenX=0,top=0,screenY=0';

	if (window.screen) {
		var ah = screen.availHeight - 30;
		var aw = screen.availWidth - 10;

		if (width > aw){width = aw}
		if (height > ah){height = ah}

	}
	features += ",width=" + width + ",innerWidth=" + width;
	features += ",height=" + height + ",innerHeight=" + height;

	var wnd = window.open(url, wname, features);
	wnd.focus();
}
