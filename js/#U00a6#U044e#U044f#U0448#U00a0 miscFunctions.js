﻿/*
This functions were written by Samiev Suhrob,
experienced web developer and software engineer

write2me:suhrob87@gmail.com
*/

/**-----------------------------------------------------------------------------------------*/
function ajaxFunction(queryStr)
{
var qstr = queryStr;
var xmlHttp;
var responseTxt;
try
  {
  // Firefox, Opera 8.0+, Safari
  xmlHttp=new XMLHttpRequest();
  }
catch (e)
  {
  // Internet Explorer
  try
    {
    xmlHttp=new ActiveXObject("MSXML6.XMLHTTP");
    }
  catch (e)
    {
    try
      {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    catch (e)
      {
      alert("Your browser does not support AJAX!");
      return false;
      }
    }
  }
  xmlHttp.onreadystatechange=function()
    {
    if(xmlHttp.readyState==4)
      {
       responseTxt = xmlHttp.responseText;
      }
    }
  xmlHttp.open('GET', qstr, false);
  xmlHttp.send(null);
  return responseTxt;
  }



function RequestStudentFullname(field)
{
	var stud_id = field.value;
	var engine = 'inc/ReturnStudentFullname.php';
	var query = engine + "?login=" + stud_id;
    //alert(query);
    var txt = ajaxFunction(query);
return txt;



}
  
function setFoc(){
	document.PrihodniyKassoviyOrder1.kv_num1.focus();
}


function intcheck(val){
    var tmp = parseInt(val);
	if (isNaN(tmp)){
        alert("not numeric");
	}
    else{
        alert("numeric");
	}



}





/*---------------------------------------------------------

Скрипт для написание числа прописью
Автор Самиев Сухроб Сатторович -> Software engineer with an utmost experience in web programming !
email:suhrob87@gmail.com
The most dangerous thing in this world is an idea
The most dangerous man in this world is a man with an idea.
---------------------------------------------------------*/
function digitsInWords(field){
		//document.characterSet='UTF-8';
		field.onclick = function(){
		if (field.value == 0)
            field.value ='';
		}
		var fval = String(field.value);
		var result;
		//alert(num);
		var ar = new Array();
		var bool = true;
		for (var i=0;i<fval.length;i++){
			if (fval.charCodeAt(i)<48 || fval.charCodeAt(i)>57){
				alert("Танхо ракамро дохил намоед !");
				field.focus();
				field.value = 0;
				field.style.borderColor="red";
				//field.style.backgroundColor='red';
				bool = false;
				break;
			}else{
				field.style.borderColor='#000000';
				ar[i] = fval.charAt(i);
			}
		}
		if (bool == true){
			var dec = new Array();
			var du = new Array();
			var hundred = 'сад';
			var thousand ='хазор';
			var million = 'миллион';
			var va ='у ';
			dec[1]='як';
			dec[2]='ду';
			dec[3]='се';
			dec[4]='чор';
			dec[5]='панч';
			dec[6]='шаш';
			dec[7]='хафт';
			dec[8]='хашт';
			dec[9]='нух';
			dec[10]='дах';
			dec[11]='ёздах';
			dec[12]='двоздах';
			dec[13]='сенздах';
			dec[14]='чордах';
			dec[15]='понздах';
			dec[16]='шонздах';
			dec[17]='хабдах';
			dec[18]='хаждах';
			dec[19]='нуздах';

			du[1]='дах';
			du[2]='бист';
			du[3]='сих';
			du[4]='чил';
			du[5]='панчох';
			du[6]='шаст';
			du[7]='хафтод';
			du[8]='хаштод';
			du[9]='навад';

			if (ar.length==1){
				result = dec[ar[0]];
				//alert(result);
			}
			else
				if (ar.length == 2)
				{

					two = parseInt(String(ar[0].concat(ar[1])));
					if (two <=19 )
					{
						result = (dec[two]);
					//	alert(result);
					}
					else
						if (two > 19)
						{
							var zero = String(two);
							if (zero.substr(1,1)== 0)
							{
								result = du[(parseInt(zero.substr(0,1)))];
					  //			alert(result);
							}
							else
							{
								var t = new String(two);
								var first = parseInt(t.substr(0,1));
								var second = parseInt(t.substr(1,1));
								result = String(du[first])+ 'у '+ String(dec[second]);
						//		alert(result);
							}

						}
				}
			else
				if (ar.length == 3)
				{
					three = parseInt(String(ar[0] + ar[1]+ ar[2]));
					var thr = String(ar[0] + ar[1]+ ar[2]);
					if (thr.substr(1,1) == 0 && thr.substr(2,1)== 0 )
					{
						result = String( dec[parseInt( thr.substr(0,1) ) ] + hundred);
					//	alert(result);
					}else
					if (thr.substr(1,1) == 0)
					{
						result = String( dec[parseInt( thr.substr(0,1) ) ] + hundred + 'у ');
						result+= String(dec[thr.substr(2)]);
					  //	alert(result);
					}else
					if (thr.substr(2,1) == 0)
					{
						result = String (dec[parseInt(thr.substr(0,1))] + hundred + 'у ' + du[parseInt(thr.substr(1,1))]);
						//alert(result);
					}
					else
					{
						result = String(dec[parseInt(thr.substr(0,1))] + hundred + 'у ');
						th2 = String (ar[1]+ar[2]);
						if ((parseInt(th2))<=19)
						{
							result += dec[parseInt(th2)];
						  //	alert(result);
						}
						else
						{
							result += du[parseInt(thr.substr(1,1))] + 'у '+ dec[(thr.substr(2,1))];
							//alert(result);

						}
					}
				}
               else
				if (ar.length == 4)
				{
					four = parseInt(String(ar[0] + ar[1]+ ar[2]+ ar[3]));
					var fo = String(ar[0] + ar[1]+ ar[2]+ar[3]);
                    if (fo.substr(0,1)!=0 && fo.substr(1,1)==0 && fo.substr(2,1)==0 && fo.substr(3,1)==0)
					{
							result = String(dec[parseInt(fo.substr(0,1))] + ' ' + thousand) ;
							
					}else
					if( fo.substr(0,1)!=0 && fo.substr(1,1) == 0 && fo.substr(2,1) != 0 && fo.substr(3,1) != 0)
					{
						result = String(dec[fo.substr(0,1)] + ' ' + thousand + va );
						lasttwooffo = String(fo.substr(2,1)+fo.substr(3,1));
						if (parseInt(lasttwooffo)<20)
						{
							result+=String(dec[parseInt(lasttwooffo)]);
						}
						else
							if(parseInt(lasttwooffo)>19)
							{
								result+= String(du[parseInt(lasttwooffo.substr(0,1))] + va + dec[parseInt(lasttwooffo.substr(1,1))]);
							}
						
					}else
					if( fo.substr(0,1)!=0 && fo.substr(1,1) == 0 && fo.substr(2,1) != 0 && fo.substr(3,1) == 0)
					{
						result = String(dec[fo.substr(0,1)] + ' ' + thousand + va );
						result+=String(du[parseInt(fo.substr(2,1))]);
					}else
					if( fo.substr(0,1)!=0 && fo.substr(1,1) != 0 && fo.substr(2,1) == 0 && fo.substr(3,1) != 0)
					{
						result = String(dec[fo.substr(0,1)] + ' ' + thousand + va );
						result+=String(dec[parseInt(fo.substr(1,1))]+ hundred + va);
						result+=String(dec[parseInt(fo.substr(3,1))]);
						
					}else
					if( fo.substr(0,1)!=0 && fo.substr(1,1) != 0 && fo.substr(2,1) == 0 && fo.substr(3,1) == 0)
					{
						result = String(dec[fo.substr(0,1)] + ' ' + thousand + va );
						if (parseInt(fo.substr(1,1))==1)
						{
							result+=String(hundred);
						}
						else
						{
							result+=String(dec[parseInt(fo.substr(1,1))] + hundred);
						}
						
					}else
					if( fo.substr(0,1)!=0 && fo.substr(1,1) != 0 && fo.substr(2,1) != 0 && fo.substr(3,1) != 0)
					{
						result = String(dec[fo.substr(0,1)] + ' ' + thousand + va );
						result+= String(dec[parseInt(fo.substr(1,1))] + hundred + va );
						lasttwooffo = String(fo.substr(2,1)+fo.substr(3,1));
						if (parseInt(lasttwooffo)<20)
						{
							result+=String(dec[parseInt(lasttwooffo)]);
						}
						else
						{
							result+=String(du[parseInt(lasttwooffo.substr(0,1))] + va );
							result+=String(dec[parseInt(lasttwooffo.substr(1,1))]);
						}
					
					}else
					if( fo.substr(0,1)!=0 && fo.substr(1,1) != 0 && fo.substr(2,1) != 0 && fo.substr(3,1) == 0)
					{
						result = String(dec[fo.substr(0,1)] + ' ' + thousand + va );
						result+= String(dec[parseInt(fo.substr(1,1))] + hundred + va );
						result+= String(du[parseInt(fo.substr(2,1))]);
					}
				}else
				if (ar.length == 5)
				{
					fiv = parseInt(String(ar[0] + ar[1]+ ar[2]+ ar[3]+ar[4]));
					var five = String(ar[0] + ar[1]+ ar[2]+ar[3]+ar[4]);
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)==0 && five.substr(3,1)==0 && five.substr(4,1)==0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' + thousand );
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)==0 && five.substr(3,1)==0 && five.substr(4,1)!=0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' +thousand + va);
						result+= String(dec[parseInt(five.substr(4,1))])
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)==0 && five.substr(3,1)!=0 && five.substr(4,1)==0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' +thousand + va);
						result+=String(du[parseInt(five.substr(3,1))]);
					
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)==0 && five.substr(3,1)!=0 && five.substr(4,1)!=0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' +thousand + va);
						lasttwooffive = String(five.substr(3,1)+five.substr(4,1));
						if (parseInt(lasttwooffive)<20)
						{
							result+= String(dec[parseInt(lasttwooffive)]);
						}else
						{
							result+= String(du[parseInt(lasttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(lasttwooffive.substr(1,1))]);
						}
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)!=0 && five.substr(3,1)==0 && five.substr(4,1)==0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' + thousand + va);
						if (parseInt(five.substr(2,1))==1)
						{
							result+=String(hundred);
						}else
						{
							result+= String(dec[ parseInt(five.substr(2,1))] + hundred);
						}
					
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)!=0 && five.substr(3,1)==0 && five.substr(4,1)!=0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' + thousand + va);
						result+= String(dec[parseInt(five.substr(2,1))] + hundred + va + ' ');
						result+= String(dec[parseInt(five.substr(4,1))]);
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)!=0 && five.substr(3,1)!=0 && five.substr(4,1)==0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' + thousand + va);
						result+= String(dec[parseInt(five.substr(2,1))] + hundred + va + ' ');
						result+=String(du[parseInt(five.substr(3,1))]);
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)==0 && five.substr(2,1)!=0 && five.substr(3,1)!=0 && five.substr(4,1)!=0 )
					{
						result = String(du[parseInt(five.substr(0,1))] + ' ' + thousand + va);
						lasttwooffive = String(five.substr(3,1)+five.substr(4,1));
						if (parseInt(lasttwooffive)<20)
						{
							result+= String(dec[parseInt(five.substr(2,1))] + hundred + va + ' ');
							result+=String(dec[lasttwooffive]);
						}else
						{
							result+= String(dec[parseInt(five.substr(2,1))] + hundred + va + ' ');
							result+=String(du[parseInt(five.substr(3,1))] + va);
							result+=String(dec[parseInt(five.substr(4,1))])
						}
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)==0 && five.substr(3,1)==0 && five.substr(4,1)==0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand);
						}
						
						
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)==0 && five.substr(3,1)==0 && five.substr(4,1)!=0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va);
						}
						result+=String(dec[parseInt(five.substr(4,1))]);
					
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)==0 && five.substr(3,1)!=0 && five.substr(4,1)==0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va);
						}
						result+=String(du[five.substr(3,1)]);
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)==0 && five.substr(3,1)!=0 && five.substr(4,1)!=0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand + va);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va+' ');
						}
						lasttwooffive = String(five.substr(3,1)+five.substr(4,1));
						if (parseInt(lasttwooffive)<20)
						{
							result+=String(dec[parseInt(lasttwooffive)]);
						}else
						{
							result+=String(du[parseInt(lasttwooffive.substr(0,1))] +va);
							result+=String(dec[parseInt(lasttwooffive.substr(1,1))]);
						}
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)!=0 && five.substr(3,1)==0 && five.substr(4,1)==0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand + va);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va+' ');
						}
						result+=String(dec[parseInt(five.substr(2,1))] + hundred);
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)!=0 && five.substr(3,1)==0 && five.substr(4,1)!=0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand + va);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va+' ');
						}
						result+=String(dec[parseInt(five.substr(2,1))] + hundred + va);
						result+=String(dec[parseInt(five.substr(4,1))]);
					
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)!=0 && five.substr(3,1)!=0 && five.substr(4,1)==0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand + va);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va+' ');
						}
						result+=String(dec[parseInt(five.substr(2,1))] + hundred + va);
						result+=String(du[parseInt(five.substr(3,1))]);
					}else
					if (five.substr(0,1)!=0 && five.substr(1,1)!=0 && five.substr(2,1)!=0 && five.substr(3,1)!=0 && five.substr(4,1)!=0 )
					{
						firsttwooffive=String(five.substr(0,1)+five.substr(1,1));
						if (parseInt(firsttwooffive)<20)
						{
							result = String(dec[parseInt(firsttwooffive)] + ' ' + thousand + va);
						}else
						{
							result = String(du[parseInt(firsttwooffive.substr(0,1))] + va );
							result+= String(dec[parseInt(firsttwooffive.substr(1,1))] + ' ' +thousand + va+' ');
						}
						result+=String(dec[parseInt(five.substr(2,1))] + hundred + va);
						lasttwooffive = String(five.substr(3,1)+five.substr(4,1));
						if (parseInt(lasttwooffive)<20)
						{
							result+=String(dec[lasttwooffive]);
						}else
						{
							result+=String(du[parseInt(five.substr(3,1))] + va);
							result+=String(dec[parseInt(five.substr(4,1))])
						}
						//WOW IT'S DONE XXXXX IT'S GREAT SUHROB ... YEAH
					
					}
					
					
					
				
				
				
				
				
				
				}


		}
		return (result) + ' сомони';
	 };

/**
*
*  UTF-8 data encode / decode
* 
*
**/
var Utf8 = {
 
	// public method for url encoding
	encode : function (string) {
		string = string.replace(/\r\n/g,"\n");
		var utftext = "";
 
		for (var n = 0; n < string.length; n++) {
 
			var c = string.charCodeAt(n);
 
			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}
 
		}
 
		return utftext;
	},
 
	// public method for url decoding
	decode : function (utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;
 
		while ( i < utftext.length ) {
 
			c = utftext.charCodeAt(i);
 
			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i+1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i+1);
				c3 = utftext.charCodeAt(i+2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}
 
		}
 
		return string;
	}
 
}  
	 
function shoh(id,mode)
{ if (document.getElementById)
  { // DOM3 = IE5, NS6
    if(mode=='toggle')
    {	if(document.getElementById(id).style.display=='none')
	{	document.getElementById(id).style.display='';
	} else
	{	document.getElementById(id).style.display='none';
	}
    } else
    {	document.getElementById(id).style.display = mode;
    }
  } else
  { if (document.layers)
    {	if(mode=='toggle')
	{	if(document.id.display=='none')
		{	document.id.display = '';
		} else
		{	document.id.display = 'none';
		} 
	} else
	{	document.id.display = mode;
	}
    } else
    {	if(mode=='toggle')
	{	if(document.all.id.style.display=='none')
		{	document.all.id.style.display = '';
		} else
		{	document.all.id.style.display = 'none';
		}
	} else
	{	document.all.id.style.display = mode;
	}
    }
  }
}	 

function defaultControl(c){
	c.select();
	c.focus();
}
function ReloadForm(fB){
	fB.click();
}
function rTN(event){
	if (window.event) k=window.event.keyCode;
	else if (event) k=event.which;
	else return true;
	kC=String.fromCharCode(k);
	if ((k==null) || (k==0) || (k==8) || (k==9) || (k==13) || (k==27)) return true;
	else if ((("0123456789,.-").indexOf(kC)>-1)) return true;
	else return false;
}
function assignComboToInput(c,i){
	i.value=c.value;
}
function inArray(v,tA,m){
	for (i=0;i<tA.length;i++) if (v==tA[i].value) return true;
	alert(m);
	return false;
}
function isDate(dS,dF){
var mA=dS.match(/^(\d{1,2})(\/|-|.)(\d{1,2})(\/|-|.)(\d{4})$/);
if (mA==null){
alert("Please enter the date in the format "+dF);
return false;
}
if (dF=="d/m/Y"){
d=mA[1];
m=mA[3];
}else{
d=mA[3];
m=mA[1];
}
y=mA[5];
if (m<1 || m>12){
alert("Month must be between 1 and 12");
return false;
}
if (d<1 || d>31){
alert("Day must be between 1 and 31");
return false;
}
if ((m==4 || m==6 || m==9 || m==11) && d==31){
alert("Month "+m+" doesn`t have 31 days");
return false;
}
if (m==2){
var isleap=(y%4==0);
if (d>29 || (d==29 && !isleap)){
alert("February "+y+" doesn`t have "+d+" days");
return false;
}
}
return true;
}
function eitherOr(o,t){
if (o.value!='') t.value='';
else if (o.value=='NaN') o.value='';
}
/*Renier & Louis (info@tillcor.com) 25.02.2007
Copyright 2004-2007 Tillcor International
*/
days=new Array('Su','Mo','Tu','We','Th','Fr','Sa');
months=new Array('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
dateDivID="calendar";
function Calendar(md,dF){
iF=document.getElementsByName(md).item(0);
pB=iF;
x=pB.offsetLeft;
y=pB.offsetTop+pB.offsetHeight;
var p=pB;
while (p.offsetParent){
p=p.offsetParent;
x+=p.offsetLeft;
y+=p.offsetTop;
}
dt=convertDate(iF.value,dF);
nN=document.createElement("div");
nN.setAttribute("id",dateDivID);
nN.setAttribute("style","visibility:hidden;");
document.body.appendChild(nN);
cD=document.getElementById(dateDivID);
cD.style.position="absolute";
cD.style.left=x+"px";
cD.style.top=y+"px";
cD.style.visibility=(cD.style.visibility=="visible" ? "hidden" : "visible");
cD.style.display=(cD.style.display=="block" ? "none" : "block");
cD.style.zIndex=10000;
drawCalendar(md,dt.getFullYear(),dt.getMonth(),dt.getDate(),dF);
}
function drawCalendar(md,y,m,d,dF){
var tD=new Date();
if ((m>=0) && (y>0)) tD=new Date(y,m,1);
else{
d=tD.getDate();
tD.setDate(1);
}
TR="<tr>";
xTR="</tr>";
TD="<td class='dpTD' onMouseOut='this.className=\"dpTD\";' onMouseOver='this.className=\"dpTDHover\";'";
xTD="</td>";
html="<table class='dpTbl'>"+TR+"<th colspan=3>"+months[tD.getMonth()]+" "+tD.getFullYear()+"</th>"+"<td colspan=2>"+
getButtonCode(md,tD,-1,"&lt;",dF)+xTD+"<td colspan=2>"+getButtonCode(md,tD,1,"&gt;",dF)+xTD+xTR+TR;
for(i=0;i<days.length;i++) html+="<th>"+days[i]+"</th>";
html+=xTR+TR;
for (i=0;i<tD.getDay();i++) html+=TD+"&nbsp;"+xTD;
do{
dN=tD.getDate();
TD_onclick=" onclick=\"postDate('"+md+"','"+formatDate(tD,dF)+"');\">";
if (dN==d) html+="<td"+TD_onclick+"<div class='dpDayHighlight'>"+dN+"</div>"+xTD;
else html+=TD+TD_onclick+dN+xTD;
if (tD.getDay()==6) html+=xTR+TR;
tD.setDate(tD.getDate()+1);
} while (tD.getDate()>1)
if (tD.getDay()>0) for (i=6;i>tD.getDay();i--) html+=TD+"&nbsp;"+xTD;
html+="</table>";
document.getElementById(dateDivID).innerHTML=html;
}
function getButtonCode(mD,dV,a,lb,dF){
nM=(dV.getMonth()+a)%12;
nY=dV.getFullYear()+parseInt((dV.getMonth()+a)/12,10);
if (nM<0){
nM+=12;
nY+=-1;
}
return "<button onClick='drawCalendar(\""+mD+"\","+nY+","+nM+","+1+",\""+dF+"\");'>"+lb+"</button>";
}
function formatDate(dV,dF){
	ds=String(dV.getDate());
	ms=String(dV.getMonth()+1);
	d=("0"+dV.getDate()).substring(ds.length-1,ds.length+1);
	m=("0"+(dV.getMonth()+1)).substring(ms.length-1,ms.length+1);
	y=dV.getFullYear();
	switch (dF) {
	case "d/m/Y":
	return d+"/"+m+"/"+y;
	case "d.m.Y":
	return d+"."+m+"."+y;
	case "Y/m/d":
	return y+"/"+m+"/"+d;
	default :
	return m+"/"+d+"/"+y;
	}
}
function convertDate(dS,dF){
	var d,m,y;
	if (dF="d.m.Y")
	dA=dS.split(".")
	else
	dA=dS.split("/");
	switch (dF){
	case "d/m/Y","d.m.Y":
	d=parseInt(dA[0],10);
	m=parseInt(dA[1],10)-1;
	y=parseInt(dA[2],10);
	break;
	case "Y/m/d":
	d=parseInt(dA[2],10);
	m=parseInt(dA[1],10)-1;
	y=parseInt(dA[0],10);
	break;
	default :
	d=parseInt(dA[1],10);
	m=parseInt(dA[0],10)-1;
	y=parseInt(dA[2],10);
	break;
	}
	return new Date(y,m,d);
}

function postDate(mydate,dS){
	var iF=document.getElementsByName(mydate).item(0);
	iF.value=dS;
	var cD=document.getElementById(dateDivID);
	cD.style.visibility="hidden";
	cD.style.display="none";
	iF.focus();
}
function clickDate(){
Calendar(this.name,this.alt);
}
function changeDate(){
isDate(this.value,this.alt);
}

	 
	 
	 
/*---------------------------------------------START OF CODE FOR DIAL1*/
// global variables //
var TIMER = 5;
var SPEED = 15;
var WRAPPER = 'soderj';

// calculate the current window width //
function pageWidth() {
  return window.innerWidth != null ? window.innerWidth : document.documentElement && document.documentElement.clientWidth ? document.documentElement.clientWidth : document.body != null ? document.body.clientWidth : null;
}

// calculate the current window height //
function pageHeight() {
  return window.innerHeight != null? window.innerHeight : document.documentElement && document.documentElement.clientHeight ? document.documentElement.clientHeight : document.body != null? document.body.clientHeight : null;
}

// calculate the current window vertical offset //
function topPosition() {
  return typeof window.pageYOffset != 'undefined' ? window.pageYOffset : document.documentElement && document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ? document.body.scrollTop : 0;
}

// calculate the position starting at the left of the window //
function leftPosition() {
  return typeof window.pageXOffset != 'undefined' ? window.pageXOffset : document.documentElement && document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ? document.body.scrollLeft : 0;
}

// build/show the dialog box, populate the data and call the fadeDialog function //
function showDialog(title,message,type,autohide) {
  if(!type) {
    type = 'error';
  }
  var dialog;
  var dialogheader;
  var dialogclose;
  var dialogtitle;
  var dialogcontent;
  var dialogmask;
  if(!document.getElementById('dialog')) {
    dialog = document.createElement('div');
    dialog.id = 'dialog';
    dialogheader = document.createElement('div');
    dialogheader.id = 'dialog-header';
    dialogtitle = document.createElement('div');
    dialogtitle.id = 'dialog-title';
    dialogclose = document.createElement('div');
    dialogclose.id = 'dialog-close'
    dialogcontent = document.createElement('div');
    dialogcontent.id = 'dialog-content';
    dialogmask = document.createElement('div');
    dialogmask.id = 'dialog-mask';
    document.body.appendChild(dialogmask);
    document.body.appendChild(dialog);
    dialog.appendChild(dialogheader);
    dialogheader.appendChild(dialogtitle);
    dialogheader.appendChild(dialogclose);
    dialog.appendChild(dialogcontent);;
    dialogclose.setAttribute('onclick','hideDialog()');
    dialogclose.onclick = hideDialog;
  } else {
    dialog = document.getElementById('dialog');
    dialogheader = document.getElementById('dialog-header');
    dialogtitle = document.getElementById('dialog-title');
    dialogclose = document.getElementById('dialog-close');
    dialogcontent = document.getElementById('dialog-content');
    dialogmask = document.getElementById('dialog-mask');
    dialogmask.style.visibility = "visible";
    dialog.style.visibility = "visible";
  }
  dialog.style.opacity = .00;
  dialog.style.filter = 'alpha(opacity=0)';
  dialog.alpha = 0;
  var width = pageWidth();
  var height = pageHeight();
  var left = leftPosition();
  var top = topPosition();
  var dialogwidth = dialog.offsetWidth;
  var dialogheight = dialog.offsetHeight;
  var topposition = top + (height / 3) - (dialogheight / 2);
  var leftposition = left + (width / 2) - (dialogwidth / 2);
  dialog.style.top = topposition + "px";
  dialog.style.left = leftposition + "px";
  dialogheader.className = type + "header";
  dialogtitle.innerHTML = title;
  dialogcontent.className = type;
  dialogcontent.innerHTML = message;
  var content = document.getElementById(WRAPPER);
  dialogmask.style.height = content.offsetHeight + 'px';
  dialog.timer = setInterval("fadeDialog(1)", TIMER);
  if(autohide) {
    dialogclose.style.visibility = "hidden";
    window.setTimeout("hideDialog()", (autohide * 1000));
  } else {
    dialogclose.style.visibility = "visible";
  }
}

// hide the dialog box //
function hideDialog() {
  var dialog = document.getElementById('dialog');
  clearInterval(dialog.timer);
  dialog.timer = setInterval("fadeDialog(0)", TIMER);
}

// fade-in the dialog box //
function fadeDialog(flag) {
  if(flag == null) {
    flag = 1;
  }
  var dialog = document.getElementById('dialog');
  var value;
  if(flag == 1) {
    value = dialog.alpha + SPEED;
  } else {
    value = dialog.alpha - SPEED;
  }
  dialog.alpha = value;
  dialog.style.opacity = (value / 100);
  dialog.style.filter = 'alpha(opacity=' + value + ')';
  if(value >= 99) {
    clearInterval(dialog.timer);
    dialog.timer = null;
  } else if(value <= 1) {
    dialog.style.visibility = "hidden";
    document.getElementById('dialog-mask').style.visibility = "hidden";
    clearInterval(dialog.timer);
  }
}


/*---------------------------------------------END  OF CODE FOR DIAL1*/




/*---------------------------------------------START  OF CODE FOR DIAL 2*/


// global variables
var shade;             // shade div (object reference)
var dialog_box;        // dialog box (object reference)
var dialog_width  = 0; // initialize dialog height
var dialog_height = 0; // initialize dialog height
var opHigh = 60;       // highest opacity level
var opLow  = 0;        // lowest opacity level (should be the same as initial opacity in the CSS)
var fadeSpeed = 18;		 // 18ms pause - fade speed
var function_call;		 // function called after button is pressed 


// initialize
window.onload = function(){
	// create dialog box
	dialog_box = document.createElement('div');
	dialog_box.setAttribute('id', 'dialog2');
	// create shade div
	shade = document.createElement('div');
	shade.setAttribute('id', 'shade');
	// append dialog box and shade to the page body
	var body = document.getElementsByTagName('body')[0];
	body.appendChild(shade);
	body.appendChild(dialog_box);
	// define onscroll & onresize event handler
	window.onscroll = dialog_position;
	window.onresize = dialog_position;
}


// show dialog box
function showDialog2(width, height, text, button1, button2){
	// define input2 (optional) button and set button1 default value
	var input2 = '';
	if (button1 == undefined) button1 = 'Close';
	// set dialog width, height and calculate central position
	dialog_width  = width;
	dialog_height = height;
	dialog_position();
	// if text ends with jpg, jpeg, gif or png, then prepare img tag
	var img = /(.jpg|.jpeg|.gif|.png)$/i;
	if (img.test(text))	text = '<IMG src="'+text+'" height="'+(height-40)+'"/>';
	// prepare button1
	button1 = button1.split('|');
	input1  = '<INPUT type="button" onclick="hideDialog2(\''+button1[1]+
						'\');" value="'+button1[0]+'">'
	// prepare optional button2 
	if (button2 != undefined){
		button2 = button2.split('|');
		input2  = '<INPUT type="button" onclick="hideDialog2(\''+button2[1]+
							'\');" value="'+button2[0]+'">'
	}
	// set HTML for dialog box - use table to vertical align content inside
	// dialog box (this should work in all browsers)
	dialog_box.innerHTML = '<TABLE><TR><TD valign="center" height="'+height+ 
												 '" width="'+width+'">'+text+'<P>'+input1+input2+'</P>'+
												 '</TD></TR></TABLE>';
	// show shade and dialog box
	shade.style.display = dialog_box.style.display = 'block';
	// hide dropdown menus, iframes & flash
	set_visibility('hidden');
	// show shaded div slowly
	fade(opLow, 10);
}


// hide dialog box and shade
function hideDialog2(fnc){
	// set function call
	function_call = fnc;
	// start fade out
	fade(opHigh, -20);
	// hide dialog box
	dialog_box.style.display = 'none';
	// show dropdown menu, iframe & flash
	set_visibility('visible');
	
}


// function sets dialog position to the center and maximize shade div
function dialog_position(){
	// define local variables
	var window_width, window_height, scrollX, scrollY;
	// Non-IE (Netscape compliant)
  if (typeof(window.innerWidth) == 'number'){
		window_width  = window.innerWidth;
		window_height = window.innerHeight;
		scrollX = window.pageXOffset;
		scrollY = window.pageYOffset;
  }
  // IE 6 standards compliant mode
  else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)){
    window_width  = document.documentElement.clientWidth;
    window_height = document.documentElement.clientHeight;
		scrollX = document.documentElement.scrollLeft;
		scrollY = document.documentElement.scrollTop;
		// IE hack because #shade{width:100%;height:100%;} will not work for IE if body{height:100%} isn't set also ?!
		shade.style.width   = window_width  + 'px';
		shade.style.height  = window_height + 'px';
  }
  // DOM compliant
  else if (document.body && (document.body.clientWidth || document.body.clientHeight)){
    window_width  = document.body.clientWidth;
    window_height = document.body.clientHeight;
		scrollX = document.body.scrollLeft;
		scrollY = document.body.scrollTop;
  }
	// place dialog box to the center
	dialog_box.style.left = (scrollX + ((window_width  - dialog_width)  / 2)) + 'px';
	dialog_box.style.top  = (scrollY + ((window_height - dialog_height) / 2)) + 'px';
	// set shade offset
	shade.style.top  = scrollY + 'px';
	shade.style.left = scrollX + 'px';
}


// show/hide dropdown menu, iframe and flash objects (work-around for dropdown menu problem in IE6)
function set_visibility(p) {
	// define tag array
	var obj = new Array();
	obj[0] = document.getElementsByTagName('select');
	obj[1] = document.getElementsByTagName('iframe');
	obj[2] = document.getElementsByTagName('object');
	// loop through fetched elements
	for (var x=0; x<obj.length; x++)
		for (var y=0; y<obj[x].length; y++) obj[x][y].style.visibility = p;
}


// shade fade in / fade out
function fade(opacity, step){
	shade.style.opacity = opacity / 100;                // set opacity for FF
	shade.style.filter  = 'alpha(opacity='+opacity+')'; // set opacity for IE
	opacity += step;                                    // update opacity level
	// recursive call if opacity is between 'low' and 'high' values
	if (opLow <= opacity && opacity <= opHigh)
		setTimeout(function(){fade(opacity,step)}, fadeSpeed); // 18ms pause - fade speed
	// hide shade div when fade out ends and make function call 
	else if (opLow > opacity){
		shade.style.display = 'none';
		if (function_call != 'undefined') eval(function_call);
	}
}


//
// onclick button handlers
//


// onclick button1 
function button1(){
	alert('Called button1()');
}

// onclick button22
function button2(){
	alert('Called button2()');
}

// SUHROB JUST INCLUDE YOUR SUBMITTING FUNCTIONS HERE...

function submitfrmReceipts(){
	document.frmReceipts.submit();
}







/*---------------------------------------------END  OF CODE FOR DIAL 2*/


