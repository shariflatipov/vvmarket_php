function setSelectedOpener_Acc_Person()
{
    var lstAcc_person = window.opener.document.forms[0].acc_person;
    var value = frmsel_Acc_Person.acc_person.options[frmsel_Acc_Person.acc_person.selectedIndex].value;	

    for (var i = 0; i < lstAcc_person.options.length; i++)
    {
        if ( lstAcc_person.options[i].value == value ) 
            lstAcc_person.options[i].selected = true;
    }
    window.close()
};

function setCode_Dependent()
{
    var code = document.forms[0].code;
    var type = document.forms[0].type;
    var depen_account = document.forms[0].depen_account;
    var flag = depen_account[depen_account.selectedIndex].lang;
	
    type[flag].selected = true;
    code.value = depen_account[depen_account.selectedIndex].title;
};

function setData_( _date, format )
{
    var sp_day = document.forms[0].sp_day;
    var sp_month = document.forms[0].sp_month;
    var sp_year = document.forms[0].sp_year;
    alert(_date);
    var monthPos = format.indexOf('mm');
    var yearPos = format.indexOf('yyyy');
    var dayPos = format.indexOf('dd');

    sp_month.value = _date.substr(monthPos,2);
    sp_day.value = _date.substr(dayPos,2);
    sp_year.value = _date.substr(yearPos,4);
};

function displayCalendar_(inputField,format,buttonObj,displayTime,timeInput)
{
    displayCalendar(inputField,format, buttonObj);
    setData_(inputField.value,format);
};

function setHiddenOpener_Group( index )
{	
    var obj_group  = window.opener.document.forms[0].elements["group_" + index];
    var obj_operation  = window.opener.document.forms[0].elements["operation_" + index];
    var obj_flag = window.opener.document.forms[0].elements["chkFlag_" + index];
    var group = document.forms[0].group.value;
    var operation = document.forms[0].operation.options[document.forms[0].operation.selectedIndex].value;
    
    obj_group.value = group; 
    obj_operation.value = operation;
    obj_flag.checked = true;
	
    window.close()
};

function setSelectedOpener_Supplier()
{
    var lstSupplier = window.opener.document.forms[0].supplier;
    var value = frmsel_Supplier.supplier.options[frmsel_Supplier.supplier.selectedIndex].value;	

    for (var i = 0; i < lstSupplier.options.length; i++)
    {
        if ( lstSupplier.options[i].value == value ) 
            lstSupplier.options[i].selected = true;
    }
    window.close()
};

function setSelectedOpener_Unit()
{
    var value, text;
    var lstUnit = window.opener.document.forms[0].lstUnit;
		
    var length = lstUnit.options.length;
    for (var i = length; i < frmUnit.unit.options.length; i++) {
        value = frmUnit.unit.options[i].value;
        text = frmUnit.unit.options[i].text;
        lstUnit.options[i] =  new Option( text, value ); // method BOM					
    }
	
    value = frmUnit.unit.options[frmUnit.unit.selectedIndex].value;	
    for (var i = 0; i < lstUnit.options.length; i++) {
        if ( lstUnit.options[i].value == value ) 
            lstUnit.options[i].selected = true;
    }
    window.close()
};


function setSelectedOpener( lstName_Opener, lstName )
{
    var value, text;
    var list_opener = window.opener.document.forms[0].elements[ lstName_Opener ];
    var list_child = document.forms[0].elements[ lstName ];
	
    var length_opener = list_opener.options.length;
    var length_child = list_child.options.length;
	
    for (var i = length_opener; i < length_child; i++) {
        value = list_child.options[i].value;
        text = list_child.options[i].text;
        list_opener.options[i] =  new Option( text, value ); // method BOM					
    }

    value = list_child.options[list_child.selectedIndex].value;
	
    for (var i = 0; i < length_child; i++) {
        if ( list_opener.options[i].value == value ) 
            list_opener.options[i].selected = true;
    }
	
    window.close()
};

function setSelectedOpener_Struct_Subdiv()
{
    var value, selvalue, text;
    var subdiv = window.opener.document.forms[0].subdiv;
    var lstSubdiv = window.document.forms[0].lstSubdiv;
    var length = subdiv.options.length;
    selvalue = lstSubdiv.options[lstSubdiv.selectedIndex].value;		

    if( selvalue == "back" ) {
        window.document.forms[0].submit();
        return;
    }
    clearSelect( subdiv );
    for (var i = 1; i < lstSubdiv.options.length; i++) {
        value = lstSubdiv.options[i].value;
        text = lstSubdiv.options[i].text;
        subdiv.options[i - 1] =  new Option( text, value ); // method BOM
        if ( value == selvalue ) 
            subdiv.options[i - 1].selected = true;
    }
		
    window.close()
};

function back_Struct_Subdiv()
{
    var _subdiv = document.forms[0].lstSubdiv;
    var selvalue = _subdiv.options[_subdiv.selectedIndex].value;			

    if( selvalue == "back" ) {
        window.document.forms[0].submit();
        return;
    }
};
function whichChecked( chkname )
{
    var chkbox, count = 0;	
    var length = document.forms[0].elements.length;
	
    for ( var i = 0; i < length; i++ ) {
        chkbox = document.forms[0].elements[i];
        if( chkbox.type == 'checkbox' &&
            chkbox.name == chkname )
            if( chkbox.checked == true ) count++;		
    }	
    document.forms[0].del.disabled = !count;
    document.forms[0].edit.disabled = !count;	
    return count;
};

function checkedAll( chkname )
{
    var chkbox;	
    var length = document.forms[0].elements.length;
    var chkmain = document.forms[0].elements["chkmain"];
	
    for ( var i = 0; i < length; i++ ) {
        chkbox = document.forms[0].elements[i];
        if( chkbox.type == 'checkbox' &&
            chkbox.name == chkname )
            chkbox.checked = chkmain.checked;				
    }
	
    document.forms[0].del.disabled = !chkmain.checked;
    document.forms[0].edit.disabled = !chkmain.checked;
};

function checkedAll_( chkname )
{
    var chkbox;	
    var length = document.forms[0].elements.length;
    var chkmain = document.forms[0].elements["chkmain"];
	
    for ( var i = 0; i < length; i++ ) {
        chkbox = document.forms[0].elements[i];
        if( chkbox.type == 'checkbox' &&
            chkbox.name == chkname ) {
            chkbox.checked = chkmain.checked;				
            depentHide_Show( chkbox );
        }
    }
	
    document.forms[0].del.disabled = !chkmain.checked;
    document.forms[0].edit.disabled = !chkmain.checked;
};

function depentHide_Show( objCheckbox )
{
    var value = objCheckbox.value;
    var flag  = objCheckbox.checked;    
    var dis = [ 'none', '' ]; // inline, block not work	
    flag += 0;
    document.getElementById( "depent_" + value ).style.display = dis[flag];
};

function depentHide_Show_( value )
{
    var dis = [ 'none', '' ]; // inline, block not work	
    var row = document.getElementById( "depent_" + value ).style;
	
    if( row.display == 'none' ) row.display = '';
    else row.display = 'none'; 
};

function selectedHide_Show( chkname, flag )
{
    var chkbox;
    var dis = [ 'none', '' ]; // inline, block not work	
    var length = document.forms[0].elements.length;
	
    for ( var i = 0; i < length; i++ ) {
        chkbox = document.forms[0].elements[i];
        if( chkbox.type == 'checkbox' &&
            chkbox.name == chkname )
            if( chkbox.checked == true )
                document.getElementById( chkbox.value ).style.display = dis[flag];
    }
};

function selectedHide_Show_( chkname, flag )
{
    var chkbox;
    var dis = [ 'none', '' ]; // inline, block not work	
    var length = document.forms[0].elements.length;
	
    for ( var i = 0; i < length; i++ ) {
        chkbox = document.forms[0].elements[i];
        if( chkbox.type == 'checkbox' &&
            chkbox.name == chkname )
            if( chkbox.checked == true ) {
                document.getElementById( chkbox.value ).style.display = dis[flag];
                document.getElementById( "depent_" + chkbox.value ).style.display = dis[flag];		
            }
    }
};

function checkedAccount( chkname )
{
    var chkbox, chkbox_depen;
    var account, count = 0;	
    var length = document.forms[0].elements.length;
	
    for ( var i = 0; i < length; i++ ) {
        chkbox = document.forms[0].elements[i];
		
        if( chkbox.type == 'checkbox' && chkbox.name == chkname ) { 			
            if( chkbox.checked == true ) count++;
        }
    }

    document.forms[0].del.disabled = !count;
    document.forms[0].edit.disabled = !count;		
};

function clearSelect( lstObject )
{
    for( var i = lstObject.options.length - 1; i >= 0; i-- )
        lstObject.remove( i );
};

function calcTotal( index )
{
    var objp = document.forms[0].elements["price_" + index];
    var objt = document.forms[0].elements["total_" + index];
    var objq = document.forms[0].elements["quantity_" + index];
    var objn = document.forms[0].elements["number_" + index];
    
    var price = window.parseFloat( objp.value );
    var quantity = window.parseFloat( objq.value );
    var stotal = 0;

    if( window.isNaN( price ) == true ) price = 0;
    if( window.isNaN( quantity ) == true ) quantity = 0;
    var total = (price * quantity).toFixed(3);
    if( total != 0 ) objn.value = index;
    objt.value = total;

    var rowCount = (document.getElementById("dataStoreTable").getElementsByTagName("TR").length - 1);
    for ( var i = 1; i < rowCount; i++ ) {
        textbox = document.forms[0].elements["total_"+i];
        if( textbox.type == 'text' ) {
            total = window.parseFloat( textbox.value );
            if( window.isNaN( total ) == true )  total = 0;
            stotal += total;
        }
    }

    document.forms[0].total.value =  stotal.toFixed(3);
}

function calcTotalExpense( index )
{
    var objp = document.forms[0].elements["price_" + index];
    var objt = document.forms[0].elements["total_" + index];
    var objq = document.forms[0].elements["quantity_" + index];
    var objn = document.forms[0].elements["number_" + index];
    

    var price = window.parseFloat( objp.value );
    var quantity = window.parseFloat( objq.value );
  
    var stotal = 0;
    var sQuantity = 0;
   

    if( window.isNaN( price ) == true ) price = 0;
    if( window.isNaN( quantity ) == true ) quantity = 0;
    var total = price * quantity;


    if( total != 0 ) objn.value = index;
    objt.value = total;

    for ( var i = 1; i < 17; i++ ) {
        textbox = document.forms[0].elements["total_"+i];
        lQuantity = document.forms[0].elements["quantity_"+i];
        if( textbox.type == 'text' ) {
            total = window.parseFloat( textbox.value );
            if( window.isNaN( total ) == true )  total = 0;
            stotal += total;
        }
        if( lQuantity.type == 'text' ) {
            quantity = window.parseFloat( lQuantity.value );
            if( window.isNaN( quantity ) == true )  quantity = 0;
            sQuantity += quantity;
        }
    }

    document.forms[0].total.value =  stotal;
    document.forms[0].totalQuantity.value =  sQuantity;
   
}

function calcTotalReturn( index,count )
{
    var objq = document.getElementById("Quantity_" + index).value;
    var objh = document.forms[0].elements["HQuantity_" + index];
    var objp = document.forms[0].elements["price_" + index];
    var objt = document.forms[0].elements["summ_" + index];

    var quantity = window.parseFloat(objq);
    var hquantity = window.parseFloat( objh.value );
    var price = window.parseFloat( objp.value );
    var stotal = 0;
    if (hquantity<quantity){
        alert("Имконнопазир аст !");
        document.getElementById("Quantity_" + index).value=hquantity;
        quantity=hquantity;
    }

    if( window.isNaN( quantity ) == true ) quantity = 0;
    var total = price * quantity;
    objt.value = window.parseFloat(total);
    for ( var i = 1; i <(count+1); i++ ) {
        textbox = document.forms[0].elements["summ_"+i];
        if( textbox.type == 'text' ) {
            total = window.parseFloat( textbox.value );
            if( window.isNaN( total ) == true )  total = 0;
            stotal += total;
        }
    }
    document.forms[0].total.value =  stotal;
}

function validateNumber( objTextbox )
{
    var str = new String;
    str = objTextbox.value

    if( window.isNaN( str ) == true ) {
        objTextbox.value = str.substr( 0, str.lenght - 2 );
    }

    return true;
}