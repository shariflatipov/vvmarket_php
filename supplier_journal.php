﻿<?php
/* ------------------------------------------------------------------------------------------------------------- */
/* Add Supplier */
include_once "connectDB.php";
include_once "functions.php";
#include_once "template.inc";;
require_once "inc/headers.inc";
?>
<table ALIGN=center width=100%  border="1" cellpadding="5" cellspacing="0" class="collapse">

<script type="text/javascript">
	var chkname = 'chkSID[]';
</script>

<tr>
<td>
<?php
   
/* TODO: Add code here */


	$_name = $_REQUEST['txtName'];
	$_work_doc = $_REQUEST['txtWork_doc'];
	$_date = $_REQUEST['date'];
	$_region = $_REQUEST['region'];
	$_city = $_REQUEST['city'];
	$_address = $_REQUEST['txtAddress'];
	$_phone = $_REQUEST['txtPhone'];
	$_phone_home = $_REQUEST['txtPhone_home'];
	$_person = $_REQUEST['person'];
	$_supid = $_REQUEST['supid'];
	$_task = $_REQUEST['task'];
	$supID = $_REQUEST['chkSID'];
	$count_query = "SELECT COUNT(`SupplierID`) FROM `Suppliers`;";		
	$limit = createLimitRecord( $_epos, $_spos, $up_disabled, $down_disabled, $count_query );


	$form_name = "frmSupplier";
	$disabled = 'disabled'; 
	$edit_disabled = 'disabled';
	createElements( $cmdAdd, $cmdDel, $cmdCancel, $cmdSave, $form_name ); 
	
/* Create filter for table `Suppliers` */	
	/* Variables  for filters */
	$_fsup_name = $_REQUEST['fsup_name'];
	$_fregion = $_REQUEST['fregion'];
	$_fcity = $_REQUEST['fcity'];
	$_fperson = $_REQUEST['fperson'];

	if( !isset($_fsup_name) || $_fsup_name == '' ) $sel_sup_name = '';
	else $sel_sup_name = " AND `Name` LIKE '%{$_fsup_name}%'";

	if( !isset($_fregion) || $_fregion == 'all' ) $sel_region = '';
	else $sel_region = " AND `Region` = " . $_fregion;
		
	if( !isset($_fcity) || $_fcity == 'all' ) $sel_city = '';
	else $sel_city = " AND `City` = " . $_fcity;

	if( !isset($_fperson) || $_fperson == 'all' ) $sel_person = '';
	else $sel_person = " AND `Person` = " . $_fperson;
	
	$filters = $sel_region . " " . $sel_city . " " . $sel_person . " " . $sel_sup_name;
	
/* End filter content */
	
	//debug("TASK " . $_task);
	

if( isset($_task) && $_task == 'add' )
	elementsDisabled_Swap();
elseif( isset($_task) && $_task == 'search' ) {
	$_search = $_REQUEST['search'];
	$_search = "AND `Name` LIKE '%" . $_search . "%'";
	//debug($search);
}	
elseif( isset($_task) && $_task == 'save' ){
	if(isset($_name) &&  $_name == ''  || isset($_work_doc) && $_work_doc == '' ||
		!isset($_city) || !isset($_region) ||
		isset($_address) && $_address == '' || isset($_phone) && $_phone == '' )
		$errpole = true;
	
	else {	
		if(isset($_supid) && $_supid != '' ) {
			$upd_query = "UPDATE `Suppliers` 
				SET `Name` = '" . $_name . 
				"', `Work_doc` = '" . $_work_doc .
				"', `Date` = '" . $_date .
				"', `Region` = " . $_region .
				", `City` = " . $_city .
				", `Address` = '". $_address . 
				"', `Phone_work` = '" . $_phone .
				"', `Phone_home` = '" . $_phone_home .				
				"', `Person` = " . $_person .
				" WHERE `SupplierID` = $_supid;";
			//debug($upd_query);										
			mysql_query($upd_query);
			upd_Images_Supplier( $_supid );
			//debug(  "update". mysql_error() ); 
		}
		else {
			$ins_query = "INSERT INTO `Suppliers` VALUES
			( NULL, \"$_name\", \"$_work_doc\", NULL, \"$_date\", $_region, $_city, \"$_address\", \"$_phone\", \"$_phone_home\", $_person );";

			//debug($ins_query);			
			mysql_query($ins_query);
			//debug( "insert" . mysql_error() ); 
			upd_Images_Supplier( last_insert_id() );
			$_REQUEST['dir'] = 'end';
			$limit = createLimitRecord( $_epos, $_spos, $up_disabled, $down_disabled, $count_query ); 							
		}
		
		empty_supplier_fields();
	}
}	


elseif( isset($_task) && $_task == 'del') {

	if(isset($supID)){
		foreach( $supID as $val ) { $ids .= "$val,";
		//debug($ids);
		}	
		$ids = substr( $ids, 0, strlen($ids) - 1 );
		$_query = "DELETE  FROM `Suppliers` WHERE `SupplierID` IN( $ids );";
		//debug($_query);
		mysql_query($_query);		
	}
}
elseif( isset($_task) && $_task == 'edit') {
	
	if(isset($supID)){
		
		//debug($supID[0]);
			
		elementsDisabled_Swap();	
		$query = "SELECT *
			    FROM `Suppliers`
			   WHERE `SupplierID` = $supID[0]";
		$hquery = mysql_query($query);
		$row = mysql_fetch_array($hquery);
		
		$_name = $row['Name'];
		$_work_doc = $row['Work_doc'];
		$_date = $row['Date'];
		$_region = $row['Region'];
		$_city = $row['City'];
		$_address = $row['Address'];
		$_phone = $row['Phone_work'];
		$_phone_home = $row['Phone_home'];
		$_person = $row['Person'];
		$cmd_hidden = "<INPUT TYPE=hidden NAME=supid value=$supID[0]>";
	}
}
elseif( isset($_task) && $_task == 'cancel') {
	//debug('cancel');
	empty_supplier_fields();
	
}
elseif(isset($_region) ) {
	elementsDisabled_Swap();
	$cmd_hidden = "<INPUT TYPE=hidden NAME=supid value=$_supid>";
}
if(!isset($_region))$_region = 102;
?>
<form name="frmSupplier"  method="post" enctype="multipart/form-data">
<?php
	if( $errpole == true ) {
		fill_all_fields();
		elementsDisabled_Swap();	
		
	}


	echo '<table  border="0"  class="collapse">
		  <tr><td>' . $lang[$language.'_Person'] . '</td><td>';
	# field person
	echo "<select name=person " . $disabled . ">";
	$selected[$_person] = "selected";
	echo "<option value=1 ".$selected[1].">" . $lang[$language.'_Legal'] . "</option>";
	echo "<option value=0 ".$selected[0].">" . $lang[$language.'_Physical'] . "</option>";

	# field address
	echo '</select></td>
		  <td>'. $lang[$language.'_Address'] . '</td><td rowspan=2>
		  <textarea style="width: 300px"  wrap=phisical name="txtAddress" ' . $disabled . '>'
		  . $_address . '</textarea></td></tr>';
	# field Name
	echo '<tr><td>'. $lang[$language.'_Name'] . '</td><td>
	      <INPUT name="txtName"  style="width: 300px" ' . $disabled . '
		  value="' . $_name . '" ></td></tr>';

	# field Base
	echo '<tr><td>'. $lang[$language.'_Base'] . '</td><td>
		  <INPUT name="txtWork_doc"  style="width: 60px" ' . $disabled . '
		   value="' . $_work_doc . '" >';
	echo '<INPUT style="width: 60px" maxLength=10 readonly="readonly" name="date" value="' . $_date . 
		 '"  onclick="displayCalendar(document.' . $form_name . '.date,\'yyyy-mm-dd\', this)"' . 
		 $disabled . '>';	
		
	echo '<input type="file" name="in_file" ' . $disabled . ' accept="image/*" ></td>';
	
	# field Phone
	echo '<td>'. $lang[$language.'_Phone'] . '</td><td>
	      <INPUT style="width: 300px" name="txtPhone" ' . $disabled . ' 
		  value="' . $_phone . '"></td></tr>';

	# field Region
	echo '<tr><td>'. $lang[$language.'_Region'] . '</td><td>
		 <select name="region"  ' . $disabled . '  
		size=1 style="width: 100px" onchange="frmSupplier.submit()">';

	$region_query = "SELECT * FROM `Regions_and_Cities` WHERE `Level` = 1;";
	$hregion_query = mysql_query($region_query);
	
	while( $row = mysql_fetch_array($hregion_query) ) {
		$selected = '';
		if( $row['RCID'] == $_region ) $selected = 'selected';
		echo "<OPTION VALUE=" . $row['RCID'] . " " . $selected . ">" . $row['Name_'.$language] . "</OPTION>";
	}

	# field City
	echo '</select>'. space(5) . ' ' . $lang[$language.'_City'] . space(6) .'<select name="city" ' . $disabled . '
          size=1  style="width: 100px">';

	$city_query = "SELECT * FROM `Regions_and_Cities` WHERE `Level` = 2 AND `Dependent` = " . $_region;
	$hcity_query = mysql_query($city_query);
	
	while( $row = mysql_fetch_array($hcity_query) ) {
		$selected = '';
		if( $row['RCID'] == $_city ) $selected = 'selected';
		echo "<OPTION VALUE=" . $row['RCID'] . " " . $selected .">" . $row['Name_'.$language] . "</OPTION>";
	}

	# field  Add Region & City
	echo '</select><input type=button value=".." 
		onclick="window.open(\'addRegion_City.php\', \'popWindow\', \'dependent,width=280,height=240,left=150 ,top=100\');" ' 
		. $disabled . '>';
		
	# field phone_home
	echo '</td><td>' . $lang[$language.'_Phone_home'] . '</td><td>
		  <INPUT style="width: 300px" name="txtPhone_home" ' . $disabled . '
		  value="' . $_phone_home . '"></td></tr>';


?>
</table>
<input type="hidden" name=task value="none">
<?php
	echo $cmd_hidden;
?>
</td>
</tr>

<?php
/* ------------------------------------------------------------------------------------------------------------- */
/* Control panels */
?>
<tr>

<td>




<table border="0" class="collapse" >

<tr>
<td>
<?php
	echo $cmdAdd . "</td><td>";
	echo $cmdDel;
	
	echo '</td><td><input type="submit" name="edit" value="'. $lang[$language.'_Edit'] . '" 
		  onclick="frmSupplier.task.value=\'edit\'"' . $edit_disabled . '></td><td>';

	createNavigateRecord( $_epos, $_spos, $up_disabled, $down_disabled, 'frmSupplier' );
	
	
?>

</tr>

</table>



</td>
</tr>
<?php
/* ------------------------------------------------------------------------------------------------------------- */
/* Show suppliers */
?>
<tr>

<td>	

<?php


#echo '<table ALIGN=center width=100%  border="1" cellpadding="2" cellspacing="0" class="silver">';
echo '<table ALIGN=center style="width: 1500px"  border="1" cellpadding="2" cellspacing="0" class="silver">';
/* Create Filter for Region */
$event = 'onchange="frmSupplier.submit()"';
	echo "<tr class=rh><td></td>";
	echo "<td><input name=fsup_name id='accepted_from' value='{$_fsup_name}' style ='width: 300px' onblur='{$form_name}.submit()'></td>";
	echo '<td></td><td>';
	$create_filter_region_query = "SELECT * FROM `Suppliers` GROUP BY `Region`;";
	//debug( $create_filter_region_query );
	
	createSelect( 'fregion', '', $event, '', $create_filter_region_query, 2, 'Region' );
	
/* Create Filter for City */
echo "</td><td>";

	$create_filter_city_query = "SELECT * FROM `Suppliers`  GROUP BY `City`;"; 
	//debug( $create_filter_city_query );
	
	createSelect( 'fcity', '', $event, '', $create_filter_city_query, 2, 'City' );

/* Create Filter for Person */
echo "</td><td></td><td></td><td></td><td>";

	#$create_filter_person_query = "SELECT * FROM `Suppliers` WHERE 1 " . $sel_acc . " GROUP BY `Person`;";
	$create_filter_person_query = "SELECT * FROM `Suppliers` GROUP BY `Person`;";
	//debug( $create_filter_person_query );
	
	createSelect( 'fperson', '', $event, '', $create_filter_person_query, 2, 'Person' );


	


$sel_sup_query = "SELECT *, ( SELECT `Name_{$language}` FROM `Regions_and_Cities` WHERE `RCID` = `Region` ) `Name_Region`
						, ( SELECT `Name_{$language}` FROM `Regions_and_Cities` WHERE `RCID` = `City` ) `Name_City`
				   FROM `Suppliers` WHERE `Name` != 'none' " . $filters . " " . $_search . " " . $limit; 
debug($sel_sup_query);
$_hquery = mysql_query($sel_sup_query);

echo "</tr><tr class=rh><td width=1%>
		<input type=checkbox name=chkmain onclick=\"checkedAll( 'chkSID[]' )\"></td>
		
		<td width=30%>" . $lang[$language.'_Name'] . "</td>
		<td width=11%>" . $lang[$language.'_Base'] . "</td>
		<td width=11%>" . $lang[$language.'_Region'] . "</td>
		<td width=11%>" . $lang[$language.'_City'] . "</td>
		<td width=11%>" . $lang[$language.'_Address'] . "</td>
		<td width=11%>" . $lang[$language.'_Phone'] . "</td>
		<td width=11%>" . $lang[$language.'_Phone_home'] . "</td>
		<td width=4%>" . $lang[$language.'_Person'] . "</td></tr>";
		
while( $row = mysql_fetch_array($_hquery) ) {
	$i++;
	$i %= 2;
	$bgcolor = ($i ? 'lightyellow' : 'white');
	
	echo "<tr id=". $row['SupplierID'] . " bgcolor=".$bgcolor."><td>
		  <input  type=checkbox  name=chkSID[]  onclick=\"whichChecked( 'chkSID[]' )\"
		  value=". $row['SupplierID'] . "></td>"; 
	echo "<td><a style='cursor: hand' onclick=\"depentHide_Show_( ". $row['SupplierID'] . " );\">
			&nbsp;&nbsp;". $row['Name'] ."</a></td>";	
	
	#echo "<td>&nbsp;&nbsp;". $row['Name'] ."</td>";
	echo "<td>&nbsp;&nbsp;". $row['Work_doc'] ."</td>";
	echo "<td>&nbsp;&nbsp;".  $row['Name_Region'] ."</td>";
	echo "<td>&nbsp;&nbsp;".  $row['Name_City'] ."</td>";
	echo "<td>&nbsp;&nbsp;".  $row['Address'] ."</td>";
	echo "<td>&nbsp;&nbsp;".  $row['Phone_work'] ."</td>";
	echo "<td>&nbsp;&nbsp;".  $row['Phone_home'] ."</td>";
	echo "<td>&nbsp;&nbsp;".  getPersonType( $row['Person'] ) ."</td>";
	echo "<td align=center >";
	if( $row['Work_doc_pic'] != '' ) {
		echo "<a onclick=\"window.open('load_pic.php?supplierid=". $row['SupplierID'] ."',
			  'popWindow', '')\">
			  <img src='images/doc.bmp'></a>";
	}
	echo "</td><TR>";
	echo "<TR id=depent_". $row['SupplierID'] . " style='display: none'><TD></TD>
			<TD COLSPAN=11 >" . Acc_Person::createAcc_Person_Prod_depent( $row['SupplierID'], 'SupplierID') . "</TD></TR>";
	
}
echo "</TABLE>"; 
?>



</td>

</td></tr>
</TABLE>
</form>